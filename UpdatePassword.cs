using System;
using System.Windows.Forms;

namespace ARMS
{
    public partial class UpdatePassword : Form
    {
        public UpdatePassword()
        {
            InitializeComponent();
            user = (User) Helper.Deserialize(Application.StartupPath + "\\userdb.xml", typeof(User));
        }

        private readonly User user;

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (textNewConfirm.Text != textNewPassword.Text)
            {
                MessageBox.Show("Passwords do not match. Please ensure you have entered the exact same passwords in the password field.", "Password Mismatch", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            if (textNewConfirm.Text.Length < 8)
            {
                MessageBox.Show("Password cannot be less than 8 characters.", "Password too Short", MessageBoxButtons.OK,
                                MessageBoxIcon.Asterisk);
                return;
            }
            if (Crypto.computeMD5(textOldPassword.Text) == user.Password)
            {
                try
                {
                    user.Password = Crypto.computeMD5(textNewConfirm.Text);
                    var result = Helper.Update("users", new[] {"password"}, new[] {user.Password}, new[] {"username"},
                                               new[] {user.Username}, "");
                    if (result == -1)
                    {
                        MessageBox.Show("An error occurred while trying to update your password. Please try again.",
                                        "Error Updating Password", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("An error occurred while trying to update your password. Please try again.",
                                        "Error Updating Password", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                Helper.SerializeData(typeof (User), Application.StartupPath + "\\userdb.xml", user);
                MessageBox.Show(
                    "Password updated successfully. You will have to enter your new password next time you login.",
                    "Password Updated Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                MessageBox.Show("Your Old Password is incorrect. Please enter correct password and try again.");
                textOldPassword.Clear();
                textOldPassword.Focus();
            }
        }
    }
}
