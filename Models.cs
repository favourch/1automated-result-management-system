using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARMS
{
    #region Models
    /// <summary>
    /// Model for the messages table. Contains method to additionally retrive sender/receiver images
    /// </summary>
    public class Messages
    {
        private string sender;
        private string recipient;
        private string message;
        private string subject;
        private DateTime date;
        private string table;//table to check for new value if critical
        private MessageType type;//0 means not critical, 1 means it's critical
        private string field;//field to ccompare with expceted value if critical
        private string expected;
        private string recipientImage;
        private string senderImage;

        public string RecipientImage
        {
            get
            {
                if (recipientImage == null)
                {
                    try
                    {
                        recipientImage =
                            Helper.selectResults(new[] { "display_image" }, "lecturers", new[] { "username" },
                                                  new[] { recipient }, "")[0][0];
                        return recipientImage;
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(ex);
                        return null;
                    }
                }
                return recipientImage;
            }
            set { recipientImage = value; }
        }

        public string SenderImage
        {
            get
            {
                if (senderImage == null)
                {
                    try
                    {
                        senderImage =
                            Helper.selectResults(new[] { "display_image" }, "lecturers", new[] { "username" },
                                                  new[] { sender }, "")[0][0];
                        return senderImage;
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(ex);
                        return null;
                    }
                }
                return senderImage;
            }
            set { senderImage = value; }
        }

        private Boolean unread;

        public string Sender
        {
            get { return sender; }
            set { sender = value; }
        }

        public string Recipient
        {
            get { return recipient; }
            set { recipient = value; }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public string Table
        {
            get { return table; }
            set { table = value; }
        }

        public MessageType Type
        {
            get { return type; }
            set { type = value; }
        }

        public string Field
        {
            get { return field; }
            set { field = value; }
        }

        public string Expected
        {
            get { return expected; }
            set { expected = value; }
        }

        public bool Unread
        {
            get { return unread; }
            set { unread = value; }
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        private string id;
    }

    /// <summary>
    /// Semester Enumeration
    /// </summary>
    public enum Semester
    {
        FIRST = 1,
        SECOND = 2
    }

    /// <summary>
    /// Model for Lecturer Courses Table
    /// </summary>
    public class LecturerCourse
    {
        private string courseCode;
        private string department;
        private string username;
        private string academicYear;
        private bool submitted;
        private bool? hodApproved;
        private bool? deanApproved;

        public string CourseCode
        {
            get { return courseCode; }
            set { courseCode = value; }
        }

        public bool Submitted
        {
            get { return submitted; }
            set { submitted = value; }
        }

        public bool? HodApproved
        {
            get { return hodApproved; }
            set { hodApproved = value; }
        }

        public bool? DeanApproved
        {
            get { return deanApproved; }
            set { deanApproved = value; }
        }

        public string AcademicYear
        {
            get { return academicYear; }
            set { academicYear = value; }
        }

        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
    }

    /// <summary>
    /// Model for the Course Table
    /// </summary>
    public class Course
    {
        private string courseTitle;
        private Semester semester;
        private ushort units;
        private string courseCode;
        public string Level
        {
            get { return courseCode[0] + "00"; }
        }

        public string CourseTitle
        {
            get { return courseTitle; }
            set { courseTitle = value; }
        }

        public string CourseCode
        {
            get { return courseCode; }
            set { courseCode = value; }
        }

        public Semester Semester
        {
            get { return semester; }
            set { semester = value; }
        }

        public ushort Units
        {
            get { return units; }
            set { units = value; }
        }
    }

    public struct UserRoles
    {
        public const string DepartmentAdmin = "Department Admin";
        public const string ExamOfficer = "Exam Officer";
        public const string Lecturer = "Lecturer";
        public const string HOD = "HOD";
        public const string Bursar = "Bursar";
        public const string Dean = "Dean";
    }

    public class StudentResult
    {
        private string regNo;
        private string _courseCode;
        private string ca_score = "AR";
        private string exam_score = "AR";
        private string total_score = "AR";
        private int score;
        private string academicYear;
        private string name;
        private string _grade = "AR";

        public string AcademicYear
        {
            get { return academicYear; }
            set { academicYear = value; }
        }

        public string Grade
        {
            get { return _grade; }
        }

        public int GpScore
        {
            get
            {
                switch (_grade)
                {
                    case "A":
                        return 5;
                    case "B":
                        return 4;
                    case "C":
                        return 3;
                    case "D":
                        return 2;
                    case "E":
                        return 1;
                    case "F":
                        return 0;
                    default:
                        return -1;
                }
            }
        }

        public int Score
        {
            get { return score; }
            set
            {
                score = value;
                _grade = StudentData.ComputeGrade(score);
            }
        }

        public string ExamScore
        {
            get { return exam_score; }
            set { exam_score = value; }
        }

        public string CaScore
        {
            get { return ca_score; }
            set { ca_score = value; }
        }

        public string CourseCode
        {
            get { return _courseCode; }
            set { _courseCode = value; }
        }

        public string RegNo
        {
            get { return regNo; }
            set { regNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string TotalScore
        {
            get { return total_score; }
            set
            {
                total_score = value;
                int cscore;
                if (Int32.TryParse(value, out cscore))
                {
                    score = cscore;
                    _grade = StudentData.ComputeGrade(score);
                }
            }
        }
    }

    public class Announcement
    {
        private string text;
        private string sender;
        private string target;
        private string group;

        public string Group
        {
            get { return @group; }
            set { @group = value; }
        }

        public string Target
        {
            get { return target; }
            set { target = value; }
        }

        public string Sender
        {
            get { return sender; }
            set { sender = value; }
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
    }

    public class Activity
    {
        private string _text;
        private string _username;
        private string _department;
        private string _faculty;
        private string _id;
        private DateTime date;

        public string Faculty
        {
            get { return _faculty; }
            set { _faculty = value; }
        }

        public string Department
        {
            get { return _department; }
            set { _department = value; }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        public DateTime ActivityDate
        {
            get { return date; }
            set { date = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }
    }

    public class Error
    {
        private string message;
        private string stackTrace;
        private string errorDate;

        public Error() { }
        public Error(Exception e)
        {
            message = e.Message;
            stackTrace = e.StackTrace;
            errorDate = DateTime.Now.ToShortDateString();
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public string StackTrace
        {
            get { return stackTrace; }
            set { stackTrace = value; }
        }

        public string ErrorDate
        {
            get { return errorDate; }
            set { errorDate = value; }
        }

        public string[] getArray()
        {
            List<string> arr = new List<string>();
            arr.AddRange(new[] { message, stackTrace, errorDate });
            return arr.ToArray();
        }
    }

    public class StudentData
    {
        private string studentName;

        public string StudentName
        {
            get { return studentName; }
            set { studentName = value; }
        }
        private string regNo;

        public string RegNo
        {
            get { return regNo; }
            set { regNo = value; }
        }
        private string dept;

        public string Dept
        {
            get { return dept; }
            set { dept = value; }
        }

        private string deptShortName;

        public string DeptShortName
        {
            get { return deptShortName; }
            set { deptShortName = value; }
        }

        private string level;

        public string Level
        {
            get { return level; }
            set { level = value; }
        }

        private string caMark;
        private double caScore;

        public string CaMark
        {
            get { return caMark; }
            set
            {
                if (value == "AR" || value == "MR" || value == "NR")
                {
                    totalMark = value;
                    totalScore = 0 + examScore;
                }
                else
                {
                    try
                    {
                        caScore = Convert.ToDouble(value);
                        totalScore = examScore + caScore;
                        totalMark = totalScore.ToString();
                        gradeLetter = ComputeGrade((int)totalScore);
                    }
                    catch { }
                }
                caMark = value;
            }
        }

        private string examMark;
        private double examScore;

        public string ExamMark
        {
            get { return examMark; }
            set
            {
                if (value == "AR" || value == "MR" || value == "NR")
                {
                    totalMark = value;
                    totalScore = 0 + caScore;
                }
                else
                {
                    try
                    {
                        examScore = Convert.ToDouble(value);
                        totalScore = examScore + caScore;
                        totalMark = totalScore.ToString();
                        gradeLetter = ComputeGrade((int)totalScore);
                    }
                    catch { }
                }
                examMark = value;
            }
        }

        private string totalMark;
        private double totalScore;

        public string TotalMark
        {
            get { return totalMark; }
        }

        private string gradeLetter;

        public string GradeLetter
        {
            get { return gradeLetter; }
        }

        public double CaScore
        {
            get { return caScore; }
        }

        public double ExamScore
        {
            get { return examScore; }
        }

        public double TotalScore
        {
            get { return totalScore; }
        }

        public int GpScore
        {
            get
            {
                switch (gradeLetter)
                {
                    case "A":
                        return 5;
                    case "B":
                        return 4;
                    case "C":
                        return 3;
                    case "D":
                        return 2;
                    case "E":
                        return 1;
                    case "F":
                        return 0;
                    default:
                        return -1;
                }
            }
        }

        private string courseOfStudy;
        public string CourseOfStudy
        {
            get { return courseOfStudy; }
            set { courseOfStudy = value; }
        }

        private string phone;
        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public List<string> GetItems()
        {
            List<string> internallist = new List<string>();
            internallist.Add(studentName);
            internallist.Add(regNo);
            internallist.Add(dept);
            internallist.Add(level);
            internallist.Add(caMark);
            internallist.Add(examMark);
            internallist.Add(totalMark);
            internallist.Add(gradeLetter ?? "NR");
            return internallist;
        }

        public List<string> GetItemsShort()
        {
            List<string> internallist = new List<string>();
            internallist.Add(studentName);
            internallist.Add(regNo);
            internallist.Add(deptShortName);
            internallist.Add(level);
            internallist.Add(caMark);
            internallist.Add(examMark);
            internallist.Add(totalMark);
            internallist.Add(gradeLetter ?? "NR");
            return internallist;
        }

        public static string ComputeGrade(int score)
        {
            if (score >= 70 && score <= 100)
                return "A";
            if (score >= 60 && score < 70)
                return "B";
            if (score >= 50 && score < 60)
                return "C";
            if (score >= 45 && score < 50)
                return "D";
            if (score >= 40 && score < 45)
                return "E";
            if (score < 40)
                return "F";
            return "Missing";
        }
    }

    public class LecturerDetails
    {
        private string username;
        private string title;
        private string faculty;
        private string email;
        private string _staffCode;
        private string middleName;
        private string firstName;

        public string UserName
        {
            get { return username; }
            set { username = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }


        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        private string department;

        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string StaffCode
        {
            get { return _staffCode; }
            set { _staffCode = value; }
        }

        public string Faculty
        {
            get { return faculty; }
            set { faculty = value; }
        }

        public string MiddleName
        {
            get { return middleName; }
            set { middleName = value; }
        }

        public string FormalName
        {
            get
            {
                return string.Format("{0} {2}.{3} {1}", title, lastName, firstName[0],
                                     middleName == null ? "" : middleName[0] + ".");
            }
        }

        public string NiceName
        {
            get { return string.Format("{0} {1} {2}.", title, lastName, firstName); }
        }
    }

    public class DeptDetails
    {
        private string deptName;
        private string shortName;
        private string faculty;
        private string courseDuration;
        private string[] _programmes;
        private string _programmeType;

        public string DeptName
        {
            get { return deptName; }
            set { deptName = value; }
        }

        public string ShortName
        {
            get { return shortName; }
            set { shortName = value; }
        }

        public string Faculty
        {
            get { return faculty; }
            set { faculty = value; }
        }

        public string CourseDuration
        {
            get { return courseDuration; }
            set { courseDuration = value; }
        }

        public string[] Programmes
        {
            get { return _programmes; }
            set { _programmes = value; }
        }

        public string ProgrammeType
        {
            get { return _programmeType; }
            set { _programmeType = value; }
        }
    }

    public class Staff
    {
        private readonly LecturerDetails _lcd;
        private readonly string _staffCode;
        private readonly string name;

        public Staff(string staffCode, LecturerDetails ld)
        {
            _staffCode = staffCode;
            _lcd = ld;
            name = string.Format("{0} {1} {2}", ld.Title, ld.LastName, ld.FirstName);
            ;
        }

        public string StaffCode
        {
            get { return _staffCode; }
        }

        public LecturerDetails Lcd
        {
            get { return _lcd; }
        }

        public string Name
        {
            get { return name; }
        }
    }
    #endregion
}
