using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ARMS
{
    public partial class CheckedListCombo : UserControl
    {
        public CheckedListCombo()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Clears all items on the Control
        /// </summary>
        public void ClearItems()
        {
            checkedListBox1.Items.Clear();
        }

        /// <summary>
        /// Adds an array of items to the Control
        /// </summary>
        /// <param name="list"></param>
        public void AddItems(object[] list)
        {
            checkedListBox1.Items.AddRange(list);
        }

        public void AddItem(object item)
        {
            checkedListBox1.Items.Add(item);
        }

        /// <summary>
        /// Gets the checked items on the control separated by comma.
        /// </summary>
        public string Value
        {
            get { return String.Join(", ", GetChecked()); }
        }

        /// <summary>
        /// Gets or sets the text for the Control
        /// </summary>
        [Description("The text to be displayed in the comboBox. This automatically changes when items are selected."), Category("Data")]
        public string ComboText
        {
            get { return comboBox1.Text; }
            set { comboBox1.Text = value; }
        }

        /// <summary>
        /// Returns an IEnumerable &lt;string&gt; collection of the Checked Items. This only works for .NET 3.0 and above
        /// </summary>
        public string[] GetChecked()
        {
            return ComboText.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries);
        }

        private int originalHeight;
        private int originalListHeight;

        private void CheckedListbox_Leave(object sender, EventArgs e)
        {
            //Done, set selected items.
            checkedListBox1.Visible = false;
            comboBox1.Text = Value;
            Height = originalHeight;
            this.BorderStyle = BorderStyle.None;
        }

        private void comboBox1_SizeChanged(object sender, EventArgs e)
        {
            checkedListBox1.Location = new Point(0, comboBox1.Bottom);
        }

        private void CheckedListbox_Load(object sender, EventArgs e)
        {
            originalHeight = Height;
            originalListHeight = checkedListBox1.Height;

            //Try to keep the control on top so dropdown can be shown properly
            BringToFront();
            UpdateZOrder();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkedListBox1.ClearSelected();
            comboBox1.Text = "";
            foreach (var item in checkedListBox1.CheckedItems)
            {
                comboBox1.Text += item.ToString() + ",";
            }
        }

        private void comboBox1_MouseClick(object sender, MouseEventArgs e)
        {
            //Todo: Can implement dropdown only region click here
            if (checkedListBox1.Items.Count == 0)
            {
                return;
            }
            this.BorderStyle = BorderStyle.FixedSingle;
            checkedListBox1.Height = originalListHeight*checkedListBox1.Items.Count;
            if (originalHeight != checkedListBox1.Height)
            {
                Height += checkedListBox1.Height;
            }
            checkedListBox1.Visible = true;
        }

        private bool settingSize = false;

        private void CheckedListCombo_SizeChanged(object sender, EventArgs e)
        {
            if (!checkedListBox1.Visible && !settingSize)
            {
                settingSize = true;
                Size = comboBox1.Size;
                settingSize = false;
            }
        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.Items.Count == 0)
            {
                return;
            }
            this.BorderStyle = BorderStyle.FixedSingle;
            checkedListBox1.Height = originalListHeight * checkedListBox1.Items.Count;
            Height += checkedListBox1.Height;
            checkedListBox1.Visible = true;
        }
    }
}
