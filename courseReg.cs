using System;
using System.Windows.Forms;
using System.IO;

namespace ARMS
{
    public partial class courseReg : Form
    {
        private string department;
        public courseReg(string department)
        {
            InitializeComponent();
            this.department = department;
            textDept.Text = department;
            textDept.ReadOnly = true;
        }

        private void btnAddCourse_Click(object sender, EventArgs e)
        {
            var x = new[] { "NULL", textCourseTitle.Text, textCourseCode.Text, textUnitLoad.Text, comboSemester.Text, textDept.Text };
            if (Helper.dbInsert("courses", new[] { "id", "CourseTitle", "CourseCode", "Units", "Semester", "department" }, x) == 1)
            {

                MessageBox.Show(string.Format("{0} registered successfully.", textCourseTitle.Text), "Course Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
                x[0] = Environment.NewLine;
                File.AppendAllLines("courses.txt", x);
                textCourseCode.Clear();
                textCourseTitle.Clear();
                textUnitLoad.Clear();
            }
            else
            {
                MessageBox.Show("Error encountered trying to register course. Please check all fields and try again.", "Error Adding Course", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
