using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ARMS
{
    /// <summary>
    /// A simple window to display the ARMS rotating logo when a task that takes a long time is occurring.
    /// </summary>
    public partial class Processing : Form
    {
        /// <summary>
        /// Default constructor for the Form
        /// </summary>
        public Processing()
        {
            InitializeComponent();
        }
    }
}
