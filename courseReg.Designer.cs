namespace ARMS
{
    partial class courseReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(courseReg));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panelRegForm = new System.Windows.Forms.Panel();
            this.comboSemester = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textCourseTitle = new System.Windows.Forms.TextBox();
            this.textCourseCode = new System.Windows.Forms.TextBox();
            this.textUnitLoad = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textDept = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddCourse = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panelRegForm.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.label10);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(545, 51);
            this.panel1.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Candara", 25F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(545, 51);
            this.label10.TabIndex = 4;
            this.label10.Text = "Add/Edit Course";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelRegForm
            // 
            this.panelRegForm.Controls.Add(this.comboSemester);
            this.panelRegForm.Controls.Add(this.label1);
            this.panelRegForm.Controls.Add(this.textCourseTitle);
            this.panelRegForm.Controls.Add(this.textCourseCode);
            this.panelRegForm.Controls.Add(this.textUnitLoad);
            this.panelRegForm.Controls.Add(this.label8);
            this.panelRegForm.Controls.Add(this.textDept);
            this.panelRegForm.Controls.Add(this.label4);
            this.panelRegForm.Controls.Add(this.label3);
            this.panelRegForm.Controls.Add(this.label2);
            this.panelRegForm.Location = new System.Drawing.Point(37, 76);
            this.panelRegForm.Name = "panelRegForm";
            this.panelRegForm.Size = new System.Drawing.Size(453, 174);
            this.panelRegForm.TabIndex = 9;
            // 
            // comboSemester
            // 
            this.comboSemester.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboSemester.FormatString = "Semester \'{0}\'";
            this.comboSemester.FormattingEnabled = true;
            this.comboSemester.Items.AddRange(new object[] {
            "1",
            "2"});
            this.comboSemester.Location = new System.Drawing.Point(129, 77);
            this.comboSemester.MaxLength = 1;
            this.comboSemester.Name = "comboSemester";
            this.comboSemester.Size = new System.Drawing.Size(309, 21);
            this.comboSemester.TabIndex = 3;
            this.comboSemester.Text = "1";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label1.Location = new System.Drawing.Point(7, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Course Code:";
            // 
            // textCourseTitle
            // 
            this.textCourseTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCourseTitle.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textCourseTitle.Location = new System.Drawing.Point(129, 45);
            this.textCourseTitle.Name = "textCourseTitle";
            this.textCourseTitle.Size = new System.Drawing.Size(309, 25);
            this.textCourseTitle.TabIndex = 2;
            // 
            // textCourseCode
            // 
            this.textCourseCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCourseCode.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textCourseCode.Location = new System.Drawing.Point(129, 14);
            this.textCourseCode.Name = "textCourseCode";
            this.textCourseCode.Size = new System.Drawing.Size(309, 25);
            this.textCourseCode.TabIndex = 1;
            // 
            // textUnitLoad
            // 
            this.textUnitLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textUnitLoad.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textUnitLoad.Location = new System.Drawing.Point(129, 107);
            this.textUnitLoad.MaxLength = 2;
            this.textUnitLoad.Name = "textUnitLoad";
            this.textUnitLoad.Size = new System.Drawing.Size(309, 25);
            this.textUnitLoad.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label8.Location = new System.Drawing.Point(7, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 18);
            this.label8.TabIndex = 2;
            this.label8.Text = "Department:";
            // 
            // textDept
            // 
            this.textDept.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textDept.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textDept.Location = new System.Drawing.Point(129, 138);
            this.textDept.Name = "textDept";
            this.textDept.Size = new System.Drawing.Size(309, 25);
            this.textDept.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label4.Location = new System.Drawing.Point(7, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Unit Load:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label3.Location = new System.Drawing.Point(7, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Semester:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label2.Location = new System.Drawing.Point(7, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Course Title:";
            // 
            // btnAddCourse
            // 
            this.btnAddCourse.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.btnAddCourse.Location = new System.Drawing.Point(406, 266);
            this.btnAddCourse.Name = "btnAddCourse";
            this.btnAddCourse.Size = new System.Drawing.Size(119, 39);
            this.btnAddCourse.TabIndex = 10;
            this.btnAddCourse.Text = "Add Course";
            this.btnAddCourse.UseVisualStyleBackColor = true;
            this.btnAddCourse.Click += new System.EventHandler(this.btnAddCourse_Click);
            // 
            // courseReg
            // 
            this.AcceptButton = this.btnAddCourse;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 326);
            this.Controls.Add(this.btnAddCourse);
            this.Controls.Add(this.panelRegForm);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(553, 355);
            this.MinimumSize = new System.Drawing.Size(553, 355);
            this.Name = "courseReg";
            this.Text = "New Course Registration";
            this.panel1.ResumeLayout(false);
            this.panelRegForm.ResumeLayout(false);
            this.panelRegForm.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panelRegForm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textCourseCode;
        private System.Windows.Forms.TextBox textUnitLoad;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textDept;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddCourse;
        private System.Windows.Forms.TextBox textCourseTitle;
        private System.Windows.Forms.ComboBox comboSemester;
    }
}
