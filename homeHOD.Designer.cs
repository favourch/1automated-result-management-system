namespace ARMS
{
    partial class homeHOD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(homeHOD));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelMode = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripComboCourse = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolTextAcademicYear = new System.Windows.Forms.ToolStripTextBox();
            this.btnAnnouncements = new System.Windows.Forms.ToolStripButton();
            this.toolButtonLock = new System.Windows.Forms.ToolStripButton();
            this.btnDeptDetails = new System.Windows.Forms.ToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBoxResultSummary = new System.Windows.Forms.GroupBox();
            this.tableDetails = new System.Windows.Forms.TableLayoutPanel();
            this.labelNR = new System.Windows.Forms.Label();
            this.labelA = new System.Windows.Forms.Label();
            this.labelAR = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelF = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            this.labelE = new System.Windows.Forms.Label();
            this.labelD = new System.Windows.Forms.Label();
            this.btnReject = new ARMS.RibbonMenuButton();
            this.btnApprove = new ARMS.RibbonMenuButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBoxLecturer1 = new System.Windows.Forms.ComboBox();
            this.buttonAssignCourse = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxSession = new System.Windows.Forms.TextBox();
            this.labelPerformance = new System.Windows.Forms.Label();
            this.labelApproved = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.studentsData = new System.Windows.Forms.DataGridView();
            this.StudentSn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExamScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboCourse = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageHODHome = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBoxMessages = new System.Windows.Forms.GroupBox();
            this.richTextBoxAnnouncements = new System.Windows.Forms.RichTextBox();
            this.buttonPreviousAnnounce = new System.Windows.Forms.Button();
            this.buttonNextAnnounce = new System.Windows.Forms.Button();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.displayPic = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dataGridActivity = new System.Windows.Forms.DataGridView();
            this.activityDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activityText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageCourses = new System.Windows.Forms.TabPage();
            this.checkedListDepartments = new ARMS.CheckedListCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewCourses = new System.Windows.Forms.DataGridView();
            this.CourseCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.units = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.semester = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonSaveCourses = new System.Windows.Forms.Button();
            this.buttonDeleteCourse = new System.Windows.Forms.Button();
            this.buttonEditCourse = new System.Windows.Forms.Button();
            this.buttonAddCourse = new System.Windows.Forms.Button();
            this.comboBoxCourse = new System.Windows.Forms.ComboBox();
            this.tabPageHODResults = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.buttonSaveStaff = new System.Windows.Forms.Button();
            this.buttonDeleteStaff = new System.Windows.Forms.Button();
            this.buttonEditStaff = new System.Windows.Forms.Button();
            this.buttonAddStaff = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonAssignExams = new System.Windows.Forms.Button();
            this.buttonAssignAdmin = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxDeptAdmin = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxExamOfficer = new System.Windows.Forms.ComboBox();
            this.dataGridViewStaff = new System.Windows.Forms.DataGridView();
            this.staffID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SurName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBoxResultSummary.SuspendLayout();
            this.tableDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageHODHome.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBoxMessages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).BeginInit();
            this.tabPageCourses.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourses)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabPageHODResults.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStaff)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.labelMode);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(844, 62);
            this.panel2.TabIndex = 2;
            // 
            // labelMode
            // 
            this.labelMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.labelMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.labelMode.ForeColor = System.Drawing.Color.Red;
            this.labelMode.Location = new System.Drawing.Point(657, 12);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(176, 39);
            this.labelMode.TabIndex = 4;
            this.labelMode.Text = "ONLINE";
            this.labelMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboCourse,
            this.toolStripLabel1,
            this.toolTextAcademicYear,
            this.btnAnnouncements,
            this.toolButtonLock,
            this.btnDeptDetails});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(844, 62);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripComboCourse
            // 
            this.toolStripComboCourse.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolStripComboCourse.Name = "toolStripComboCourse";
            this.toolStripComboCourse.Size = new System.Drawing.Size(160, 62);
            this.toolStripComboCourse.Text = "Select Course";
            this.toolStripComboCourse.SelectedIndexChanged += new System.EventHandler(this.comboBoxCourse_SelectedIndexChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(105, 59);
            this.toolStripLabel1.Text = "Academic Year:";
            // 
            // toolTextAcademicYear
            // 
            this.toolTextAcademicYear.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolTextAcademicYear.Name = "toolTextAcademicYear";
            this.toolTextAcademicYear.Padding = new System.Windows.Forms.Padding(3);
            this.toolTextAcademicYear.Size = new System.Drawing.Size(78, 62);
            this.toolTextAcademicYear.Text = "2012/2013";
            // 
            // btnAnnouncements
            // 
            this.btnAnnouncements.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAnnouncements.Image = global::ARMS.Properties.Resources.announcements;
            this.btnAnnouncements.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAnnouncements.Name = "btnAnnouncements";
            this.btnAnnouncements.Size = new System.Drawing.Size(52, 59);
            this.btnAnnouncements.Text = "New Announcement";
            this.btnAnnouncements.Click += new System.EventHandler(this.btnAnnouncements_Click);
            // 
            // toolButtonLock
            // 
            this.toolButtonLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonLock.Image = global::ARMS.Properties.Resources.Lock;
            this.toolButtonLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonLock.Name = "toolButtonLock";
            this.toolButtonLock.Size = new System.Drawing.Size(52, 59);
            this.toolButtonLock.Text = "Lock Session";
            this.toolButtonLock.Click += new System.EventHandler(this.toolButtonLock_Click);
            // 
            // btnDeptDetails
            // 
            this.btnDeptDetails.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeptDetails.Image = global::ARMS.Properties.Resources.advancedsettings;
            this.btnDeptDetails.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeptDetails.Name = "btnDeptDetails";
            this.btnDeptDetails.Size = new System.Drawing.Size(52, 59);
            this.btnDeptDetails.Text = "Manage Department Profile";
            this.btnDeptDetails.Click += new System.EventHandler(this.btnUpdateData_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBoxResultSummary);
            this.panel3.Controls.Add(this.btnReject);
            this.panel3.Controls.Add(this.btnApprove);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 349);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5);
            this.panel3.Size = new System.Drawing.Size(827, 125);
            this.panel3.TabIndex = 3;
            // 
            // groupBoxResultSummary
            // 
            this.groupBoxResultSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxResultSummary.Controls.Add(this.tableDetails);
            this.groupBoxResultSummary.Location = new System.Drawing.Point(8, 8);
            this.groupBoxResultSummary.Name = "groupBoxResultSummary";
            this.groupBoxResultSummary.Size = new System.Drawing.Size(374, 106);
            this.groupBoxResultSummary.TabIndex = 3;
            this.groupBoxResultSummary.TabStop = false;
            this.groupBoxResultSummary.Text = "Result Summary";
            // 
            // tableDetails
            // 
            this.tableDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tableDetails.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableDetails.ColumnCount = 4;
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.93617F));
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.06383F));
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189F));
            this.tableDetails.Controls.Add(this.labelNR, 3, 1);
            this.tableDetails.Controls.Add(this.labelA, 0, 0);
            this.tableDetails.Controls.Add(this.labelAR, 3, 0);
            this.tableDetails.Controls.Add(this.labelB, 0, 1);
            this.tableDetails.Controls.Add(this.labelF, 2, 1);
            this.tableDetails.Controls.Add(this.labelC, 1, 0);
            this.tableDetails.Controls.Add(this.labelE, 2, 0);
            this.tableDetails.Controls.Add(this.labelD, 1, 1);
            this.tableDetails.Location = new System.Drawing.Point(6, 22);
            this.tableDetails.Name = "tableDetails";
            this.tableDetails.RowCount = 2;
            this.tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableDetails.Size = new System.Drawing.Size(362, 78);
            this.tableDetails.TabIndex = 4;
            // 
            // labelNR
            // 
            this.labelNR.AutoSize = true;
            this.labelNR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNR.Location = new System.Drawing.Point(173, 40);
            this.labelNR.Name = "labelNR";
            this.labelNR.Size = new System.Drawing.Size(184, 36);
            this.labelNR.TabIndex = 0;
            this.labelNR.Text = "No Results:";
            this.labelNR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelA.Location = new System.Drawing.Point(5, 2);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(40, 36);
            this.labelA.TabIndex = 0;
            this.labelA.Text = "As:";
            this.labelA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAR
            // 
            this.labelAR.AutoSize = true;
            this.labelAR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAR.Location = new System.Drawing.Point(173, 2);
            this.labelAR.Name = "labelAR";
            this.labelAR.Size = new System.Drawing.Size(184, 36);
            this.labelAR.TabIndex = 0;
            this.labelAR.Text = "Ommitted Results:";
            this.labelAR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelB.Location = new System.Drawing.Point(5, 40);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(40, 36);
            this.labelB.TabIndex = 0;
            this.labelB.Text = "Bs:";
            this.labelB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelF
            // 
            this.labelF.AutoSize = true;
            this.labelF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelF.Location = new System.Drawing.Point(103, 40);
            this.labelF.Name = "labelF";
            this.labelF.Size = new System.Drawing.Size(62, 36);
            this.labelF.TabIndex = 0;
            this.labelF.Text = "Fs:";
            this.labelF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC.Location = new System.Drawing.Point(53, 2);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(42, 36);
            this.labelC.TabIndex = 0;
            this.labelC.Text = "Cs:";
            this.labelC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelE
            // 
            this.labelE.AutoSize = true;
            this.labelE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelE.Location = new System.Drawing.Point(103, 2);
            this.labelE.Name = "labelE";
            this.labelE.Size = new System.Drawing.Size(62, 36);
            this.labelE.TabIndex = 0;
            this.labelE.Text = "Es:";
            this.labelE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelD
            // 
            this.labelD.AutoSize = true;
            this.labelD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelD.Location = new System.Drawing.Point(53, 40);
            this.labelD.Name = "labelD";
            this.labelD.Size = new System.Drawing.Size(42, 36);
            this.labelD.TabIndex = 0;
            this.labelD.Text = "Ds:";
            this.labelD.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnReject
            // 
            this.btnReject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReject.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnReject.BackColor = System.Drawing.Color.Transparent;
            this.btnReject.ColorBase = System.Drawing.Color.Gainsboro;
            this.btnReject.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))));
            this.btnReject.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReject.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnReject.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnReject.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnReject.Enabled = false;
            this.btnReject.FadingSpeed = 35;
            this.btnReject.FlatAppearance.BorderSize = 0;
            this.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReject.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnReject.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnReject.Image = global::ARMS.Properties.Resources.Delete;
            this.btnReject.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnReject.ImageOffset = 5;
            this.btnReject.IsPressed = false;
            this.btnReject.KeepPress = false;
            this.btnReject.Location = new System.Drawing.Point(676, 8);
            this.btnReject.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnReject.MenuPos = new System.Drawing.Point(0, 0);
            this.btnReject.Name = "btnReject";
            this.btnReject.Radius = 6;
            this.btnReject.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnReject.Size = new System.Drawing.Size(143, 106);
            this.btnReject.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnReject.SplitDistance = 0;
            this.btnReject.TabIndex = 1;
            this.btnReject.Text = "Reject Results";
            this.btnReject.Title = "";
            this.btnReject.UseVisualStyleBackColor = false;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnApprove
            // 
            this.btnApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApprove.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnApprove.BackColor = System.Drawing.Color.Transparent;
            this.btnApprove.ColorBase = System.Drawing.Color.Gainsboro;
            this.btnApprove.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))));
            this.btnApprove.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnApprove.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnApprove.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnApprove.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnApprove.Enabled = false;
            this.btnApprove.FadingSpeed = 35;
            this.btnApprove.FlatAppearance.BorderSize = 0;
            this.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApprove.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnApprove.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnApprove.Image = global::ARMS.Properties.Resources.apply;
            this.btnApprove.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnApprove.ImageOffset = 5;
            this.btnApprove.IsPressed = false;
            this.btnApprove.KeepPress = false;
            this.btnApprove.Location = new System.Drawing.Point(525, 8);
            this.btnApprove.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnApprove.MenuPos = new System.Drawing.Point(0, 0);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Radius = 6;
            this.btnApprove.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnApprove.Size = new System.Drawing.Size(143, 106);
            this.btnApprove.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnApprove.SplitDistance = 0;
            this.btnApprove.TabIndex = 2;
            this.btnApprove.Text = "Approve Results";
            this.btnApprove.Title = "";
            this.btnApprove.UseVisualStyleBackColor = false;
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(723, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 18);
            this.label2.TabIndex = 8;
            this.toolTip1.SetToolTip(this.label2, "Result performance rating");
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.LimeGreen;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(684, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(28, 25);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "Result performance rating");
            // 
            // comboBoxLecturer1
            // 
            this.comboBoxLecturer1.FormattingEnabled = true;
            this.comboBoxLecturer1.Location = new System.Drawing.Point(190, 43);
            this.comboBoxLecturer1.Name = "comboBoxLecturer1";
            this.comboBoxLecturer1.Size = new System.Drawing.Size(262, 26);
            this.comboBoxLecturer1.TabIndex = 0;
            this.comboBoxLecturer1.Text = "Select Lecturer";
            this.toolTip1.SetToolTip(this.comboBoxLecturer1, "Select the Lecturer 1 for this course");
            this.comboBoxLecturer1.SelectedIndexChanged += new System.EventHandler(this.comboBoxLecturer1_SelectedIndexChanged);
            // 
            // buttonAssignCourse
            // 
            this.buttonAssignCourse.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAssignCourse.BackColor = System.Drawing.Color.Transparent;
            this.buttonAssignCourse.ForeColor = System.Drawing.Color.Black;
            this.buttonAssignCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAssignCourse.ImageKey = "cap_graduate.png";
            this.buttonAssignCourse.ImageList = this.imageList1;
            this.buttonAssignCourse.Location = new System.Drawing.Point(692, 34);
            this.buttonAssignCourse.Name = "buttonAssignCourse";
            this.buttonAssignCourse.Padding = new System.Windows.Forms.Padding(6);
            this.buttonAssignCourse.Size = new System.Drawing.Size(124, 42);
            this.buttonAssignCourse.TabIndex = 1;
            this.buttonAssignCourse.Text = "(Un)&Assign";
            this.buttonAssignCourse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonAssignCourse, "Assign or Unassign this course  to this lecturer");
            this.buttonAssignCourse.UseVisualStyleBackColor = false;
            this.buttonAssignCourse.Click += new System.EventHandler(this.buttonAssignCourse_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "print_48.png");
            this.imageList1.Images.SetKeyName(1, "cap_graduate.png");
            this.imageList1.Images.SetKeyName(2, "pencil.png");
            this.imageList1.Images.SetKeyName(3, "edit_add.png");
            this.imageList1.Images.SetKeyName(4, "edit_remove.png");
            this.imageList1.Images.SetKeyName(5, "apply.png");
            this.imageList1.Images.SetKeyName(6, "Occupé.png");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxSession);
            this.panel1.Controls.Add(this.labelPerformance);
            this.panel1.Controls.Add(this.labelApproved);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.studentsData);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.comboCourse);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(827, 474);
            this.panel1.TabIndex = 10;
            // 
            // textBoxSession
            // 
            this.textBoxSession.Location = new System.Drawing.Point(119, 18);
            this.textBoxSession.Name = "textBoxSession";
            this.textBoxSession.Size = new System.Drawing.Size(117, 23);
            this.textBoxSession.TabIndex = 14;
            // 
            // labelPerformance
            // 
            this.labelPerformance.AutoSize = true;
            this.labelPerformance.Location = new System.Drawing.Point(678, 23);
            this.labelPerformance.Name = "labelPerformance";
            this.labelPerformance.Size = new System.Drawing.Size(0, 18);
            this.labelPerformance.TabIndex = 12;
            // 
            // labelApproved
            // 
            this.labelApproved.AutoEllipsis = true;
            this.labelApproved.Location = new System.Drawing.Point(593, 20);
            this.labelApproved.Name = "labelApproved";
            this.labelApproved.Size = new System.Drawing.Size(47, 21);
            this.labelApproved.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(513, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 18);
            this.label7.TabIndex = 13;
            this.label7.Text = "Approved:";
            // 
            // studentsData
            // 
            this.studentsData.AllowUserToAddRows = false;
            this.studentsData.AllowUserToDeleteRows = false;
            this.studentsData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.studentsData.BackgroundColor = System.Drawing.Color.White;
            this.studentsData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentsData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StudentSn,
            this.StudentName,
            this.RegNo,
            this.CAScore,
            this.ExamScore,
            this.TotalScore,
            this.Grade});
            this.studentsData.Location = new System.Drawing.Point(0, 53);
            this.studentsData.Name = "studentsData";
            this.studentsData.ReadOnly = true;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsData.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.studentsData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.studentsData.Size = new System.Drawing.Size(819, 298);
            this.studentsData.TabIndex = 11;
            // 
            // StudentSn
            // 
            this.StudentSn.DividerWidth = 1;
            this.StudentSn.HeaderText = "SN.";
            this.StudentSn.MaxInputLength = 900;
            this.StudentSn.Name = "StudentSn";
            this.StudentSn.ReadOnly = true;
            this.StudentSn.ToolTipText = "Serial Number";
            this.StudentSn.Width = 40;
            // 
            // StudentName
            // 
            this.StudentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StudentName.DividerWidth = 1;
            this.StudentName.HeaderText = "Name of Student";
            this.StudentName.Name = "StudentName";
            this.StudentName.ReadOnly = true;
            this.StudentName.ToolTipText = "Student Name";
            // 
            // RegNo
            // 
            this.RegNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RegNo.DividerWidth = 1;
            this.RegNo.HeaderText = "Reg. No.";
            this.RegNo.Name = "RegNo";
            this.RegNo.ReadOnly = true;
            this.RegNo.ToolTipText = "Student Registration Number";
            this.RegNo.Width = 81;
            // 
            // CAScore
            // 
            this.CAScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CAScore.DividerWidth = 1;
            this.CAScore.HeaderText = "CA Mark";
            this.CAScore.MaxInputLength = 2;
            this.CAScore.Name = "CAScore";
            this.CAScore.ReadOnly = true;
            this.CAScore.ToolTipText = "Continous Assessment";
            this.CAScore.Width = 79;
            // 
            // ExamScore
            // 
            this.ExamScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ExamScore.DividerWidth = 1;
            this.ExamScore.HeaderText = "Exam Mark";
            this.ExamScore.MaxInputLength = 2;
            this.ExamScore.Name = "ExamScore";
            this.ExamScore.ReadOnly = true;
            this.ExamScore.ToolTipText = "Examination Mark";
            this.ExamScore.Width = 93;
            // 
            // TotalScore
            // 
            this.TotalScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TotalScore.DividerWidth = 1;
            this.TotalScore.FillWeight = 10F;
            this.TotalScore.HeaderText = "Total";
            this.TotalScore.MaxInputLength = 3;
            this.TotalScore.Name = "TotalScore";
            this.TotalScore.ReadOnly = true;
            this.TotalScore.ToolTipText = "Total Mark";
            this.TotalScore.Width = 66;
            // 
            // Grade
            // 
            this.Grade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Grade.DividerWidth = 1;
            this.Grade.FillWeight = 10F;
            this.Grade.HeaderText = "Grade";
            this.Grade.MaxInputLength = 2;
            this.Grade.Name = "Grade";
            this.Grade.ReadOnly = true;
            this.Grade.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Grade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Grade.ToolTipText = "Result Letter Grade";
            this.Grade.Width = 52;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Candara", 9.75F);
            this.label6.Location = new System.Drawing.Point(5, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "Academic Session:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Candara", 9.75F);
            this.label4.Location = new System.Drawing.Point(264, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Course:";
            // 
            // comboCourse
            // 
            this.comboCourse.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboCourse.FormattingEnabled = true;
            this.comboCourse.Location = new System.Drawing.Point(318, 16);
            this.comboCourse.Name = "comboCourse";
            this.comboCourse.Size = new System.Drawing.Size(154, 24);
            this.comboCourse.TabIndex = 4;
            this.comboCourse.Text = "Select Course";
            this.comboCourse.SelectedIndexChanged += new System.EventHandler(this.comboCourse_SelectedIndexChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageHODHome);
            this.tabControl1.Controls.Add(this.tabPageCourses);
            this.tabControl1.Controls.Add(this.tabPageHODResults);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.tabControl1.Location = new System.Drawing.Point(3, 63);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(9, 6);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(841, 517);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPageHODHome
            // 
            this.tabPageHODHome.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageHODHome.Controls.Add(this.panel4);
            this.tabPageHODHome.Controls.Add(this.panel6);
            this.tabPageHODHome.Location = new System.Drawing.Point(4, 33);
            this.tabPageHODHome.Name = "tabPageHODHome";
            this.tabPageHODHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHODHome.Size = new System.Drawing.Size(833, 480);
            this.tabPageHODHome.TabIndex = 1;
            this.tabPageHODHome.Text = "Home";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.groupBoxMessages);
            this.panel4.Controls.Add(this.labelWelcome);
            this.panel4.Controls.Add(this.displayPic);
            this.panel4.Location = new System.Drawing.Point(3, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(827, 245);
            this.panel4.TabIndex = 4;
            // 
            // groupBoxMessages
            // 
            this.groupBoxMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMessages.Controls.Add(this.richTextBoxAnnouncements);
            this.groupBoxMessages.Controls.Add(this.buttonPreviousAnnounce);
            this.groupBoxMessages.Controls.Add(this.buttonNextAnnounce);
            this.groupBoxMessages.Location = new System.Drawing.Point(8, 41);
            this.groupBoxMessages.Name = "groupBoxMessages";
            this.groupBoxMessages.Size = new System.Drawing.Size(670, 201);
            this.groupBoxMessages.TabIndex = 3;
            this.groupBoxMessages.TabStop = false;
            this.groupBoxMessages.Text = "Announcements";
            // 
            // richTextBoxAnnouncements
            // 
            this.richTextBoxAnnouncements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxAnnouncements.Location = new System.Drawing.Point(6, 22);
            this.richTextBoxAnnouncements.Name = "richTextBoxAnnouncements";
            this.richTextBoxAnnouncements.Size = new System.Drawing.Size(658, 140);
            this.richTextBoxAnnouncements.TabIndex = 2;
            this.richTextBoxAnnouncements.Text = "No announcements found";
            // 
            // buttonPreviousAnnounce
            // 
            this.buttonPreviousAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPreviousAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonPreviousAnnounce.Location = new System.Drawing.Point(482, 168);
            this.buttonPreviousAnnounce.Name = "buttonPreviousAnnounce";
            this.buttonPreviousAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonPreviousAnnounce.TabIndex = 1;
            this.buttonPreviousAnnounce.Text = "Previous";
            this.buttonPreviousAnnounce.UseVisualStyleBackColor = true;
            // 
            // buttonNextAnnounce
            // 
            this.buttonNextAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonNextAnnounce.Location = new System.Drawing.Point(576, 168);
            this.buttonNextAnnounce.Name = "buttonNextAnnounce";
            this.buttonNextAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonNextAnnounce.TabIndex = 1;
            this.buttonNextAnnounce.Text = "Next";
            this.buttonNextAnnounce.UseVisualStyleBackColor = true;
            // 
            // labelWelcome
            // 
            this.labelWelcome.BackColor = System.Drawing.Color.Transparent;
            this.labelWelcome.Font = new System.Drawing.Font("Trebuchet MS", 18F);
            this.labelWelcome.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelWelcome.Location = new System.Drawing.Point(3, 4);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(488, 33);
            this.labelWelcome.TabIndex = 1;
            this.labelWelcome.Text = "Good day Mr. UNN. ";
            this.labelWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // displayPic
            // 
            this.displayPic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.displayPic.Image = global::ARMS.Properties.Resources.UNN_Logo1;
            this.displayPic.Location = new System.Drawing.Point(684, 51);
            this.displayPic.Name = "displayPic";
            this.displayPic.Size = new System.Drawing.Size(138, 185);
            this.displayPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.displayPic.TabIndex = 0;
            this.displayPic.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.AliceBlue;
            this.panel6.Controls.Add(this.dataGridActivity);
            this.panel6.Location = new System.Drawing.Point(0, 257);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(5);
            this.panel6.Size = new System.Drawing.Size(833, 222);
            this.panel6.TabIndex = 5;
            // 
            // dataGridActivity
            // 
            this.dataGridActivity.AllowUserToAddRows = false;
            this.dataGridActivity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridActivity.BackgroundColor = System.Drawing.Color.White;
            this.dataGridActivity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridActivity.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridActivity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridActivity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.activityDate,
            this.activityText});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridActivity.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridActivity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridActivity.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridActivity.EnableHeadersVisualStyles = false;
            this.dataGridActivity.GridColor = System.Drawing.Color.White;
            this.dataGridActivity.Location = new System.Drawing.Point(5, 5);
            this.dataGridActivity.MultiSelect = false;
            this.dataGridActivity.Name = "dataGridActivity";
            this.dataGridActivity.ReadOnly = true;
            this.dataGridActivity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridActivity.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridActivity.Size = new System.Drawing.Size(823, 212);
            this.dataGridActivity.TabIndex = 3;
            this.dataGridActivity.TabStop = false;
            // 
            // activityDate
            // 
            this.activityDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.activityDate.DividerWidth = 1;
            this.activityDate.FillWeight = 25F;
            this.activityDate.HeaderText = "Date";
            this.activityDate.MinimumWidth = 150;
            this.activityDate.Name = "activityDate";
            this.activityDate.ReadOnly = true;
            this.activityDate.Width = 160;
            // 
            // activityText
            // 
            this.activityText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.activityText.HeaderText = "Details";
            this.activityText.Name = "activityText";
            this.activityText.ReadOnly = true;
            // 
            // tabPageCourses
            // 
            this.tabPageCourses.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageCourses.Controls.Add(this.checkedListDepartments);
            this.tabPageCourses.Controls.Add(this.label5);
            this.tabPageCourses.Controls.Add(this.buttonAssignCourse);
            this.tabPageCourses.Controls.Add(this.groupBox2);
            this.tabPageCourses.Controls.Add(this.comboBoxLecturer1);
            this.tabPageCourses.Controls.Add(this.panel5);
            this.tabPageCourses.Controls.Add(this.comboBoxCourse);
            this.tabPageCourses.Location = new System.Drawing.Point(4, 33);
            this.tabPageCourses.Name = "tabPageCourses";
            this.tabPageCourses.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCourses.Size = new System.Drawing.Size(833, 480);
            this.tabPageCourses.TabIndex = 4;
            this.tabPageCourses.Text = "Manage Courses";
            // 
            // checkedListDepartments
            // 
            this.checkedListDepartments.BackColor = System.Drawing.Color.White;
            this.checkedListDepartments.ComboText = "Choose Departments";
            this.checkedListDepartments.Location = new System.Drawing.Point(467, 43);
            this.checkedListDepartments.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkedListDepartments.MinimumSize = new System.Drawing.Size(205, 26);
            this.checkedListDepartments.Name = "checkedListDepartments";
            this.checkedListDepartments.Size = new System.Drawing.Size(205, 26);
            this.checkedListDepartments.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 18);
            this.label5.TabIndex = 7;
            this.label5.Text = "Assign Courses:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dataGridViewCourses);
            this.groupBox2.Location = new System.Drawing.Point(5, 92);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(817, 323);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Manage Department Courses";
            // 
            // dataGridViewCourses
            // 
            this.dataGridViewCourses.AllowUserToAddRows = false;
            this.dataGridViewCourses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCourses.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCourses.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewCourses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCourses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CourseCode,
            this.CourseTitle,
            this.units,
            this.semester});
            this.dataGridViewCourses.Location = new System.Drawing.Point(10, 29);
            this.dataGridViewCourses.Name = "dataGridViewCourses";
            this.dataGridViewCourses.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCourses.Size = new System.Drawing.Size(801, 290);
            this.dataGridViewCourses.TabIndex = 2;
            this.dataGridViewCourses.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewCourses_CellBeginEdit);
            this.dataGridViewCourses.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCourses_CellEndEdit);
            // 
            // CourseCode
            // 
            this.CourseCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CourseCode.DividerWidth = 1;
            this.CourseCode.FillWeight = 30F;
            this.CourseCode.HeaderText = "Course Code";
            this.CourseCode.Name = "CourseCode";
            // 
            // CourseTitle
            // 
            this.CourseTitle.DividerWidth = 1;
            this.CourseTitle.FillWeight = 70F;
            this.CourseTitle.HeaderText = "Course Title";
            this.CourseTitle.Name = "CourseTitle";
            // 
            // units
            // 
            this.units.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.units.DividerWidth = 1;
            this.units.FillWeight = 10F;
            this.units.HeaderText = "Units";
            this.units.Name = "units";
            this.units.Width = 67;
            // 
            // semester
            // 
            this.semester.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.semester.DividerWidth = 1;
            this.semester.FillWeight = 10F;
            this.semester.HeaderText = "Semester";
            this.semester.Name = "semester";
            this.semester.Width = 93;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.buttonSaveCourses);
            this.panel5.Controls.Add(this.buttonDeleteCourse);
            this.panel5.Controls.Add(this.buttonEditCourse);
            this.panel5.Controls.Add(this.buttonAddCourse);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(3, 421);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(827, 56);
            this.panel5.TabIndex = 1;
            // 
            // buttonSaveCourses
            // 
            this.buttonSaveCourses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveCourses.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSaveCourses.BackColor = System.Drawing.Color.Transparent;
            this.buttonSaveCourses.Enabled = false;
            this.buttonSaveCourses.ForeColor = System.Drawing.Color.Black;
            this.buttonSaveCourses.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSaveCourses.ImageKey = "apply.png";
            this.buttonSaveCourses.ImageList = this.imageList1;
            this.buttonSaveCourses.Location = new System.Drawing.Point(671, 7);
            this.buttonSaveCourses.Name = "buttonSaveCourses";
            this.buttonSaveCourses.Padding = new System.Windows.Forms.Padding(6);
            this.buttonSaveCourses.Size = new System.Drawing.Size(148, 43);
            this.buttonSaveCourses.TabIndex = 9;
            this.buttonSaveCourses.Text = "&Save Changes";
            this.buttonSaveCourses.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSaveCourses.UseVisualStyleBackColor = false;
            this.buttonSaveCourses.Click += new System.EventHandler(this.buttonSaveCourses_Click_1);
            // 
            // buttonDeleteCourse
            // 
            this.buttonDeleteCourse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDeleteCourse.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonDeleteCourse.BackColor = System.Drawing.Color.Transparent;
            this.buttonDeleteCourse.ForeColor = System.Drawing.Color.Black;
            this.buttonDeleteCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDeleteCourse.ImageKey = "edit_remove.png";
            this.buttonDeleteCourse.ImageList = this.imageList1;
            this.buttonDeleteCourse.Location = new System.Drawing.Point(203, 7);
            this.buttonDeleteCourse.Name = "buttonDeleteCourse";
            this.buttonDeleteCourse.Padding = new System.Windows.Forms.Padding(6);
            this.buttonDeleteCourse.Size = new System.Drawing.Size(104, 43);
            this.buttonDeleteCourse.TabIndex = 6;
            this.buttonDeleteCourse.Text = "&Delete";
            this.buttonDeleteCourse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonDeleteCourse.UseVisualStyleBackColor = false;
            this.buttonDeleteCourse.Click += new System.EventHandler(this.buttonDeleteCourse_Click);
            // 
            // buttonEditCourse
            // 
            this.buttonEditCourse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonEditCourse.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonEditCourse.BackColor = System.Drawing.Color.Transparent;
            this.buttonEditCourse.ForeColor = System.Drawing.Color.Black;
            this.buttonEditCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEditCourse.ImageKey = "pencil.png";
            this.buttonEditCourse.ImageList = this.imageList1;
            this.buttonEditCourse.Location = new System.Drawing.Point(107, 7);
            this.buttonEditCourse.Name = "buttonEditCourse";
            this.buttonEditCourse.Padding = new System.Windows.Forms.Padding(6);
            this.buttonEditCourse.Size = new System.Drawing.Size(90, 43);
            this.buttonEditCourse.TabIndex = 7;
            this.buttonEditCourse.Text = "&Edit";
            this.buttonEditCourse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEditCourse.UseVisualStyleBackColor = false;
            this.buttonEditCourse.Click += new System.EventHandler(this.buttonEditCourse_Click);
            // 
            // buttonAddCourse
            // 
            this.buttonAddCourse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddCourse.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAddCourse.BackColor = System.Drawing.Color.Transparent;
            this.buttonAddCourse.ForeColor = System.Drawing.Color.Black;
            this.buttonAddCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddCourse.ImageKey = "edit_add.png";
            this.buttonAddCourse.ImageList = this.imageList1;
            this.buttonAddCourse.Location = new System.Drawing.Point(12, 7);
            this.buttonAddCourse.Name = "buttonAddCourse";
            this.buttonAddCourse.Padding = new System.Windows.Forms.Padding(6);
            this.buttonAddCourse.Size = new System.Drawing.Size(89, 43);
            this.buttonAddCourse.TabIndex = 8;
            this.buttonAddCourse.Text = "&Add";
            this.buttonAddCourse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddCourse.UseVisualStyleBackColor = false;
            this.buttonAddCourse.Click += new System.EventHandler(this.buttonAddCourse_Click);
            // 
            // comboBoxCourse
            // 
            this.comboBoxCourse.FormattingEnabled = true;
            this.comboBoxCourse.Location = new System.Drawing.Point(24, 43);
            this.comboBoxCourse.Name = "comboBoxCourse";
            this.comboBoxCourse.Size = new System.Drawing.Size(148, 26);
            this.comboBoxCourse.TabIndex = 0;
            this.comboBoxCourse.Text = "Select Course";
            this.comboBoxCourse.SelectedIndexChanged += new System.EventHandler(this.comboBoxCourse_SelectedIndexChanged);
            // 
            // tabPageHODResults
            // 
            this.tabPageHODResults.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageHODResults.Controls.Add(this.panel1);
            this.tabPageHODResults.Location = new System.Drawing.Point(4, 33);
            this.tabPageHODResults.Name = "tabPageHODResults";
            this.tabPageHODResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHODResults.Size = new System.Drawing.Size(833, 480);
            this.tabPageHODResults.TabIndex = 0;
            this.tabPageHODResults.Text = "Approve Results";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage1.Controls.Add(this.panel7);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.dataGridViewStaff);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(833, 480);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Manage Staff";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.buttonSaveStaff);
            this.panel7.Controls.Add(this.buttonDeleteStaff);
            this.panel7.Controls.Add(this.buttonEditStaff);
            this.panel7.Controls.Add(this.buttonAddStaff);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(3, 426);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(827, 51);
            this.panel7.TabIndex = 3;
            // 
            // buttonSaveStaff
            // 
            this.buttonSaveStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSaveStaff.BackColor = System.Drawing.Color.Transparent;
            this.buttonSaveStaff.ForeColor = System.Drawing.Color.Black;
            this.buttonSaveStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSaveStaff.ImageKey = "apply.png";
            this.buttonSaveStaff.ImageList = this.imageList1;
            this.buttonSaveStaff.Location = new System.Drawing.Point(671, 6);
            this.buttonSaveStaff.Name = "buttonSaveStaff";
            this.buttonSaveStaff.Padding = new System.Windows.Forms.Padding(6);
            this.buttonSaveStaff.Size = new System.Drawing.Size(148, 43);
            this.buttonSaveStaff.TabIndex = 7;
            this.buttonSaveStaff.Text = "&Save Changes";
            this.buttonSaveStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSaveStaff.UseVisualStyleBackColor = false;
            this.buttonSaveStaff.Click += new System.EventHandler(this.buttonSaveStaff_Click);
            // 
            // buttonDeleteStaff
            // 
            this.buttonDeleteStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDeleteStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonDeleteStaff.BackColor = System.Drawing.Color.Transparent;
            this.buttonDeleteStaff.ForeColor = System.Drawing.Color.Black;
            this.buttonDeleteStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDeleteStaff.ImageKey = "Occupé.png";
            this.buttonDeleteStaff.ImageList = this.imageList1;
            this.buttonDeleteStaff.Location = new System.Drawing.Point(201, 5);
            this.buttonDeleteStaff.Name = "buttonDeleteStaff";
            this.buttonDeleteStaff.Padding = new System.Windows.Forms.Padding(6);
            this.buttonDeleteStaff.Size = new System.Drawing.Size(104, 43);
            this.buttonDeleteStaff.TabIndex = 2;
            this.buttonDeleteStaff.Text = "&Delete";
            this.buttonDeleteStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonDeleteStaff.UseVisualStyleBackColor = false;
            this.buttonDeleteStaff.Click += new System.EventHandler(this.buttonDeleteStaff_Click);
            // 
            // buttonEditStaff
            // 
            this.buttonEditStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonEditStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonEditStaff.BackColor = System.Drawing.Color.Transparent;
            this.buttonEditStaff.ForeColor = System.Drawing.Color.Black;
            this.buttonEditStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEditStaff.ImageKey = "pencil.png";
            this.buttonEditStaff.ImageList = this.imageList1;
            this.buttonEditStaff.Location = new System.Drawing.Point(105, 6);
            this.buttonEditStaff.Name = "buttonEditStaff";
            this.buttonEditStaff.Padding = new System.Windows.Forms.Padding(6);
            this.buttonEditStaff.Size = new System.Drawing.Size(90, 43);
            this.buttonEditStaff.TabIndex = 2;
            this.buttonEditStaff.Text = "&Edit";
            this.buttonEditStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEditStaff.UseVisualStyleBackColor = false;
            this.buttonEditStaff.Click += new System.EventHandler(this.buttonEditStaff_Click);
            // 
            // buttonAddStaff
            // 
            this.buttonAddStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAddStaff.BackColor = System.Drawing.Color.Transparent;
            this.buttonAddStaff.ForeColor = System.Drawing.Color.Black;
            this.buttonAddStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddStaff.ImageKey = "edit_add.png";
            this.buttonAddStaff.ImageList = this.imageList1;
            this.buttonAddStaff.Location = new System.Drawing.Point(10, 5);
            this.buttonAddStaff.Name = "buttonAddStaff";
            this.buttonAddStaff.Padding = new System.Windows.Forms.Padding(6);
            this.buttonAddStaff.Size = new System.Drawing.Size(89, 43);
            this.buttonAddStaff.TabIndex = 2;
            this.buttonAddStaff.Text = "&Add";
            this.buttonAddStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddStaff.UseVisualStyleBackColor = false;
            this.buttonAddStaff.Click += new System.EventHandler(this.buttonAddStaff_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.buttonAssignExams);
            this.groupBox3.Controls.Add(this.buttonAssignAdmin);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.comboBoxDeptAdmin);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.comboBoxExamOfficer);
            this.groupBox3.Location = new System.Drawing.Point(13, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(807, 101);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Role Assignment";
            // 
            // buttonAssignExams
            // 
            this.buttonAssignExams.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAssignExams.BackColor = System.Drawing.Color.Transparent;
            this.buttonAssignExams.ForeColor = System.Drawing.Color.Black;
            this.buttonAssignExams.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAssignExams.ImageKey = "cap_graduate.png";
            this.buttonAssignExams.ImageList = this.imageList1;
            this.buttonAssignExams.Location = new System.Drawing.Point(279, 45);
            this.buttonAssignExams.Name = "buttonAssignExams";
            this.buttonAssignExams.Padding = new System.Windows.Forms.Padding(6);
            this.buttonAssignExams.Size = new System.Drawing.Size(103, 38);
            this.buttonAssignExams.TabIndex = 2;
            this.buttonAssignExams.Text = "&Assign";
            this.buttonAssignExams.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAssignExams.UseVisualStyleBackColor = false;
            this.buttonAssignExams.Click += new System.EventHandler(this.buttonAssignExams_Click);
            // 
            // buttonAssignAdmin
            // 
            this.buttonAssignAdmin.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAssignAdmin.BackColor = System.Drawing.Color.Transparent;
            this.buttonAssignAdmin.ForeColor = System.Drawing.Color.Black;
            this.buttonAssignAdmin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAssignAdmin.ImageKey = "cap_graduate.png";
            this.buttonAssignAdmin.ImageList = this.imageList1;
            this.buttonAssignAdmin.Location = new System.Drawing.Point(683, 45);
            this.buttonAssignAdmin.Name = "buttonAssignAdmin";
            this.buttonAssignAdmin.Padding = new System.Windows.Forms.Padding(6);
            this.buttonAssignAdmin.Size = new System.Drawing.Size(103, 38);
            this.buttonAssignAdmin.TabIndex = 2;
            this.buttonAssignAdmin.Text = "&Assign";
            this.buttonAssignAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAssignAdmin.UseVisualStyleBackColor = false;
            this.buttonAssignAdmin.Click += new System.EventHandler(this.buttonAssignAdmin_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(410, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(180, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Department Administrator:";
            // 
            // comboBoxDeptAdmin
            // 
            this.comboBoxDeptAdmin.FormattingEnabled = true;
            this.comboBoxDeptAdmin.Location = new System.Drawing.Point(413, 52);
            this.comboBoxDeptAdmin.Name = "comboBoxDeptAdmin";
            this.comboBoxDeptAdmin.Size = new System.Drawing.Size(264, 26);
            this.comboBoxDeptAdmin.TabIndex = 0;
            this.comboBoxDeptAdmin.SelectedIndexChanged += new System.EventHandler(this.comboBoxLecturer1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Department Exam Officer:";
            // 
            // comboBoxExamOfficer
            // 
            this.comboBoxExamOfficer.FormattingEnabled = true;
            this.comboBoxExamOfficer.Location = new System.Drawing.Point(9, 52);
            this.comboBoxExamOfficer.Name = "comboBoxExamOfficer";
            this.comboBoxExamOfficer.Size = new System.Drawing.Size(264, 26);
            this.comboBoxExamOfficer.TabIndex = 0;
            this.comboBoxExamOfficer.SelectedIndexChanged += new System.EventHandler(this.comboBoxLecturer1_SelectedIndexChanged);
            // 
            // dataGridViewStaff
            // 
            this.dataGridViewStaff.AllowUserToAddRows = false;
            this.dataGridViewStaff.AllowUserToDeleteRows = false;
            this.dataGridViewStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewStaff.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewStaff.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStaff.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.staffID,
            this.FirstName,
            this.SurName,
            this.email,
            this.staffCode});
            this.dataGridViewStaff.Location = new System.Drawing.Point(13, 124);
            this.dataGridViewStaff.Name = "dataGridViewStaff";
            this.dataGridViewStaff.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewStaff.Size = new System.Drawing.Size(809, 302);
            this.dataGridViewStaff.TabIndex = 1;
            this.dataGridViewStaff.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewStaff_CellBeginEdit);
            this.dataGridViewStaff.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStaff_CellEndEdit);
            // 
            // staffID
            // 
            this.staffID.HeaderText = "ID";
            this.staffID.Name = "staffID";
            this.staffID.ReadOnly = true;
            this.staffID.Width = 46;
            // 
            // FirstName
            // 
            this.FirstName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FirstName.FillWeight = 80F;
            this.FirstName.HeaderText = "First Name";
            this.FirstName.Name = "FirstName";
            // 
            // SurName
            // 
            this.SurName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SurName.FillWeight = 80F;
            this.SurName.HeaderText = "SurName";
            this.SurName.Name = "SurName";
            // 
            // email
            // 
            this.email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.email.FillWeight = 70F;
            this.email.HeaderText = "E-mail";
            this.email.Name = "email";
            // 
            // staffCode
            // 
            this.staffCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.staffCode.FillWeight = 38F;
            this.staffCode.HeaderText = "Staff Code";
            this.staffCode.Name = "staffCode";
            this.staffCode.ReadOnly = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "A robust result management solution for Educational Institutions";
            this.notifyIcon1.BalloonTipTitle = "Automated Result Management System 1.0";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
            this.notifyIcon1.BalloonTipShown += new System.EventHandler(this.notifyIcon1_BalloonTipShown);
            // 
            // homeHOD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(844, 582);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(860, 620);
            this.Name = "homeHOD";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Head of Department Administration Panel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.homeHOD_Load);
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBoxResultSummary.ResumeLayout(false);
            this.tableDetails.ResumeLayout(false);
            this.tableDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageHODHome.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBoxMessages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).EndInit();
            this.tabPageCourses.ResumeLayout(false);
            this.tabPageCourses.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourses)).EndInit();
            this.panel5.ResumeLayout(false);
            this.tabPageHODResults.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStaff)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboCourse;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageHODHome;
        private System.Windows.Forms.TabPage tabPageHODResults;
        private System.Windows.Forms.GroupBox groupBoxResultSummary;
        private RibbonMenuButton btnReject;
        private RibbonMenuButton btnApprove;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabPage tabPageCourses;
        private System.Windows.Forms.Button buttonAssignCourse;
        private System.Windows.Forms.ComboBox comboBoxLecturer1;
        private System.Windows.Forms.ComboBox comboBoxCourse;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridViewStaff;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxDeptAdmin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxExamOfficer;
        private System.Windows.Forms.ToolStripButton btnAnnouncements;
        private System.Windows.Forms.Button buttonAssignExams;
        private System.Windows.Forms.Button buttonAssignAdmin;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button buttonEditStaff;
        private System.Windows.Forms.Button buttonAddStaff;
        private System.Windows.Forms.Button buttonDeleteStaff;
        private System.Windows.Forms.DataGridView studentsData;
        private System.Windows.Forms.Label labelPerformance;
        private System.Windows.Forms.Label labelApproved;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonEditCourse;
        private System.Windows.Forms.Button buttonAddCourse;
        private System.Windows.Forms.Button buttonSaveStaff;
        private System.Windows.Forms.ToolStripComboBox toolStripComboCourse;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewCourses;
        private CheckedListCombo checkedListDepartments;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentSn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExamScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grade;
        private System.Windows.Forms.TableLayoutPanel tableDetails;
        private System.Windows.Forms.Label labelNR;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelAR;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelF;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Label labelE;
        private System.Windows.Forms.Label labelD;
        private System.Windows.Forms.Button buttonSaveCourses;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBoxMessages;
        private System.Windows.Forms.RichTextBox richTextBoxAnnouncements;
        private System.Windows.Forms.Button buttonPreviousAnnounce;
        private System.Windows.Forms.Button buttonNextAnnounce;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.PictureBox displayPic;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridView dataGridActivity;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityText;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn units;
        private System.Windows.Forms.DataGridViewTextBoxColumn semester;
        private System.Windows.Forms.TextBox textBoxSession;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonDeleteCourse;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox toolTextAcademicYear;
        private System.Windows.Forms.ToolStripButton btnDeptDetails;
        private System.Windows.Forms.ToolStripButton toolButtonLock;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SurName;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffCode;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

