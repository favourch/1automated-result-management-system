using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ARMS
{
    public partial class DepartmentEditForm : Form
    {
        public DepartmentEditForm()
        {
            InitializeComponent();
        }

        private DeptDetails dd;
        private string username;

        public DepartmentEditForm(DeptDetails dd, string username)
        {
            InitializeComponent();
            this.dd = dd;
            this.username = username;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Helper.Update("departments", new[] {"programmeType", "shortName", "courseDuration"},
                              new[] {comboBoxPType.Text, textBoxDeptShortName.Text, textBoxCourseDuration.Text},
                              new[] {"deptName"}, new[] {dd.DeptName}, "");
                Helper.ExecuteNonQuery(string.Format("INSERT IGNORE INTO programmes (deptName, programme) values('{1}', '{0}')", String.Join("'),('"+dd.DeptName + "','", richTextBoxPrograms.Lines), dd.DeptName));
                MessageBox.Show("Department Details Updated Successfully.", "Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Helper.logActivity(new Activity
                    {
                        Department = dd.DeptName,
                        Faculty = dd.Faculty,
                        Username = username,
                        Text =
                            string.Format(
                                "The HOD has updated the details of the department. New values: Short Name:{0}, CourseDuration:{1}, Programme Type:{2}",
                                textBoxDeptShortName.Text, textBoxCourseDuration.Text, comboBoxPType.Text)
                    });
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while trying to update the changes. Please try again",
                                "Update Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DepartmentEditForm_Load(object sender, EventArgs e)
        {
            textBoxDeptShortName.Text = dd.ShortName;
            textBoxCourseDuration.Text = dd.CourseDuration;
            comboBoxPType.Text = dd.ProgrammeType;
            richTextBoxPrograms.Text = string.Join("\r\n", dd.Programmes);
        }
    }
}
