namespace ARMS
{
    partial class ChooseLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChooseLoad));
            this.labelMain = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new ARMS.RibbonMenuButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.button2 = new ARMS.RibbonMenuButton();
            this.button3 = new ARMS.RibbonMenuButton();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelMain
            // 
            this.labelMain.BackColor = System.Drawing.Color.Black;
            this.labelMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelMain.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold);
            this.labelMain.ForeColor = System.Drawing.Color.AliceBlue;
            this.labelMain.Location = new System.Drawing.Point(0, 0);
            this.labelMain.Name = "labelMain";
            this.labelMain.Size = new System.Drawing.Size(570, 62);
            this.labelMain.TabIndex = 4;
            this.labelMain.Text = "You have multiple roles. Please choose an interface to continue.";
            this.labelMain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Controls.Add(this.button3);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(20, 82);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(9);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(543, 178);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button1.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button1.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button1.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button1.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button1.ColorPressStroke = System.Drawing.Color.Aqua;
            this.button1.FadingSpeed = 35;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.button1.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.button1.ImageKey = "(none)";
            this.button1.ImageList = this.imageList1;
            this.button1.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.button1.ImageOffset = 5;
            this.button1.IsPressed = false;
            this.button1.KeepPress = false;
            this.button1.Location = new System.Drawing.Point(18, 18);
            this.button1.Margin = new System.Windows.Forms.Padding(9);
            this.button1.MaxImageSize = new System.Drawing.Point(96, 96);
            this.button1.MenuPos = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(9);
            this.button1.Radius = 6;
            this.button1.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.button1.Size = new System.Drawing.Size(150, 141);
            this.button1.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.button1.SplitDistance = 0;
            this.button1.TabIndex = 3;
            this.button1.Title = "Department Admin";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.buttonClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "female");
            this.imageList1.Images.SetKeyName(1, "female1");
            this.imageList1.Images.SetKeyName(2, "female2");
            this.imageList1.Images.SetKeyName(3, "male");
            this.imageList1.Images.SetKeyName(4, "male1");
            this.imageList1.Images.SetKeyName(5, "male2");
            this.imageList1.Images.SetKeyName(6, "male3");
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button2.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button2.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button2.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button2.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button2.ColorPressStroke = System.Drawing.Color.Aqua;
            this.button2.FadingSpeed = 35;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.button2.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.button2.ImageKey = "(none)";
            this.button2.ImageList = this.imageList1;
            this.button2.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.button2.ImageOffset = 5;
            this.button2.IsPressed = false;
            this.button2.KeepPress = false;
            this.button2.Location = new System.Drawing.Point(186, 18);
            this.button2.Margin = new System.Windows.Forms.Padding(9);
            this.button2.MaxImageSize = new System.Drawing.Point(96, 96);
            this.button2.MenuPos = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(9);
            this.button2.Radius = 6;
            this.button2.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.button2.Size = new System.Drawing.Size(150, 141);
            this.button2.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.button2.SplitDistance = 0;
            this.button2.TabIndex = 3;
            this.button2.Title = "Department Admin";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.buttonClick);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button3.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button3.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button3.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button3.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button3.ColorPressStroke = System.Drawing.Color.Aqua;
            this.button3.FadingSpeed = 35;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.button3.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.button3.ImageKey = "(none)";
            this.button3.ImageList = this.imageList1;
            this.button3.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.button3.ImageOffset = 5;
            this.button3.IsPressed = false;
            this.button3.KeepPress = false;
            this.button3.Location = new System.Drawing.Point(354, 18);
            this.button3.Margin = new System.Windows.Forms.Padding(9);
            this.button3.MaxImageSize = new System.Drawing.Point(96, 96);
            this.button3.MenuPos = new System.Drawing.Point(0, 0);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(9);
            this.button3.Radius = 6;
            this.button3.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.button3.Size = new System.Drawing.Size(150, 141);
            this.button3.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.button3.SplitDistance = 0;
            this.button3.TabIndex = 3;
            this.button3.Title = "Department Admin";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.buttonClick);
            // 
            // ChooseLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.ClientSize = new System.Drawing.Size(570, 288);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.labelMain);
            this.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(577, 326);
            this.Name = "ChooseLoad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Choose Interface to Continue";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelMain;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ImageList imageList1;
        private RibbonMenuButton button1;
        private RibbonMenuButton button2;
        private RibbonMenuButton button3;

    }
}
