namespace ARMS
{
    partial class homeDept
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(homeDept));
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelMode = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolComboLecturer = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.btnLock = new System.Windows.Forms.ToolStripButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.comboBoxLecturer1 = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabHome = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBoxMessages = new System.Windows.Forms.GroupBox();
            this.richTextBoxAnnouncements = new System.Windows.Forms.RichTextBox();
            this.buttonPreviousAnnounce = new System.Windows.Forms.Button();
            this.buttonNextAnnounce = new System.Windows.Forms.Button();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.displayPic = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dataGridActivity = new System.Windows.Forms.DataGridView();
            this.activityDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activityText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageCourses = new System.Windows.Forms.TabPage();
            this.checkedListDepartments = new ARMS.CheckedListCombo();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewCourses = new System.Windows.Forms.DataGridView();
            this.CourseCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.units = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.semester = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonAssignCourse = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonSaveCourses = new System.Windows.Forms.Button();
            this.buttonEditCoures = new System.Windows.Forms.Button();
            this.comboBoxCourse = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabHome.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBoxMessages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).BeginInit();
            this.tabPageCourses.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourses)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.labelMode);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(839, 62);
            this.panel2.TabIndex = 2;
            // 
            // labelMode
            // 
            this.labelMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMode.Font = new System.Drawing.Font("Quartz MS", 18F, System.Drawing.FontStyle.Bold);
            this.labelMode.ForeColor = System.Drawing.Color.Red;
            this.labelMode.Location = new System.Drawing.Point(651, 9);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(176, 39);
            this.labelMode.TabIndex = 4;
            this.labelMode.Text = "ONLINE";
            this.labelMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolComboLecturer,
            this.toolStripButton1,
            this.btnLock});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(3, 2);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(479, 57);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolComboLecturer
            // 
            this.toolComboLecturer.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolComboLecturer.Name = "toolComboLecturer";
            this.toolComboLecturer.Size = new System.Drawing.Size(240, 57);
            this.toolComboLecturer.Text = "Select Lecturer";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::ARMS.Properties.Resources.announcements;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(52, 54);
            this.toolStripButton1.Text = "New Announcement";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // btnLock
            // 
            this.btnLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLock.Image = global::ARMS.Properties.Resources.Lock;
            this.btnLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLock.Name = "btnLock";
            this.btnLock.Size = new System.Drawing.Size(52, 54);
            this.btnLock.Text = "Lock Session";
            this.btnLock.Click += new System.EventHandler(this.btnLock_Click);
            // 
            // comboBoxLecturer1
            // 
            this.comboBoxLecturer1.FormattingEnabled = true;
            this.comboBoxLecturer1.Location = new System.Drawing.Point(189, 44);
            this.comboBoxLecturer1.Name = "comboBoxLecturer1";
            this.comboBoxLecturer1.Size = new System.Drawing.Size(249, 26);
            this.comboBoxLecturer1.TabIndex = 8;
            this.comboBoxLecturer1.Text = "Select Lecturer";
            this.toolTip1.SetToolTip(this.comboBoxLecturer1, "Select the Lecturer 1 for this course");
            this.comboBoxLecturer1.SelectedIndexChanged += new System.EventHandler(this.comboBoxLecturer1_SelectedIndexChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabHome);
            this.tabControl1.Controls.Add(this.tabPageCourses);
            this.tabControl1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.tabControl1.Location = new System.Drawing.Point(3, 63);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(9, 6);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(836, 502);
            this.tabControl1.TabIndex = 11;
            // 
            // tabHome
            // 
            this.tabHome.BackColor = System.Drawing.Color.AliceBlue;
            this.tabHome.Controls.Add(this.panel4);
            this.tabHome.Controls.Add(this.panel6);
            this.tabHome.Location = new System.Drawing.Point(4, 28);
            this.tabHome.Name = "tabHome";
            this.tabHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabHome.Size = new System.Drawing.Size(828, 470);
            this.tabHome.TabIndex = 4;
            this.tabHome.Text = "Home";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.groupBoxMessages);
            this.panel4.Controls.Add(this.labelWelcome);
            this.panel4.Controls.Add(this.displayPic);
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(822, 245);
            this.panel4.TabIndex = 6;
            // 
            // groupBoxMessages
            // 
            this.groupBoxMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMessages.Controls.Add(this.richTextBoxAnnouncements);
            this.groupBoxMessages.Controls.Add(this.buttonPreviousAnnounce);
            this.groupBoxMessages.Controls.Add(this.buttonNextAnnounce);
            this.groupBoxMessages.Location = new System.Drawing.Point(8, 41);
            this.groupBoxMessages.Name = "groupBoxMessages";
            this.groupBoxMessages.Size = new System.Drawing.Size(665, 201);
            this.groupBoxMessages.TabIndex = 3;
            this.groupBoxMessages.TabStop = false;
            this.groupBoxMessages.Text = "Announcements";
            // 
            // richTextBoxAnnouncements
            // 
            this.richTextBoxAnnouncements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxAnnouncements.BackColor = System.Drawing.Color.White;
            this.richTextBoxAnnouncements.Location = new System.Drawing.Point(6, 22);
            this.richTextBoxAnnouncements.Name = "richTextBoxAnnouncements";
            this.richTextBoxAnnouncements.ReadOnly = true;
            this.richTextBoxAnnouncements.Size = new System.Drawing.Size(653, 140);
            this.richTextBoxAnnouncements.TabIndex = 2;
            this.richTextBoxAnnouncements.Text = "No announcements found";
            // 
            // buttonPreviousAnnounce
            // 
            this.buttonPreviousAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPreviousAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonPreviousAnnounce.Location = new System.Drawing.Point(477, 168);
            this.buttonPreviousAnnounce.Name = "buttonPreviousAnnounce";
            this.buttonPreviousAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonPreviousAnnounce.TabIndex = 1;
            this.buttonPreviousAnnounce.Text = "Previous";
            this.buttonPreviousAnnounce.UseVisualStyleBackColor = true;
            // 
            // buttonNextAnnounce
            // 
            this.buttonNextAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonNextAnnounce.Location = new System.Drawing.Point(571, 168);
            this.buttonNextAnnounce.Name = "buttonNextAnnounce";
            this.buttonNextAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonNextAnnounce.TabIndex = 1;
            this.buttonNextAnnounce.Text = "Next";
            this.buttonNextAnnounce.UseVisualStyleBackColor = true;
            // 
            // labelWelcome
            // 
            this.labelWelcome.BackColor = System.Drawing.Color.Transparent;
            this.labelWelcome.Font = new System.Drawing.Font("Trebuchet MS", 18F);
            this.labelWelcome.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelWelcome.Location = new System.Drawing.Point(3, 4);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(488, 33);
            this.labelWelcome.TabIndex = 1;
            this.labelWelcome.Text = "Good day Mr. UNN. ";
            this.labelWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // displayPic
            // 
            this.displayPic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.displayPic.Image = global::ARMS.Properties.Resources.UNN_Logo1;
            this.displayPic.Location = new System.Drawing.Point(679, 51);
            this.displayPic.Name = "displayPic";
            this.displayPic.Size = new System.Drawing.Size(138, 185);
            this.displayPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.displayPic.TabIndex = 0;
            this.displayPic.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.AliceBlue;
            this.panel6.Controls.Add(this.dataGridActivity);
            this.panel6.Location = new System.Drawing.Point(0, 254);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(5);
            this.panel6.Size = new System.Drawing.Size(825, 213);
            this.panel6.TabIndex = 7;
            // 
            // dataGridActivity
            // 
            this.dataGridActivity.AllowUserToAddRows = false;
            this.dataGridActivity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridActivity.BackgroundColor = System.Drawing.Color.White;
            this.dataGridActivity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridActivity.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridActivity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridActivity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.activityDate,
            this.activityText});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridActivity.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridActivity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridActivity.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridActivity.EnableHeadersVisualStyles = false;
            this.dataGridActivity.GridColor = System.Drawing.Color.White;
            this.dataGridActivity.Location = new System.Drawing.Point(5, 5);
            this.dataGridActivity.MultiSelect = false;
            this.dataGridActivity.Name = "dataGridActivity";
            this.dataGridActivity.ReadOnly = true;
            this.dataGridActivity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridActivity.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridActivity.Size = new System.Drawing.Size(815, 203);
            this.dataGridActivity.TabIndex = 3;
            this.dataGridActivity.TabStop = false;
            // 
            // activityDate
            // 
            this.activityDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.activityDate.DividerWidth = 1;
            this.activityDate.FillWeight = 25F;
            this.activityDate.HeaderText = "Date";
            this.activityDate.MinimumWidth = 150;
            this.activityDate.Name = "activityDate";
            this.activityDate.ReadOnly = true;
            this.activityDate.Width = 160;
            // 
            // activityText
            // 
            this.activityText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.activityText.HeaderText = "Details";
            this.activityText.Name = "activityText";
            this.activityText.ReadOnly = true;
            // 
            // tabPageCourses
            // 
            this.tabPageCourses.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageCourses.Controls.Add(this.checkedListDepartments);
            this.tabPageCourses.Controls.Add(this.groupBox2);
            this.tabPageCourses.Controls.Add(this.label5);
            this.tabPageCourses.Controls.Add(this.buttonAssignCourse);
            this.tabPageCourses.Controls.Add(this.panel5);
            this.tabPageCourses.Controls.Add(this.comboBoxLecturer1);
            this.tabPageCourses.Controls.Add(this.comboBoxCourse);
            this.tabPageCourses.Location = new System.Drawing.Point(4, 28);
            this.tabPageCourses.Name = "tabPageCourses";
            this.tabPageCourses.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCourses.Size = new System.Drawing.Size(828, 470);
            this.tabPageCourses.TabIndex = 3;
            this.tabPageCourses.Text = "Manage Courses";
            // 
            // checkedListDepartments
            // 
            this.checkedListDepartments.BackColor = System.Drawing.Color.White;
            this.checkedListDepartments.ComboText = "Choose Departments";
            this.checkedListDepartments.Location = new System.Drawing.Point(460, 44);
            this.checkedListDepartments.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.checkedListDepartments.Name = "checkedListDepartments";
            this.checkedListDepartments.Size = new System.Drawing.Size(205, 21);
            this.checkedListDepartments.TabIndex = 13;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dataGridViewCourses);
            this.groupBox2.Location = new System.Drawing.Point(3, 98);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(817, 309);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Manage Department Courses";
            // 
            // dataGridViewCourses
            // 
            this.dataGridViewCourses.AllowUserToAddRows = false;
            this.dataGridViewCourses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCourses.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCourses.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewCourses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCourses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CourseCode,
            this.CourseTitle,
            this.units,
            this.semester});
            this.dataGridViewCourses.Location = new System.Drawing.Point(10, 29);
            this.dataGridViewCourses.Name = "dataGridViewCourses";
            this.dataGridViewCourses.Size = new System.Drawing.Size(804, 276);
            this.dataGridViewCourses.TabIndex = 2;
            this.dataGridViewCourses.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewCourses_CellBeginEdit);
            this.dataGridViewCourses.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCourses_CellEndEdit);
            // 
            // CourseCode
            // 
            this.CourseCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CourseCode.DividerWidth = 1;
            this.CourseCode.FillWeight = 30F;
            this.CourseCode.HeaderText = "Course Code";
            this.CourseCode.Name = "CourseCode";
            // 
            // CourseTitle
            // 
            this.CourseTitle.DividerWidth = 1;
            this.CourseTitle.FillWeight = 70F;
            this.CourseTitle.HeaderText = "Course Title";
            this.CourseTitle.Name = "CourseTitle";
            // 
            // units
            // 
            this.units.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.units.FillWeight = 10F;
            this.units.HeaderText = "Units";
            this.units.Name = "units";
            this.units.Width = 64;
            // 
            // semester
            // 
            this.semester.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.semester.FillWeight = 10F;
            this.semester.HeaderText = "Semester";
            this.semester.Name = "semester";
            this.semester.Width = 90;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 18);
            this.label5.TabIndex = 12;
            this.label5.Text = "Assign Courses:";
            // 
            // buttonAssignCourse
            // 
            this.buttonAssignCourse.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAssignCourse.BackColor = System.Drawing.Color.Transparent;
            this.buttonAssignCourse.ForeColor = System.Drawing.Color.Black;
            this.buttonAssignCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAssignCourse.ImageKey = "cap_graduate.png";
            this.buttonAssignCourse.ImageList = this.imageList1;
            this.buttonAssignCourse.Location = new System.Drawing.Point(687, 35);
            this.buttonAssignCourse.Name = "buttonAssignCourse";
            this.buttonAssignCourse.Padding = new System.Windows.Forms.Padding(6);
            this.buttonAssignCourse.Size = new System.Drawing.Size(128, 42);
            this.buttonAssignCourse.TabIndex = 10;
            this.buttonAssignCourse.Text = "(Un)&Assign";
            this.buttonAssignCourse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAssignCourse.UseVisualStyleBackColor = false;
            this.buttonAssignCourse.Click += new System.EventHandler(this.buttonAssignCourse_Click_1);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "print_48.png");
            this.imageList1.Images.SetKeyName(1, "cap_graduate.png");
            this.imageList1.Images.SetKeyName(2, "pencil.png");
            this.imageList1.Images.SetKeyName(3, "edit_add.png");
            this.imageList1.Images.SetKeyName(4, "edit_remove.png");
            this.imageList1.Images.SetKeyName(5, "apply.png");
            this.imageList1.Images.SetKeyName(6, "vcalendar.png");
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.buttonSaveCourses);
            this.panel5.Controls.Add(this.buttonEditCoures);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(3, 411);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(822, 56);
            this.panel5.TabIndex = 1;
            // 
            // buttonSaveCourses
            // 
            this.buttonSaveCourses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveCourses.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSaveCourses.BackColor = System.Drawing.Color.Transparent;
            this.buttonSaveCourses.ForeColor = System.Drawing.Color.Black;
            this.buttonSaveCourses.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSaveCourses.ImageKey = "apply.png";
            this.buttonSaveCourses.ImageList = this.imageList1;
            this.buttonSaveCourses.Location = new System.Drawing.Point(666, 8);
            this.buttonSaveCourses.Name = "buttonSaveCourses";
            this.buttonSaveCourses.Padding = new System.Windows.Forms.Padding(6);
            this.buttonSaveCourses.Size = new System.Drawing.Size(148, 43);
            this.buttonSaveCourses.TabIndex = 8;
            this.buttonSaveCourses.Text = "&Save Changes";
            this.buttonSaveCourses.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSaveCourses.UseVisualStyleBackColor = false;
            this.buttonSaveCourses.Click += new System.EventHandler(this.buttonSaveCourses_Click);
            // 
            // buttonEditCoures
            // 
            this.buttonEditCoures.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonEditCoures.BackColor = System.Drawing.Color.Transparent;
            this.buttonEditCoures.ForeColor = System.Drawing.Color.Black;
            this.buttonEditCoures.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEditCoures.ImageKey = "pencil.png";
            this.buttonEditCoures.ImageList = this.imageList1;
            this.buttonEditCoures.Location = new System.Drawing.Point(10, 8);
            this.buttonEditCoures.Name = "buttonEditCoures";
            this.buttonEditCoures.Padding = new System.Windows.Forms.Padding(6);
            this.buttonEditCoures.Size = new System.Drawing.Size(90, 43);
            this.buttonEditCoures.TabIndex = 4;
            this.buttonEditCoures.Text = "&Edit";
            this.buttonEditCoures.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEditCoures.UseVisualStyleBackColor = false;
            this.buttonEditCoures.Click += new System.EventHandler(this.buttonEditCoures_Click);
            // 
            // comboBoxCourse
            // 
            this.comboBoxCourse.FormattingEnabled = true;
            this.comboBoxCourse.Location = new System.Drawing.Point(23, 44);
            this.comboBoxCourse.Name = "comboBoxCourse";
            this.comboBoxCourse.Size = new System.Drawing.Size(148, 26);
            this.comboBoxCourse.TabIndex = 9;
            this.comboBoxCourse.Text = "Select Course";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.BalloonTipText = "A robust result management solution for Educational Institutions";this.notifyIcon1.BalloonTipTitle = "Automated Result Management System 1.0";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
            this.notifyIcon1.BalloonTipShown += new System.EventHandler(this.notifyIcon1_BalloonTipShown);
            // 
            // homeDept
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(839, 566);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(670, 473);
            this.Name = "homeDept";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Department Administration Panel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.homeDept_Load);
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabHome.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBoxMessages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).EndInit();
            this.tabPageCourses.ResumeLayout(false);
            this.tabPageCourses.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourses)).EndInit();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageCourses;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TabPage tabHome;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button buttonEditCoures;
        private System.Windows.Forms.ToolStripComboBox toolComboLecturer;
        private System.Windows.Forms.DataGridView dataGridViewCourses;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn units;
        private System.Windows.Forms.DataGridViewTextBoxColumn semester;
        private System.Windows.Forms.Button buttonSaveCourses;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonAssignCourse;
        private System.Windows.Forms.ComboBox comboBoxLecturer1;
        private System.Windows.Forms.ComboBox comboBoxCourse;
        private CheckedListCombo checkedListDepartments;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBoxMessages;
        private System.Windows.Forms.RichTextBox richTextBoxAnnouncements;
        private System.Windows.Forms.Button buttonPreviousAnnounce;
        private System.Windows.Forms.Button buttonNextAnnounce;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.PictureBox displayPic;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridView dataGridActivity;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityText;
        private System.Windows.Forms.ToolStripButton btnLock;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

