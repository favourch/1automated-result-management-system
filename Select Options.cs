using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ARMS
{
    /// <summary>
    /// A simple CheckedListBox window that enables multiple items to be selected and returns the 
    /// list of items that have been selected. This could replace the CheckedListCombo control in the project
    /// </summary>
    public partial class Select_Options : Form
    {
        /// <summary>
        /// {ublic constructore for the select_Options window. This initializes the Select_Options form using the specified dictionary.
        /// </summary>
        /// <param name="options">A dictionary defining the options to be added to the control, the boolean specifies whether item is checked</param>
        public Select_Options(Dictionary<string, bool> options)
        {
            InitializeComponent();
            foreach (var s in options)
            {
                checkedListBox1.Items.Add(s.Key, s.Value);
            }
        }
        
        /// <summary>
        /// The dictionary collection of the checked items data in the Select_Options Form.
        ///  You can use this to retrieve the items that were checked after the user closes the form
        /// </summary>
        public Dictionary<string, bool> items = new Dictionary<string,bool>();

        private void buttonProceed_Click(object sender, EventArgs e)
        {
            foreach (var i in checkedListBox1.Items)
            {
                items.Add(i.ToString(), checkedListBox1.CheckedItems.Contains(i));
            }
            Close();
        }
    }
}
