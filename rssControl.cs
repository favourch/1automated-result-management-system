using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Linq;

namespace ARMS
{
    public partial class rssControl : UserControl
    {
        
        XmlTextReader rssReader;
        XmlDocument rssDoc;
        XmlNode nodeRss;
        XmlNode nodeChannel;
        XmlNode nodeItem;
        ListViewItem rowNews;
        public rssControl()
        {
            InitializeComponent();
        }

        private void rssControl_Load(object sender, EventArgs e)
        {
            try
            {
                foreach (var VARIABLE in list)
                {
                    string url = VARIABLE; //"http://www.geekpedia.com/gp_programming.xml";
                    lstNews.Items.Clear();
                    this.Cursor = Cursors.WaitCursor;
                    // Create a new XmlTextReader from the specified URL (RSS feed)
                    rssReader = new XmlTextReader(url);
                    rssDoc = new XmlDocument();
                    // Load the XML content into a XmlDocument
                    rssDoc.Load(rssReader);

                    // Loop for the <rss> tag
                    for (int i = 0; i < rssDoc.ChildNodes.Count; i++)
                    {
                        // If it is the rss tag
                        if (rssDoc.ChildNodes[i].Name == "rss")
                        {
                            // <rss> tag found
                            nodeRss = rssDoc.ChildNodes[i];
                        }
                    }

                    // Loop for the <channel> tag
                    for (int i = 0; i < nodeRss.ChildNodes.Count; i++)
                    {
                        // If it is the channel tag
                        if (nodeRss.ChildNodes[i].Name == "channel")
                        {
                            // <channel> tag found
                            nodeChannel = nodeRss.ChildNodes[i];
                        }
                    }


                    for (int i = 0; i < nodeChannel.ChildNodes.Count; i++)
                    {
                        // If it is the item tag, then it has children tags which we will add as items to the ListView
                        if (nodeChannel.ChildNodes[i].Name == "item")
                        {
                            nodeItem = nodeChannel.ChildNodes[i];

                            // Create a new row in the ListView containing information from inside the nodes
                            rowNews = new ListViewItem();
                            rowNews.Text = nodeItem["title"].InnerText;
                            rowNews.SubItems.Add(nodeItem["link"].InnerText);
                            lstNews.Items.Add(rowNews);
                        }
                    }

                    this.Cursor = Cursors.Default;
                }

            }
            catch(Exception){}
        }

        List<string> list = new List<string>();
        private void getLecturerCourse()
        {
           
          XElement element=XElement.Load("rssLink.xml");

            foreach (var VARIABLE in Helper.GetLecturerCourses("lecturerName"))
            {
                
                 var rs=  (from a in element.Elements("Link") where a.Element("course").Value == VARIABLE select a).ToList();
                foreach (var xElement in rs)
                {
                    list.Add(xElement.Element("value").ToString());
                }
            }


        }
    }
}
