using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ARMS
{
    static class Program
    {        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {/*
            string list = "";
            DirectoryInfo di = new DirectoryInfo(@"C:\Robosys\Sed\");
            foreach (var fi in di.GetFiles())
            {
                var lines = File.ReadAllLines(fi.FullName);
               list += lines[4].Substring(lines[4].IndexOf(":")) + ",";
            }
            File.WriteAllText(@"C:\Robosys\sedoutput.txt", list.Remove(list.Length - 1));
            System.Diagnostics.Process.Start(@"C:\Robosys\sedoutpupt.txt");*/

/*
            string lgalist = @"'Ika','IO' => 'Ikono','RE' => 'Ikot Ekpene','TY' => 'Ikot Abasi','FG' => 'Ini','IY' => 'Itu','MF' => 'Mbo','KT' => 'Mkpat Enin','NS' => 'Nsit Atai','UT' => 'Nsit Ibom','SD' => 'Nsit Ubium','TO' => 'Obot Akara','BO' => 'Okobo','AN' => 'Onna','RO' => 'Oron','TR' => 'Oruk Anam','DS' => 'Udung Uko','CX' => 'Ukanafun','XZ' => 'Uruan','XC' => 'Urue-Offong/Oruko','WZ' => 'Uyo'),'Anambra' => array('WA' => 'Aguata','WS' => 'Anambra East','WR' => 'Anambra West','OC' => 'Anaocha','WY' => 'Awka North ','WU' => 'Awka South','WI' => 'Ayamelum','QD' => 'Dunukofia','QG' => 'Ekwusigo','QA' => 'Idemili North','QH' => 'Idemili South','WK' => 'Ihiala','QL' => 'Njikoka','QI' => 'Nnewi North','QU' => 'Nnewi South','QB' => 'Ogbaru','LK' => 'Onitsha North','LH' => 'Onitsha South','KF' => 'Orumba North',
            'KD' => 'Orumba South','LS' => 'Oyi'),'Bauchi' => array('z' => 'Alkaleri','y' => 'Bauchi','w' => 'Bogoro','x' => 'Damban','i' => 'Darazo','k' => 'Dass','h' => 'Ganjuwa','j' => 'Giade','v' => 'Itas/Gadau','m' => 'Jama-are','o' => 'Katagum','f' => 'Kirfi','q' => 'Misau','u' => 'Ningi','p' => 'Shira','s' => 'Tafawa-Balewa','g' => 'Toro','j' => 'Warji','n' => 'Zaki'),'Bayelsa' => array('pi' => 'Brass','ji' => 'Ekeremor','ti' => 'Kolokuma/Opokuma','gi' => 'Nembe','ai' => 'Ogbia','fi' => 'Sagbama','di' => 'South Jaw','yi' => 'Yenegoa'),'Benue' => array('do' => 'Ado','wo' => 'Aguta','qo' => 'Apa','ro' => 'Buruku','ao' => 'Gboko','fo' => 'Guma','co' => 'Gwer East','ho' => 'Gwer West','ro' => 'Katsina-Ala','io' => 'Konshisha','po' => 'Kwande','yo' => 'Logo','jo' => 'Makurdi','to' => 'Obi','yo' => 'Ogbadibo','uo' => 'Oju','ko' => 'Okpokwu','ho' => 'Ohimini','lo' => 'Oturkpo','uo' => 'Tarka','zo' => 'Ukum','no' => 'Ushongo','xo' => 'Vandeikya'),'Bornu' => array('qc' => 'Abadam','wc' => 'Askira/Uba','ec' => 'Bama','rc' => 'Bayo','tc' => 'Biu','yc' => 'Chibok','uc' => 'Damboa','ic' => 'Dikwa','oc' => 'Gubio','pc' => 'Guzamala','ac' => 'Gwoza','sc' => 'Hawul','dc' => 'Jere','fc' => 'Kaga','gc' => 'Kala/Balge','hc' => 'Konduga','jc' => 'Kukawa','kc' => 'Kwaya Kusar','lc' => 'Mafa','zc' => 'Magumeri','xc' => 'Maiduguri','cc' => 'Marte','vc' => 'Mobbar','bc' => 'Monguno','nc' => 'Ngala','mc' => 'Nganzai','cr' => 'Shani'),'Cross River' => array('qr' => 'Akpabuyo','wr' => 'Odukpani','er' => 'Akamkpa','tr' => 'Biase','yr' => 'Abi','ur' => 'Ikom','ur' => 'Yarkur',
            'ir' => 'Odubra','or' => 'Boki','pr' => 'Ogoja','ar' => 'Yala','dr' => 'Obanliku','fr' => 'Obudu','gr' => 'Calabar South','hr' => 'Etung','jr' => 'Bekassi','kr' => 'Calabar Municipality'),'Delta' => array('qt' => 'Oshimili','wt' => 'Aniocha','et' => 'Aniocha South','rt' => 'Ika South','tt' => 'Ika North-East','yt' => 'Ndokwa West','ut' => 'Ndokwa East','it' => 'Isoko South','ot' => 'Isoko North','pt' => 'Bomadi','at' => 'Burutu','st' => 'Ughelli South','dt' => 'Ughelli North','ft' => 'Ethiope West','gt' => 'Ethiope East','ht' => 'Sapele','jt' => 'Okpe','kt' => 'Warri North','lt' => 'Warri South','zt' => 'Uvwie','xt' => 'Udu','ct' => 'Warri Central','vt' => 'Ukwani','bt' => 'Oshimili North','nt' => 'Patani'),'Ebonyi' => array('qy' => 'Afikpo South','wy' => 'Afikpo North','ey' => 'Onicha','ry' => 'Ohaozara','ty' => 'Abakaliki','yy' => 'Ishielu','uy' => 'Ikwo',
            'iy' => 'Ezza','oy' => 'Ezza South','oy' => 'Ohaukwu','py' => 'Ebonyi','ay' => 'Ivo'),'Edo' => array ('sy' => 'Esan North-East','dy' => 'ESan Central','fy' => 'Esan West','gy' => 'Egor','hy' => 'Ukpoba','jy' => 'Central','ky' => 'Etsako Central','ly' => 'Igueben','zy' => 'Oredo','xy' => 'Ovia SouthWest','cy' => 'Ovia SouthEast','vy' => 'Orhionwon','by' => 'Uhunmwonde','ny' => 'Etsako East','my' => 'Esan SouthEast'),'Ekiti' => array('ct' => 'Ado','vt' => 'Ekiti East','bt' => 'Ekiti West','nt' => 'Emure/Ise/Orun','mt' => 'Ekiti SouthWest','qu' => 'Ikare','wu' => 'Irepodun','eu' => 'Ijero','ru' => 'Ido/Osi','tu' => 'Oye','yu' => 'Ikole','ou' => 'Moba','pu' => 'Gbonyin','au' => 'Efon','su' => 'Ise/Orun','du' => 'Ilejemeje'),'Enugu' => array('fu' => 'Enugu South','gu' => 'Igbo Eze South','hu' => 'Enugu North','ju' => 'Nkanu','ku' => 'Udi Agwu','lu' => 'Oji River','zu' => 'Ezeagu','xu' => 'IgboEze North','cu' => 'Isi Uzo','vu' => 'Nsukka','bu' => 'Igbo Ekiti','nu' => 'Uzo Uwani','mu' => 'Enugu East','qi' => 'Aninri',
            'wi' => 'Nkanu East','ei' => 'Udenu'),'Gombe' => array('ri' => 'Akko','ti' => 'Balanga','yi' => 'Billiri','ui' => 'Dukku','ii' => 'Kaltungo','oi' => 'Kwami','ai' => 'Shomgom','si' => 'Funakaye','di' => 'Gombe','fi' => 'Nafada/Bajoga','gi' => 'Yamaltu/Delta'),'Imo' => array('hi' => 'Aboh Mbaise','ji' => 'Ahiazu Mbaise','ki' => 'Ehime Mbano','li' => 'Ezinihitte','zi' => 'Ideato North','xi' => 'Ideato South','ci' => 'Ihitte Uboma','vi' => 'Ikeduru','bi' => 'Isiala Mbano','ni' => 'Isu','mi' => 'Mbaitoli','qp' => 'Mbaitolu','wp' => 'Ngor Okpala','ep' => 'Njaba','rp' => 'Nwangele','tp' => 'Nkwerre','yp' => 'Obowo','up' => 'Oguta','ip' => 'Ohaji Egbema','op' => 'Okigwe','pp' => 'Orlu','ap' => 'Orsu','sp' => 'Oru East','dp' => 'Oru West','fp' => 'Owerri Municipal','gp' => 'Owerri North','hp' => 'Owerri West'),'Jigawa' => array('jp' => 'Auyo','kp' => 'Babura','lp' => 'Birni Kudu','zp' => 'Biriniwa','xp' => 'Buji','cp' => 'Dutse','vp' => 'Gagarawa','bp' => 'Garki','np' => 'Gumel','mp' => 'Guri','qa' => 'Gwaram','wa' => 'Gwiwa','ea' => 'Hadejia','ra' => 'Jahun','ta' => 'Kafin Hausa','ta' => 'Kaugama Kazaure','ya' => 'Kiri Kasamma','ua' => 'Kiyawa','ia' => 'Maigatari','oa' => 'Malam Madori','pa' => 'Miga','aa' => 'Ringim','sa' => 'Roni','da' => 'Sule Tankarkar','fa' => 'Taura','ga' => 'Yankwashi'),'Kaduna' => array('ha' => 'Birni','ja' => 'Chikun','ka' => 'Giwa','la' => 'Igabi','za' => 'Ikara','xa' => 'Jaba','ca' => 'Jema a','va' => 'Kachia','ba' => 'Kaduna North','na' => 'Kaduna South',
            'ma' => 'Kagarko','qs' => 'Kajuru','ws' => 'Kaura','es' => 'Kauru','rs' => 'Kabau','ts' => 'Kudan','ys' => 'Lere','us' => 'Makarfi','is' => 'Sabon Gari','os' => 'Sanga','ps' => 'Soba','as' => 'Zango Kataf','ss' => 'Zaria'),'Kano' => array('ds' => 'Ajingi','fs' => 'Albasu','gs' => 'Bagwai','hs' => 'Bebeji','js' => 'Bichi','ks' => 'Bunkure','ls' => 'Dala','zs' => 'Dambatta','xs' => 'Dawakin Kudu','cs' => 'Dawakin Tofa','vs' => 'Doguwa','bs' => 'Fagge','ns' => 'Gabasawa','ms' => 'Garko','qd' => 'Garum','wd' => 'Mallam','ed' => 'Gaya','rd' => 'Gezawa','td' => 'Gwale','yd' => 'Gwarzo','ud' => 'Kabo','id' => 'Kano Municipal','od' => 'Karaye','pd' => 'Kibiya','ad' => 'Kiru','sd' => 'Kumbotso','dd' => 'Kunchi','fd' => 'Kura','gd' => 'Madobi','hd' => 'Makoda','jd' => 'Minjibir','kd' => 'Nasarawa','ld' => 'Rano','zd' => 'Rimin Gado','xd' => 'Rogo','cd' => 'Shanono','vd' => 'Sumaila','bd' => 'Takali','nd' => 'Tarauni','md' => 'Tofa','qf' => 'Tsanyawa','wf' => 'Tudun Wada','ef' => 'Ungogo','rf' => 'Warawa','tf' => 'Wudil'),'Kastina' => array('yf' => 'Bakori','uf' => 'Batagarawa',
            'uf' => 'Batsari','if' => 'Baure','of' => 'Bindawa','pf' => 'Charanchi','af' => 'Dandume','sf' => 'Danja','df' => 'Dan Musa','ff' => 'Daura','gf' => 'Dutsi','hf' => 'Dutsin Ma','jf' => 'Faskari','kf' => 'Funtua','lf' => 'Ingawa','zf' => 'Jibia','xf' => 'Kafur','cf' => 'Kaita','vf' => 'Kankara','bf' => 'Kankia','nf' => 'Katsina','mf' => 'Kurfi','qg' => 'Kusada','wg' => 'Mai Adua','eg' => 'Malumfashi','rg' => 'Mani','tg' => 'Mashi','yg' => 'Matazuu','ug' => 'Rimi','ig' => 'Sabuwa','og' => 'Safana','pg' => 'Sandamu','ag' => 'Zango','sg' => 'Aleiro','dg' => 'Arewa Dandi','fg' => 'Argungu','gg' => 'Augie','hg' => 'Bagudo','jg' => 'Birnin Kebbi','kg' => 'Bunza','lg' => 'Dandi','zg' => 'Fakai','xg' => 'Gwandu','cg' => 'Jega','vg' => 'Kalgo','bg' => 'Koko Besse','ng' => 'Maiyama','mg' => 'Ngaski','qh' => 'Sakaba','wh' => 'Shanga',
            'eh' => 'Suru','rh' => 'Wasagu Danko','th' => 'Yauri','yh' => 'Zuru'),'Kogi' => array('uh' => 'Adavi','ih' => 'Ajaokuta','oh' => 'Ankpa','ph' => 'Bassa','ah' => 'Dekina','sh' => 'Ibaji','dh' => 'Idah','fh' => 'Igalamela Odulu','gh' => 'Ijumu','hh' => 'Kabba Bunu','jh' => 'Kogi','kh' => 'Lokoja','lh' => 'Mopa Muro','zh' => 'Ofu','xh' => 'Ogori Mangongo','ch' => 'Okehi','vh' => 'Okene','bh' => 'Olamabolo','nh' => 'Omala','mh' => 'Yagba East','qj' => 'Yagba West'),'Kwara' => array('wj' => 'Asa','ej' => 'Baruten','rj' => 'Edu','tj' => 'Ekiti','yj' => 'Ifelodun','uj' => 'Ilorin East','ij' => 'Ilorin West','oj' => 'Irepodun','pj' => 'Isin','aj' => 'Kaiama','sj' => 'Moro','dj' => 'Offa','fj' => 'Oke Ero','gj' => 'Oyun','hj' => 'Pategi'),'Lagos' => array('jj' => 'Agege','kj' => 'Ajeromi Ifelodun','lj' => 'Alimosho','zj' => 'Amuwo Odofin','xj' => 'Apapa','cj' => 'Badagry','vj' => 'Epe','bj' => 'Eti Osa','nj' => 'Ibeju Lekki','mj' => 'Ifako Ijaye','qk' => 'Ikeja','wk' => 'Ikorodu','ek' => 'Kosofe','rk' => 'Lagos Island','tk' => 'Lagos Mainland','yk' => 'Mushin','uk' => 'Ojo','ik' => 'Ishodi Osolo','ok' => 'Shomolu','pk' => 'Shurulere'),'Nasarawa' => array('ak' => 'Akwanga','sk' => 'Awe','dk' => 'Doma','fk' => 'Karu','gk' => 'Keana','hk' => 'Keffi','jk' => 'Kokona','kk' => 'Lafia','lk' => 'Nasarawa','zk' => 'Nasarawa Eggon','xk' => 'Obi','ck' => 'Toto','vk' => 'Wamba'),'Niger' => array('bk' => 'Agaie','nk' => 'Agwara','mk' => 'Bida','ql' => 'Borgu','wl' => 'Bosso','el' => 'Chanchaga','rl' => 'Edati','tl' => 'Gbako','yl' => 'Gurara','ul' => 'Katcha','il' => 'Kontagora','ol' => 'Lapai','pl' => 'Lavun','al' => 'Magama','sl' => 'Mariga','dl' => 'Meshegu','fl' => 'Mokwa','gl' => 'Muya','jl' => 'Pailoro',
            'hl' => 'Rafi','jl' => 'Rijau','kl' => 'Shiroro','ll' => 'Suleja','zl' => 'Tafa','xl' => 'Wushishi'),'Ogun' => array('cl' => 'Abeokuta North','vl' => 'Abeokuta South','bl' => 'Ado Odo Ota','nl' => 'Egbado North','ml' => 'Egbado South','qz' => 'Ewekoro','wz' => 'Ifo','ez' => 'Ijebu East','rz' => 'Ijebu North','tz' => 'Ijebu North East','yz' => 'Ijebu Ode','uz' => 'Ikenna','iz' => 'Imeko Afon','oz' => 'Ipokia','pz' => 'Obafemi Owode','az' => 'Ogun Waterside','sz' => 'Odeda','dz' => 'Odogbolu','fz' => 'Remo North','gz' => 'Shagamu'),'Ondo' => array('hz' => 'Akoko North East','jz' => 'Akoko North West','kz' => 'Akoko South Akure East','do' => 'Akoko South West','hg' => 'Akure North','pp' => 'Akure South','ap' => 'Ese Odo','sp' => 'Idanre','dp' => 'Ifedore','fp' => 'Ilaje','gp' => 'Ile Oluji','hp' => 'Okeigbo','lz' => 'Irele','kp' => 'Odigbo','lp' => 'Okitipupa','zp' => 'Ondo East','xp' => 'Ondo West','cp' => 'Ose','vp' => 'Owo'),'Osun' => array ('zz' => 'Aiyedade','mp' => 'Aiyedire','qa' => 'Atakumosa East','ca' => 'Atakumosa West','ea' => 'Boluwaduro','ra' => 'Boripe','ta' => 'Ede North','xz' => 'Ede South','cz' => 'Egbedore','vz' => 'Ejigbo','ia' => 'Ife Central','oa' => 'Ife East','rb' => 'Ife North','aa' => 'Ife South','sa' => 'Ifedayo','da' => 'Ifelodun','nz' => 'Ila','ga' => 'Ilesha East','ha' => 'Ilesha West','ja' => 'Irepodun','ka' => 'Irewole','la' => 'Isokan','mz' => 'Iwo','qx' => 'Obokun','ca' => 'Odo Otin','ac' => 'Ola Oluwa','ex' => 'Olorunda','rx' => 'Oriade',
            'tx' => 'Orolu','yx' => 'Osogbo'),'Oyo' => array('ux' => 'Afijio','ix' => 'Akinyele','ts' => 'Atiba','ix' => 'Atigbo','oz' => 'Egbeda','px' => 'Ibadan Central','ax' => 'Ibadan North','sx' => 'Ibadan North West','dx' => 'Ibadan South East','hx' => 'Ibadan South West','ds' => 'Ibarapa Central','fs' => 'Ibarapa East','jc' => 'Ibarapa North','jx' => 'Ido','js' => 'Irepo','qn' => 'Iseyin','lc' => 'Itesiwaju','zs' => 'Iwajowa','xs' => 'Kajola','cs' => 'Lagelu Ogbomosho North','vs' => 'Ogbomosho South','bs' => 'Ogo Oluwa','ns' => 'Olorunsogo','vs' => 'Oluyole','qd' => 'Ona Ara','wd' => 'Orelope','ed' => 'Ori Ire','rd' => 'Oyo East','uc' => 'Oyo West','yd' => 'Saki East','ud' => 'Saki West','id' => 'Surulere'),'Plateau' => array('hn' => 'Barikin Ladi','qv' => 'Bassa','wv' => 'Bokkos','ev' => 'Jos East','rv' => 'Jos North','tv' => 'Jos South','hd' => 'Kanam','jd' => 'Kanke','kd' => 'Langtang North','ld' => 'Langtang South','zd' => 'Mangu','xd' => 'Mikang','cd' => 'Pankshin','vd' => 'Qua an Pan','bd' => 'Riyom','nd' => 'Shendam','md' => 'Wase'),'Rivers' => array('wf' => 'Aua Odual','ef' => 'Ahoada East','rf' => 'Ahoada West','tf' => 'Akuku Toru','yf' => 'Andoni','db' => 'Asari Toru',
            'uf' => 'Bonny','if' => 'Degema','of' => 'Emohua','pf' => 'Elema','af' => 'Etche','sf' => 'Gokana','df' => 'Ikwerre','ff' => 'Khana','gf' => 'Obia Akpor','hf' => 'Ogba Egbema Ndoni','jf' => 'Ogu Bolo','qn' => 'Okrika','on' => 'Omumma','of' => 'Opobo Nkoro','os' => 'Oyigbo','nw' => 'Port Harcourt','ug' => 'Tai'),'Sokoto' => array('uw' => 'Binji','un' => 'Bodinga','us' => 'Dange Shnsi','uo' => 'Gada','jg'=> 'Goronyo','dc'=> 'Gudu','gy'=> 'Gawabawa','bg'=> 'Illela','vf'=>'Isa','se' => 'Kware','as' => 'Kebbe','hf' => 'Rabah','fi' => 'Sabon birni','eo' => 'Shagari','cx' => 'Silame','kh' => 'Sokoto North','kj' => 'Sokoto South ','lm' => 'Tambuwal','jf' => 'Tqngaza','kd' => 'Tureta','ng' => 'Wamako','df' => 'Wurno','bn' => 'Yabo'),'Taraba' => array('n' => 'Ardo Kola','b' => 'Bali','s' => 'Donga','e' => 'Gashaka','h' => 'Cassol','s' => 'Ibi','o' => 'Jalingo','s' => 'Karin Lamido','s' => 'Kurmi','o' => 'Lau','s' => 'Sardauna','w' => 'Takum','n' => 'Ussa','c' => 'Wukari',
            'f' => 'Yorro','i' => 'Zing'),'Yobe' => array('bs' => 'Bade','sk' => 'Bursari','kw' => 'Damaturu','ow' => 'Fika','vd' => 'Fune','sk' => 'Geidam','sd' => 'Gujba','vs' => 'Gulani','sa' => 'Jakusko','ee' => 'Karasuwa','vd' => 'Karawa','lg' => 'Machina','hr' => 'Nangere','je' => 'Nguru Potiskum','ke' => 'Tarmua','je' => 'Yunusari','ns' => 'Yusufari'),'Zamfara' => array('lw' => 'Anka','ds' => 'Bakura','ke' => 'Birnin Magaji','jd' => 'Bukkuyum','jh' => 'Bungudu','kw' => 'Gummi','so' => 'Gusau','ow' => 'Kaura','kl' => 'Namoda','wl' => 'Maradun','lw' => 'Maru','kw' => 'Shinkafi','ke' => 'Talata Mafara','me' => 'Tsafe','md' => 'Zurmi'))";
            string newlist = "";
            foreach (string s in lgalist.Split(','))
            {
                if (s.Contains("=>"))
                {
                    if (s.Contains("array"))
                    {
                        string add = s.Remove(s.IndexOf("(") + 1) + s.Substring(s.LastIndexOf("=>") + 2) + ",";
                        newlist += add;
                        continue;
                    }
                    string news = s.Substring(s.IndexOf("=>") + 2);
                    newlist += news + ",";
                }
                else
                {
                    newlist += s + ",";
                }
            }
*/
            //File.WriteAllText("connection.net.settings", Crypto.Encrypt("server=10.0.0.11;port=3306;uid=admin;pwd=Innovation123;database=trwiz;Convert Zero Datetime=True"));

            //File.WriteAllText("connection.settings", Crypto.Encrypt( "server=127.0.0.1;port=3307;uid=root;pwd=usbw;database=trwiz;Convert Zero Datetime=True"));

           /* List<string> coursedata = new List<string>();
            List<string> invalid = new List<string>();
            string[] files = Directory.GetFiles(@"C:\Robosys\CodeCamp\eCampus");
            string regs = File.ReadAllText(@"C:\Robosys\CodeCamp\eCampus\allregst.csv");
            List<string> courses = new List<string>(File.ReadAllLines(@"C:\Robosys\CodeCamp\eCampus\allcodes.csv"));
            foreach (string s in files)
            {
                if (s.Contains("allreg.csv.out.courses"))
                {
                    string[] lines = File.ReadAllLines(s);
                    var collection = lines.Select(ss => ss.Split('$')).Where(split => split.Length > 2 && regs.Contains(split[0]) && courses.Contains(split[1]));
                    var ncoll = collection.Select(split => string.Format("{0},{1},2012/2013", split[0], split[1]));
                    int count = ncoll.Count();
                    if (count < 1) return;
                    coursedata.AddRange(ncoll);
                }
            }
            File.WriteAllLines(@"C:\Robosys\CodeCamp\eCampus\badstudents.txt", invalid);
            File.WriteAllLines(@"C:\Robosys\CodeCamp\eCampus\coursesdata.csv", coursedata);
*/
            if (File.Exists("connection.settings"))
            {
                Helper.LocalConnection = Crypto.Decrypt(File.ReadAllText("connection.settings"));
                Helper.BackupConnection = Helper.LocalConnection.Remove(Helper.LocalConnection.IndexOf(";", Helper.LocalConnection.IndexOf("pwd")) + 1);
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (Helper.rk.GetValue("FirstRun") != null && Helper.rk.GetValue("FirstRun").ToString() == "0")
            {
                //Prepare for Intro
            }
            else {
                Application.Run(new loginForm());
               // HiddenForm form=new HiddenForm(); Application.Run(new FileUpload(form)); 
            }
        }
    }
    
    internal class AppHelper{
        internal static string AppPath
        {
            get
            {
                return Application.StartupPath;
            }
        }

        /// <summary>
        /// Shows a message on eCampus
        /// </summary>
        /// <param name="text">The text to display</param>
        /// <param name="type">The type of mesage, 0 for normal, 1 for info, 2 for error</param>
        public static void showMessage(string text, string caption, int type)
        {
            MessageBoxIcon mbi = MessageBoxIcon.None;
            switch (type)
            {
                case 1:
                    mbi = MessageBoxIcon.Information;
                    break;
                case 2:
                    mbi = MessageBoxIcon.Error;
                    break;
            }
            MessageBox.Show(text, caption, MessageBoxButtons.OK, mbi);
        }
    }
}
