namespace ARMS
{
    partial class ExamOfficer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExamOfficer));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewStudents = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.progressLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.myProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageHome = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBoxActivity = new System.Windows.Forms.GroupBox();
            this.dataGridActivity = new System.Windows.Forms.DataGridView();
            this.activityDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activityText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBoxMessages = new System.Windows.Forms.GroupBox();
            this.richTextBoxAnnouncements = new System.Windows.Forms.RichTextBox();
            this.buttonPreviousAnnounce = new System.Windows.Forms.Button();
            this.buttonNextAnnounce = new System.Windows.Forms.Button();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.displayPic = new System.Windows.Forms.PictureBox();
            this.tabPageStudents = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.checkedListBoxIncludedata = new System.Windows.Forms.CheckedListBox();
            this.comboBoxCriteria2 = new System.Windows.Forms.ComboBox();
            this.comboBoxCriteria1 = new System.Windows.Forms.ComboBox();
            this.numericLimit = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnviewList = new System.Windows.Forms.Button();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.buttonViewinExcel = new System.Windows.Forms.Button();
            this.buttonPrintReport = new System.Windows.Forms.Button();
            this.tabPageReports = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonView = new System.Windows.Forms.Button();
            this.numericUpDownSession = new System.Windows.Forms.NumericUpDown();
            this.labelCurrentSession = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxLevel = new System.Windows.Forms.GroupBox();
            this.radioLevel800 = new System.Windows.Forms.RadioButton();
            this.radioLevel700 = new System.Windows.Forms.RadioButton();
            this.radioLevel600 = new System.Windows.Forms.RadioButton();
            this.radioLevel500 = new System.Windows.Forms.RadioButton();
            this.radioLevel400 = new System.Windows.Forms.RadioButton();
            this.radioLevel300 = new System.Windows.Forms.RadioButton();
            this.radioLevel200 = new System.Windows.Forms.RadioButton();
            this.radioLevel100 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioSemester2 = new System.Windows.Forms.RadioButton();
            this.radioSemester1 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioBroadSheet = new System.Windows.Forms.RadioButton();
            this.radioFacultySummary = new System.Windows.Forms.RadioButton();
            this.buttonGenerateReports = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.studentsData = new System.Windows.Forms.DataGridView();
            this.StudentSn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalGP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.comboBoxTLevel = new System.Windows.Forms.ComboBox();
            this.textBoxTRegNo = new System.Windows.Forms.TextBox();
            this.radioButtonByRegno = new System.Windows.Forms.RadioButton();
            this.radioButtonByLevel = new System.Windows.Forms.RadioButton();
            this.buttonTranscript = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonViewTrans = new System.Windows.Forms.Button();
            this.buttonSaveTrans = new System.Windows.Forms.Button();
            this.buttonPrintTrans = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.leaveAMessageForThisStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactLecturerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelMode = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolLevelChooser = new System.Windows.Forms.ToolStripComboBox();
            this.toolTextAcademicYear = new System.Windows.Forms.ToolStripTextBox();
            this.toolComboSemester = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAnnouncer = new System.Windows.Forms.ToolStripButton();
            this.toolButtonLock = new System.Windows.Forms.ToolStripButton();
            this.toolBtnAbout = new System.Windows.Forms.ToolStripButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            this.fbd = new System.Windows.Forms.FolderBrowserDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageHome.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBoxActivity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).BeginInit();
            this.panel4.SuspendLayout();
            this.groupBoxMessages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).BeginInit();
            this.tabPageStudents.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericLimit)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPageReports.SuspendLayout();
            this.panel8.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSession)).BeginInit();
            this.groupBoxLevel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewStudents
            // 
            this.dataGridViewStudents.AllowUserToAddRows = false;
            this.dataGridViewStudents.AllowUserToDeleteRows = false;
            this.dataGridViewStudents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewStudents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewStudents.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudents.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewStudents.Location = new System.Drawing.Point(6, 86);
            this.dataGridViewStudents.Name = "dataGridViewStudents";
            this.dataGridViewStudents.ReadOnly = true;
            this.dataGridViewStudents.Size = new System.Drawing.Size(728, 290);
            this.dataGridViewStudents.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Sort By:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressLabel,
            this.myProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 533);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(760, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.SizeChanged += new System.EventHandler(this.statusStrip1_SizeChanged);
            // 
            // progressLabel
            // 
            this.progressLabel.AutoSize = false;
            this.progressLabel.Name = "progressLabel";
            this.progressLabel.Size = new System.Drawing.Size(245, 17);
            // 
            // myProgress
            // 
            this.myProgress.AutoSize = false;
            this.myProgress.Name = "myProgress";
            this.myProgress.Size = new System.Drawing.Size(500, 16);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageHome);
            this.tabControl1.Controls.Add(this.tabPageStudents);
            this.tabControl1.Controls.Add(this.tabPageReports);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.tabControl1.Location = new System.Drawing.Point(0, 62);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(9, 6);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(751, 468);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPageHome
            // 
            this.tabPageHome.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageHome.Controls.Add(this.panel6);
            this.tabPageHome.Controls.Add(this.panel4);
            this.tabPageHome.Location = new System.Drawing.Point(4, 33);
            this.tabPageHome.Name = "tabPageHome";
            this.tabPageHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHome.Size = new System.Drawing.Size(743, 431);
            this.tabPageHome.TabIndex = 3;
            this.tabPageHome.Text = "Home";
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.AliceBlue;
            this.panel6.Controls.Add(this.groupBoxActivity);
            this.panel6.Location = new System.Drawing.Point(0, 220);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(5);
            this.panel6.Size = new System.Drawing.Size(740, 216);
            this.panel6.TabIndex = 7;
            // 
            // groupBoxActivity
            // 
            this.groupBoxActivity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxActivity.Controls.Add(this.dataGridActivity);
            this.groupBoxActivity.Location = new System.Drawing.Point(0, 7);
            this.groupBoxActivity.Name = "groupBoxActivity";
            this.groupBoxActivity.Size = new System.Drawing.Size(740, 201);
            this.groupBoxActivity.TabIndex = 1;
            this.groupBoxActivity.TabStop = false;
            this.groupBoxActivity.Text = "Department Activity";
            // 
            // dataGridActivity
            // 
            this.dataGridActivity.AllowUserToAddRows = false;
            this.dataGridActivity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridActivity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridActivity.BackgroundColor = System.Drawing.Color.White;
            this.dataGridActivity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridActivity.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridActivity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridActivity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.activityDate,
            this.activityText});
            this.dataGridActivity.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridActivity.GridColor = System.Drawing.Color.White;
            this.dataGridActivity.Location = new System.Drawing.Point(8, 22);
            this.dataGridActivity.Name = "dataGridActivity";
            this.dataGridActivity.ReadOnly = true;
            this.dataGridActivity.Size = new System.Drawing.Size(726, 173);
            this.dataGridActivity.TabIndex = 1;
            // 
            // activityDate
            // 
            this.activityDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.activityDate.HeaderText = "Date";
            this.activityDate.Name = "activityDate";
            this.activityDate.ReadOnly = true;
            this.activityDate.Width = 120;
            // 
            // activityText
            // 
            this.activityText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.activityText.HeaderText = "Details";
            this.activityText.Name = "activityText";
            this.activityText.ReadOnly = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBoxMessages);
            this.panel4.Controls.Add(this.labelWelcome);
            this.panel4.Controls.Add(this.displayPic);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(737, 215);
            this.panel4.TabIndex = 6;
            // 
            // groupBoxMessages
            // 
            this.groupBoxMessages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMessages.Controls.Add(this.richTextBoxAnnouncements);
            this.groupBoxMessages.Controls.Add(this.buttonPreviousAnnounce);
            this.groupBoxMessages.Controls.Add(this.buttonNextAnnounce);
            this.groupBoxMessages.Location = new System.Drawing.Point(11, 42);
            this.groupBoxMessages.Name = "groupBoxMessages";
            this.groupBoxMessages.Size = new System.Drawing.Size(584, 160);
            this.groupBoxMessages.TabIndex = 6;
            this.groupBoxMessages.TabStop = false;
            this.groupBoxMessages.Text = "Announcements";
            // 
            // richTextBoxAnnouncements
            // 
            this.richTextBoxAnnouncements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxAnnouncements.Location = new System.Drawing.Point(8, 22);
            this.richTextBoxAnnouncements.Name = "richTextBoxAnnouncements";
            this.richTextBoxAnnouncements.Size = new System.Drawing.Size(570, 99);
            this.richTextBoxAnnouncements.TabIndex = 3;
            this.richTextBoxAnnouncements.Text = "No announcements found";
            // 
            // buttonPreviousAnnounce
            // 
            this.buttonPreviousAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPreviousAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonPreviousAnnounce.Location = new System.Drawing.Point(396, 127);
            this.buttonPreviousAnnounce.Name = "buttonPreviousAnnounce";
            this.buttonPreviousAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonPreviousAnnounce.TabIndex = 1;
            this.buttonPreviousAnnounce.Text = "Previous";
            this.buttonPreviousAnnounce.UseVisualStyleBackColor = true;
            this.buttonPreviousAnnounce.Click += new System.EventHandler(this.buttonPreviousAnnounce_Click);
            // 
            // buttonNextAnnounce
            // 
            this.buttonNextAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonNextAnnounce.Location = new System.Drawing.Point(490, 127);
            this.buttonNextAnnounce.Name = "buttonNextAnnounce";
            this.buttonNextAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonNextAnnounce.TabIndex = 1;
            this.buttonNextAnnounce.Text = "Next";
            this.buttonNextAnnounce.UseVisualStyleBackColor = true;
            this.buttonNextAnnounce.Click += new System.EventHandler(this.buttonNextAnnounce_Click);
            // 
            // labelWelcome
            // 
            this.labelWelcome.BackColor = System.Drawing.Color.Transparent;
            this.labelWelcome.Font = new System.Drawing.Font("Trebuchet MS", 18F);
            this.labelWelcome.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelWelcome.Location = new System.Drawing.Point(6, 6);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(488, 33);
            this.labelWelcome.TabIndex = 4;
            this.labelWelcome.Text = "Good day Mr. UNN. ";
            this.labelWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // displayPic
            // 
            this.displayPic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.displayPic.Image = global::ARMS.Properties.Resources.UNN_Logo1;
            this.displayPic.Location = new System.Drawing.Point(601, 41);
            this.displayPic.Name = "displayPic";
            this.displayPic.Size = new System.Drawing.Size(122, 155);
            this.displayPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.displayPic.TabIndex = 3;
            this.displayPic.TabStop = false;
            // 
            // tabPageStudents
            // 
            this.tabPageStudents.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageStudents.Controls.Add(this.panel7);
            this.tabPageStudents.Controls.Add(this.panel1);
            this.tabPageStudents.Controls.Add(this.dataGridViewStudents);
            this.tabPageStudents.Location = new System.Drawing.Point(4, 33);
            this.tabPageStudents.Name = "tabPageStudents";
            this.tabPageStudents.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStudents.Size = new System.Drawing.Size(743, 431);
            this.tabPageStudents.TabIndex = 0;
            this.tabPageStudents.Text = "Generate Student List";
            this.tabPageStudents.Click += new System.EventHandler(this.tabPageStudents_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.buttonGenerate);
            this.panel7.Controls.Add(this.checkedListBoxIncludedata);
            this.panel7.Controls.Add(this.comboBoxCriteria2);
            this.panel7.Controls.Add(this.comboBoxCriteria1);
            this.panel7.Controls.Add(this.numericLimit);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(737, 83);
            this.panel7.TabIndex = 12;
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonGenerate.ImageKey = "APPLY.PNG";
            this.buttonGenerate.ImageList = this.imageList1;
            this.buttonGenerate.Location = new System.Drawing.Point(661, 36);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Padding = new System.Windows.Forms.Padding(2);
            this.buttonGenerate.Size = new System.Drawing.Size(70, 38);
            this.buttonGenerate.TabIndex = 7;
            this.buttonGenerate.Text = "&Go";
            this.buttonGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "APPLY.PNG");
            // 
            // checkedListBoxIncludedata
            // 
            this.checkedListBoxIncludedata.FormattingEnabled = true;
            this.checkedListBoxIncludedata.Items.AddRange(new object[] {
            "Reg. No",
            "Name",
            "Age",
            "Gender",
            "Department",
            "Email",
            "Phone",
            "Level",
            "Lga",
            "State",
            "Course of Study",
            "GPA",
            "CGPA"});
            this.checkedListBoxIncludedata.Location = new System.Drawing.Point(465, 36);
            this.checkedListBoxIncludedata.Name = "checkedListBoxIncludedata";
            this.checkedListBoxIncludedata.Size = new System.Drawing.Size(182, 40);
            this.checkedListBoxIncludedata.TabIndex = 6;
            this.checkedListBoxIncludedata.Click += new System.EventHandler(this.checkedListBoxIncludedata_Click);
            this.checkedListBoxIncludedata.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // comboBoxCriteria2
            // 
            this.comboBoxCriteria2.FormattingEnabled = true;
            this.comboBoxCriteria2.Items.AddRange(new object[] {
            "Possible Graduands",
            "Carry Overs"});
            this.comboBoxCriteria2.Location = new System.Drawing.Point(195, 48);
            this.comboBoxCriteria2.Name = "comboBoxCriteria2";
            this.comboBoxCriteria2.Size = new System.Drawing.Size(167, 26);
            this.comboBoxCriteria2.TabIndex = 5;
            this.comboBoxCriteria2.Text = "Select Filter";
            // 
            // comboBoxCriteria1
            // 
            this.comboBoxCriteria1.FormattingEnabled = true;
            this.comboBoxCriteria1.Items.AddRange(new object[] {
            "GPA",
            "CGPA",
            "Course of Study"});
            this.comboBoxCriteria1.Location = new System.Drawing.Point(12, 48);
            this.comboBoxCriteria1.Name = "comboBoxCriteria1";
            this.comboBoxCriteria1.Size = new System.Drawing.Size(168, 26);
            this.comboBoxCriteria1.TabIndex = 5;
            this.comboBoxCriteria1.Text = "Level";
            // 
            // numericLimit
            // 
            this.numericLimit.Location = new System.Drawing.Point(383, 51);
            this.numericLimit.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericLimit.Name = "numericLimit";
            this.numericLimit.Size = new System.Drawing.Size(65, 23);
            this.numericLimit.TabIndex = 4;
            this.numericLimit.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(462, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 18);
            this.label5.TabIndex = 3;
            this.label5.Text = "Data to Include:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(380, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Limit:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(192, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Filter By:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnviewList);
            this.panel1.Controls.Add(this.buttonViewinExcel);
            this.panel1.Controls.Add(this.buttonPrintReport);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 378);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(737, 50);
            this.panel1.TabIndex = 11;
            // 
            // btnviewList
            // 
            this.btnviewList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnviewList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnviewList.ImageKey = "Gloss PNGKKMenu_Office.png";
            this.btnviewList.ImageList = this.imageList2;
            this.btnviewList.Location = new System.Drawing.Point(349, 7);
            this.btnviewList.Name = "btnviewList";
            this.btnviewList.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.btnviewList.Size = new System.Drawing.Size(116, 40);
            this.btnviewList.TabIndex = 16;
            this.btnviewList.Text = "&View File";
            this.btnviewList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnviewList.UseVisualStyleBackColor = true;
            this.btnviewList.Click += new System.EventHandler(this.btnviewList_Click);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "build.png");
            this.imageList2.Images.SetKeyName(1, "print_48.png");
            this.imageList2.Images.SetKeyName(2, "Delete.png");
            this.imageList2.Images.SetKeyName(3, "MyDocuments.png");
            this.imageList2.Images.SetKeyName(4, "Gloss PNGKKMenu_Office.png");
            // 
            // buttonViewinExcel
            // 
            this.buttonViewinExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonViewinExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonViewinExcel.ImageKey = "MyDocuments.png";
            this.buttonViewinExcel.ImageList = this.imageList2;
            this.buttonViewinExcel.Location = new System.Drawing.Point(570, 7);
            this.buttonViewinExcel.Name = "buttonViewinExcel";
            this.buttonViewinExcel.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonViewinExcel.Size = new System.Drawing.Size(159, 40);
            this.buttonViewinExcel.TabIndex = 8;
            this.buttonViewinExcel.Text = "&Export to Excel";
            this.buttonViewinExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonViewinExcel.UseVisualStyleBackColor = true;
            this.buttonViewinExcel.Click += new System.EventHandler(this.buttonViewinExcel_Click);
            // 
            // buttonPrintReport
            // 
            this.buttonPrintReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrintReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrintReport.ImageKey = "print_48.png";
            this.buttonPrintReport.ImageList = this.imageList2;
            this.buttonPrintReport.Location = new System.Drawing.Point(471, 7);
            this.buttonPrintReport.Name = "buttonPrintReport";
            this.buttonPrintReport.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonPrintReport.Size = new System.Drawing.Size(95, 40);
            this.buttonPrintReport.TabIndex = 10;
            this.buttonPrintReport.Text = "&Print";
            this.buttonPrintReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonPrintReport.UseVisualStyleBackColor = true;
            this.buttonPrintReport.Click += new System.EventHandler(this.buttonPrintReport_Click);
            // 
            // tabPageReports
            // 
            this.tabPageReports.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageReports.Controls.Add(this.panel8);
            this.tabPageReports.Location = new System.Drawing.Point(4, 33);
            this.tabPageReports.Name = "tabPageReports";
            this.tabPageReports.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageReports.Size = new System.Drawing.Size(743, 431);
            this.tabPageReports.TabIndex = 2;
            this.tabPageReports.Text = "Generate Reports";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.AliceBlue;
            this.panel8.Controls.Add(this.groupBox3);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(737, 425);
            this.panel8.TabIndex = 15;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonView);
            this.groupBox3.Controls.Add(this.numericUpDownSession);
            this.groupBox3.Controls.Add(this.labelCurrentSession);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.groupBoxLevel);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.buttonGenerateReports);
            this.groupBox3.Location = new System.Drawing.Point(5, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(729, 398);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Report Generation Panel";
            // 
            // buttonView
            // 
            this.buttonView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonView.ImageKey = "Gloss PNGKKMenu_Office.png";
            this.buttonView.ImageList = this.imageList2;
            this.buttonView.Location = new System.Drawing.Point(473, 352);
            this.buttonView.Name = "buttonView";
            this.buttonView.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonView.Size = new System.Drawing.Size(116, 40);
            this.buttonView.TabIndex = 15;
            this.buttonView.Text = "&View File";
            this.buttonView.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonView.UseVisualStyleBackColor = true;
            this.buttonView.Click += new System.EventHandler(this.buttonView_Click);
            // 
            // numericUpDownSession
            // 
            this.numericUpDownSession.Location = new System.Drawing.Point(532, 65);
            this.numericUpDownSession.Maximum = new decimal(new int[] {
            2999,
            0,
            0,
            0});
            this.numericUpDownSession.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numericUpDownSession.Name = "numericUpDownSession";
            this.numericUpDownSession.Size = new System.Drawing.Size(97, 23);
            this.numericUpDownSession.TabIndex = 14;
            this.numericUpDownSession.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numericUpDownSession.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // labelCurrentSession
            // 
            this.labelCurrentSession.AutoSize = true;
            this.labelCurrentSession.Location = new System.Drawing.Point(592, 37);
            this.labelCurrentSession.Name = "labelCurrentSession";
            this.labelCurrentSession.Size = new System.Drawing.Size(71, 18);
            this.labelCurrentSession.TabIndex = 13;
            this.labelCurrentSession.Text = "2000/2001";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(529, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 18);
            this.label4.TabIndex = 13;
            this.label4.Text = "Session:";
            // 
            // groupBoxLevel
            // 
            this.groupBoxLevel.Controls.Add(this.radioLevel800);
            this.groupBoxLevel.Controls.Add(this.radioLevel700);
            this.groupBoxLevel.Controls.Add(this.radioLevel600);
            this.groupBoxLevel.Controls.Add(this.radioLevel500);
            this.groupBoxLevel.Controls.Add(this.radioLevel400);
            this.groupBoxLevel.Controls.Add(this.radioLevel300);
            this.groupBoxLevel.Controls.Add(this.radioLevel200);
            this.groupBoxLevel.Controls.Add(this.radioLevel100);
            this.groupBoxLevel.Location = new System.Drawing.Point(29, 37);
            this.groupBoxLevel.Name = "groupBoxLevel";
            this.groupBoxLevel.Size = new System.Drawing.Size(227, 273);
            this.groupBoxLevel.TabIndex = 11;
            this.groupBoxLevel.TabStop = false;
            this.groupBoxLevel.Text = "Select Level";
            // 
            // radioLevel800
            // 
            this.radioLevel800.AutoSize = true;
            this.radioLevel800.Location = new System.Drawing.Point(13, 224);
            this.radioLevel800.Name = "radioLevel800";
            this.radioLevel800.Size = new System.Drawing.Size(84, 22);
            this.radioLevel800.TabIndex = 8;
            this.radioLevel800.TabStop = true;
            this.radioLevel800.Text = "800 Level";
            this.radioLevel800.UseVisualStyleBackColor = true;
            // 
            // radioLevel700
            // 
            this.radioLevel700.AutoSize = true;
            this.radioLevel700.Location = new System.Drawing.Point(13, 196);
            this.radioLevel700.Name = "radioLevel700";
            this.radioLevel700.Size = new System.Drawing.Size(84, 22);
            this.radioLevel700.TabIndex = 8;
            this.radioLevel700.TabStop = true;
            this.radioLevel700.Text = "700 Level";
            this.radioLevel700.UseVisualStyleBackColor = true;
            // 
            // radioLevel600
            // 
            this.radioLevel600.AutoSize = true;
            this.radioLevel600.Location = new System.Drawing.Point(13, 168);
            this.radioLevel600.Name = "radioLevel600";
            this.radioLevel600.Size = new System.Drawing.Size(84, 22);
            this.radioLevel600.TabIndex = 8;
            this.radioLevel600.TabStop = true;
            this.radioLevel600.Text = "600 Level";
            this.radioLevel600.UseVisualStyleBackColor = true;
            // 
            // radioLevel500
            // 
            this.radioLevel500.AutoSize = true;
            this.radioLevel500.Location = new System.Drawing.Point(13, 140);
            this.radioLevel500.Name = "radioLevel500";
            this.radioLevel500.Size = new System.Drawing.Size(84, 22);
            this.radioLevel500.TabIndex = 8;
            this.radioLevel500.TabStop = true;
            this.radioLevel500.Text = "500 Level";
            this.radioLevel500.UseVisualStyleBackColor = true;
            // 
            // radioLevel400
            // 
            this.radioLevel400.AutoSize = true;
            this.radioLevel400.Location = new System.Drawing.Point(13, 112);
            this.radioLevel400.Name = "radioLevel400";
            this.radioLevel400.Size = new System.Drawing.Size(84, 22);
            this.radioLevel400.TabIndex = 8;
            this.radioLevel400.TabStop = true;
            this.radioLevel400.Text = "400 Level";
            this.radioLevel400.UseVisualStyleBackColor = true;
            // 
            // radioLevel300
            // 
            this.radioLevel300.AutoSize = true;
            this.radioLevel300.Location = new System.Drawing.Point(13, 84);
            this.radioLevel300.Name = "radioLevel300";
            this.radioLevel300.Size = new System.Drawing.Size(84, 22);
            this.radioLevel300.TabIndex = 8;
            this.radioLevel300.TabStop = true;
            this.radioLevel300.Text = "300 Level";
            this.radioLevel300.UseVisualStyleBackColor = true;
            // 
            // radioLevel200
            // 
            this.radioLevel200.AutoSize = true;
            this.radioLevel200.Location = new System.Drawing.Point(13, 56);
            this.radioLevel200.Name = "radioLevel200";
            this.radioLevel200.Size = new System.Drawing.Size(84, 22);
            this.radioLevel200.TabIndex = 8;
            this.radioLevel200.TabStop = true;
            this.radioLevel200.Text = "200 Level";
            this.radioLevel200.UseVisualStyleBackColor = true;
            // 
            // radioLevel100
            // 
            this.radioLevel100.AutoSize = true;
            this.radioLevel100.Location = new System.Drawing.Point(13, 28);
            this.radioLevel100.Name = "radioLevel100";
            this.radioLevel100.Size = new System.Drawing.Size(84, 22);
            this.radioLevel100.TabIndex = 8;
            this.radioLevel100.TabStop = true;
            this.radioLevel100.Text = "100 Level";
            this.radioLevel100.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioSemester2);
            this.groupBox2.Controls.Add(this.radioSemester1);
            this.groupBox2.Location = new System.Drawing.Point(297, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(213, 92);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Semester";
            // 
            // radioSemester2
            // 
            this.radioSemester2.AutoSize = true;
            this.radioSemester2.Location = new System.Drawing.Point(19, 56);
            this.radioSemester2.Name = "radioSemester2";
            this.radioSemester2.Size = new System.Drawing.Size(76, 22);
            this.radioSemester2.TabIndex = 8;
            this.radioSemester2.TabStop = true;
            this.radioSemester2.Text = "SECOND";
            this.radioSemester2.UseVisualStyleBackColor = true;
            this.radioSemester2.CheckedChanged += new System.EventHandler(this.radioSemester2_CheckedChanged);
            // 
            // radioSemester1
            // 
            this.radioSemester1.AutoSize = true;
            this.radioSemester1.Location = new System.Drawing.Point(19, 28);
            this.radioSemester1.Name = "radioSemester1";
            this.radioSemester1.Size = new System.Drawing.Size(60, 22);
            this.radioSemester1.TabIndex = 8;
            this.radioSemester1.TabStop = true;
            this.radioSemester1.Text = "FIRST";
            this.radioSemester1.UseVisualStyleBackColor = true;
            this.radioSemester1.CheckedChanged += new System.EventHandler(this.radioSemester1_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioBroadSheet);
            this.groupBox1.Controls.Add(this.radioFacultySummary);
            this.groupBox1.Location = new System.Drawing.Point(297, 163);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 92);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose Template to Generate";
            // 
            // radioBroadSheet
            // 
            this.radioBroadSheet.AutoSize = true;
            this.radioBroadSheet.Location = new System.Drawing.Point(19, 56);
            this.radioBroadSheet.Name = "radioBroadSheet";
            this.radioBroadSheet.Size = new System.Drawing.Size(160, 22);
            this.radioBroadSheet.TabIndex = 8;
            this.radioBroadSheet.TabStop = true;
            this.radioBroadSheet.Text = "Broad Sheet (Form C)";
            this.radioBroadSheet.UseVisualStyleBackColor = true;
            // 
            // radioFacultySummary
            // 
            this.radioFacultySummary.AutoSize = true;
            this.radioFacultySummary.Location = new System.Drawing.Point(19, 28);
            this.radioFacultySummary.Name = "radioFacultySummary";
            this.radioFacultySummary.Size = new System.Drawing.Size(244, 22);
            this.radioFacultySummary.TabIndex = 8;
            this.radioFacultySummary.TabStop = true;
            this.radioFacultySummary.Text = "Faculty Summary Result Evaluation";
            this.radioFacultySummary.UseVisualStyleBackColor = true;
            // 
            // buttonGenerateReports
            // 
            this.buttonGenerateReports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGenerateReports.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonGenerateReports.ImageKey = "APPLY.PNG";
            this.buttonGenerateReports.ImageList = this.imageList1;
            this.buttonGenerateReports.Location = new System.Drawing.Point(596, 352);
            this.buttonGenerateReports.Name = "buttonGenerateReports";
            this.buttonGenerateReports.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonGenerateReports.Size = new System.Drawing.Size(127, 40);
            this.buttonGenerateReports.TabIndex = 3;
            this.buttonGenerateReports.Text = "&Generate";
            this.buttonGenerateReports.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonGenerateReports.UseVisualStyleBackColor = true;
            this.buttonGenerateReports.Click += new System.EventHandler(this.buttonGenerateReports_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage1.Controls.Add(this.studentsData);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(743, 431);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Generate Transcripts";
            // 
            // studentsData
            // 
            this.studentsData.AllowUserToAddRows = false;
            this.studentsData.AllowUserToDeleteRows = false;
            this.studentsData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.studentsData.BackgroundColor = System.Drawing.Color.White;
            this.studentsData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentsData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StudentSn,
            this.StudentName,
            this.RegNo,
            this.Email,
            this.PhoneNumber,
            this.TotalGP,
            this.CGP});
            this.studentsData.Location = new System.Drawing.Point(6, 77);
            this.studentsData.Name = "studentsData";
            this.studentsData.ReadOnly = true;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsData.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.studentsData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.studentsData.Size = new System.Drawing.Size(734, 303);
            this.studentsData.TabIndex = 15;
            // 
            // StudentSn
            // 
            this.StudentSn.DividerWidth = 1;
            this.StudentSn.HeaderText = "SN.";
            this.StudentSn.MaxInputLength = 900;
            this.StudentSn.Name = "StudentSn";
            this.StudentSn.ReadOnly = true;
            this.StudentSn.ToolTipText = "Serial Number";
            this.StudentSn.Width = 40;
            // 
            // StudentName
            // 
            this.StudentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StudentName.DividerWidth = 1;
            this.StudentName.HeaderText = "Name of Student";
            this.StudentName.MinimumWidth = 70;
            this.StudentName.Name = "StudentName";
            this.StudentName.ReadOnly = true;
            this.StudentName.ToolTipText = "Student Name";
            // 
            // RegNo
            // 
            this.RegNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RegNo.DividerWidth = 1;
            this.RegNo.HeaderText = "Reg. No.";
            this.RegNo.Name = "RegNo";
            this.RegNo.ReadOnly = true;
            this.RegNo.ToolTipText = "Student Registration Number";
            this.RegNo.Width = 81;
            // 
            // Email
            // 
            this.Email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Email.DividerWidth = 1;
            this.Email.HeaderText = "Phone Number";
            this.Email.MaxInputLength = 2;
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            this.Email.ToolTipText = "Phone Number";
            this.Email.Width = 116;
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PhoneNumber.DividerWidth = 1;
            this.PhoneNumber.FillWeight = 10F;
            this.PhoneNumber.HeaderText = "Email";
            this.PhoneNumber.MaxInputLength = 3;
            this.PhoneNumber.Name = "PhoneNumber";
            this.PhoneNumber.ReadOnly = true;
            this.PhoneNumber.ToolTipText = "Email";
            this.PhoneNumber.Width = 67;
            // 
            // TotalGP
            // 
            this.TotalGP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TotalGP.DividerWidth = 1;
            this.TotalGP.FillWeight = 10F;
            this.TotalGP.HeaderText = "GPA";
            this.TotalGP.MaxInputLength = 2;
            this.TotalGP.Name = "TotalGP";
            this.TotalGP.ReadOnly = true;
            this.TotalGP.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TotalGP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TotalGP.ToolTipText = "Grade Point Average";
            this.TotalGP.Width = 41;
            // 
            // CGP
            // 
            this.CGP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CGP.HeaderText = "CGPA";
            this.CGP.Name = "CGP";
            this.CGP.ReadOnly = true;
            this.CGP.ToolTipText = "Cummulative Grade Point Average";
            this.CGP.Width = 67;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.AliceBlue;
            this.panel5.Controls.Add(this.comboBoxTLevel);
            this.panel5.Controls.Add(this.textBoxTRegNo);
            this.panel5.Controls.Add(this.radioButtonByRegno);
            this.panel5.Controls.Add(this.radioButtonByLevel);
            this.panel5.Controls.Add(this.buttonTranscript);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(737, 68);
            this.panel5.TabIndex = 14;
            // 
            // comboBoxTLevel
            // 
            this.comboBoxTLevel.Enabled = false;
            this.comboBoxTLevel.FormattingEnabled = true;
            this.comboBoxTLevel.Items.AddRange(new object[] {
            "100",
            "200",
            "300",
            "400",
            "500",
            "600",
            "700",
            "800"});
            this.comboBoxTLevel.Location = new System.Drawing.Point(113, 23);
            this.comboBoxTLevel.Name = "comboBoxTLevel";
            this.comboBoxTLevel.Size = new System.Drawing.Size(136, 26);
            this.comboBoxTLevel.TabIndex = 6;
            this.comboBoxTLevel.Text = "Choose Level";
            this.comboBoxTLevel.SelectedIndexChanged += new System.EventHandler(this.comboBoxTLevel_SelectedIndexChanged);
            // 
            // textBoxTRegNo
            // 
            this.textBoxTRegNo.Location = new System.Drawing.Point(394, 23);
            this.textBoxTRegNo.Name = "textBoxTRegNo";
            this.textBoxTRegNo.Size = new System.Drawing.Size(148, 23);
            this.textBoxTRegNo.TabIndex = 5;
            // 
            // radioButtonByRegno
            // 
            this.radioButtonByRegno.AutoSize = true;
            this.radioButtonByRegno.Checked = true;
            this.radioButtonByRegno.Location = new System.Drawing.Point(284, 24);
            this.radioButtonByRegno.Name = "radioButtonByRegno";
            this.radioButtonByRegno.Size = new System.Drawing.Size(104, 22);
            this.radioButtonByRegno.TabIndex = 4;
            this.radioButtonByRegno.TabStop = true;
            this.radioButtonByRegno.Text = "By Reg. No.:";
            this.radioButtonByRegno.UseVisualStyleBackColor = true;
            this.radioButtonByRegno.CheckedChanged += new System.EventHandler(this.radioButtonByRegno_CheckedChanged);
            // 
            // radioButtonByLevel
            // 
            this.radioButtonByLevel.AutoSize = true;
            this.radioButtonByLevel.Location = new System.Drawing.Point(24, 24);
            this.radioButtonByLevel.Name = "radioButtonByLevel";
            this.radioButtonByLevel.Size = new System.Drawing.Size(83, 22);
            this.radioButtonByLevel.TabIndex = 4;
            this.radioButtonByLevel.Text = "By Level:";
            this.radioButtonByLevel.UseVisualStyleBackColor = true;
            this.radioButtonByLevel.CheckedChanged += new System.EventHandler(this.radioButtonByLevel_CheckedChanged);
            // 
            // buttonTranscript
            // 
            this.buttonTranscript.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTranscript.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonTranscript.ImageKey = "APPLY.PNG";
            this.buttonTranscript.ImageList = this.imageList1;
            this.buttonTranscript.Location = new System.Drawing.Point(594, 15);
            this.buttonTranscript.Name = "buttonTranscript";
            this.buttonTranscript.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonTranscript.Size = new System.Drawing.Size(127, 40);
            this.buttonTranscript.TabIndex = 3;
            this.buttonTranscript.Text = "&Generate";
            this.buttonTranscript.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonTranscript.UseVisualStyleBackColor = true;
            this.buttonTranscript.Click += new System.EventHandler(this.buttonTranscript_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.AliceBlue;
            this.panel3.Controls.Add(this.buttonViewTrans);
            this.panel3.Controls.Add(this.buttonSaveTrans);
            this.panel3.Controls.Add(this.buttonPrintTrans);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(3, 378);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(737, 50);
            this.panel3.TabIndex = 12;
            // 
            // buttonViewTrans
            // 
            this.buttonViewTrans.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonViewTrans.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonViewTrans.ImageKey = "Gloss PNGKKMenu_Office.png";
            this.buttonViewTrans.ImageList = this.imageList2;
            this.buttonViewTrans.Location = new System.Drawing.Point(399, 5);
            this.buttonViewTrans.Name = "buttonViewTrans";
            this.buttonViewTrans.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonViewTrans.Size = new System.Drawing.Size(116, 40);
            this.buttonViewTrans.TabIndex = 8;
            this.buttonViewTrans.Text = "&View File";
            this.buttonViewTrans.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonViewTrans.UseVisualStyleBackColor = true;
            this.buttonViewTrans.Click += new System.EventHandler(this.buttonView_Click);
            // 
            // buttonSaveTrans
            // 
            this.buttonSaveTrans.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveTrans.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSaveTrans.ImageKey = "MyDocuments.png";
            this.buttonSaveTrans.ImageList = this.imageList2;
            this.buttonSaveTrans.Location = new System.Drawing.Point(521, 5);
            this.buttonSaveTrans.Name = "buttonSaveTrans";
            this.buttonSaveTrans.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonSaveTrans.Size = new System.Drawing.Size(111, 40);
            this.buttonSaveTrans.TabIndex = 9;
            this.buttonSaveTrans.Text = "&Save As";
            this.buttonSaveTrans.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSaveTrans.UseVisualStyleBackColor = true;
            this.buttonSaveTrans.Click += new System.EventHandler(this.buttonSaveTrans_Click);
            // 
            // buttonPrintTrans
            // 
            this.buttonPrintTrans.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrintTrans.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrintTrans.ImageKey = "print_48.png";
            this.buttonPrintTrans.ImageList = this.imageList2;
            this.buttonPrintTrans.Location = new System.Drawing.Point(638, 5);
            this.buttonPrintTrans.Name = "buttonPrintTrans";
            this.buttonPrintTrans.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonPrintTrans.Size = new System.Drawing.Size(95, 40);
            this.buttonPrintTrans.TabIndex = 10;
            this.buttonPrintTrans.Text = "&Print";
            this.buttonPrintTrans.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonPrintTrans.UseVisualStyleBackColor = true;
            this.buttonPrintTrans.Click += new System.EventHandler(this.buttonPrintTrans_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leaveAMessageForThisStudentToolStripMenuItem,
            this.contactLecturerToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(246, 48);
            // 
            // leaveAMessageForThisStudentToolStripMenuItem
            // 
            this.leaveAMessageForThisStudentToolStripMenuItem.Name = "leaveAMessageForThisStudentToolStripMenuItem";
            this.leaveAMessageForThisStudentToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.leaveAMessageForThisStudentToolStripMenuItem.Text = "Leave a message for this student";
            this.leaveAMessageForThisStudentToolStripMenuItem.Click += new System.EventHandler(this.leaveAMessageForThisStudentToolStripMenuItem_Click);
            // 
            // contactLecturerToolStripMenuItem
            // 
            this.contactLecturerToolStripMenuItem.Name = "contactLecturerToolStripMenuItem";
            this.contactLecturerToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.contactLecturerToolStripMenuItem.Text = "Contact Lecturer for this course";
            this.contactLecturerToolStripMenuItem.Click += new System.EventHandler(this.contactLecturerToolStripMenuItem_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.labelMode);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(760, 62);
            this.panel2.TabIndex = 6;
            // 
            // labelMode
            // 
            this.labelMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.labelMode.ForeColor = System.Drawing.Color.Red;
            this.labelMode.Location = new System.Drawing.Point(572, 12);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(176, 39);
            this.labelMode.TabIndex = 3;
            this.labelMode.Text = "ONLINE";
            this.labelMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolLevelChooser,
            this.toolTextAcademicYear,
            this.toolComboSemester,
            this.toolStripSeparator2,
            this.btnAnnouncer,
            this.toolButtonLock,
            this.toolBtnAbout});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(3, 2);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(519, 57);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolLevelChooser
            // 
            this.toolLevelChooser.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolLevelChooser.Items.AddRange(new object[] {
            "100",
            "200",
            "300",
            "400",
            "500",
            "600",
            "700",
            "800"});
            this.toolLevelChooser.Name = "toolLevelChooser";
            this.toolLevelChooser.Padding = new System.Windows.Forms.Padding(3);
            this.toolLevelChooser.Size = new System.Drawing.Size(119, 57);
            this.toolLevelChooser.Text = "Choose Level";
            this.toolLevelChooser.SelectedIndexChanged += new System.EventHandler(this.toolLevelChooser_SelectedIndexChanged);
            // 
            // toolTextAcademicYear
            // 
            this.toolTextAcademicYear.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolTextAcademicYear.Name = "toolTextAcademicYear";
            this.toolTextAcademicYear.Padding = new System.Windows.Forms.Padding(3);
            this.toolTextAcademicYear.Size = new System.Drawing.Size(74, 57);
            this.toolTextAcademicYear.Text = "2012/2013";
            // 
            // toolComboSemester
            // 
            this.toolComboSemester.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolComboSemester.Items.AddRange(new object[] {
            "FIRST",
            "SECOND"});
            this.toolComboSemester.Name = "toolComboSemester";
            this.toolComboSemester.Padding = new System.Windows.Forms.Padding(3);
            this.toolComboSemester.Size = new System.Drawing.Size(86, 57);
            this.toolComboSemester.Text = "Semester";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 57);
            // 
            // btnAnnouncer
            // 
            this.btnAnnouncer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAnnouncer.Image = global::ARMS.Properties.Resources.announcements;
            this.btnAnnouncer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAnnouncer.Name = "btnAnnouncer";
            this.btnAnnouncer.Size = new System.Drawing.Size(52, 54);
            this.btnAnnouncer.Text = "New Announcement";
            this.btnAnnouncer.Click += new System.EventHandler(this.btnAnnouncer_Click);
            // 
            // toolButtonLock
            // 
            this.toolButtonLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonLock.Image = global::ARMS.Properties.Resources.Lock;
            this.toolButtonLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonLock.Name = "toolButtonLock";
            this.toolButtonLock.Size = new System.Drawing.Size(52, 54);
            this.toolButtonLock.Text = "Lock Session";
            this.toolButtonLock.Click += new System.EventHandler(this.toolButtonLock_Click);
            // 
            // toolBtnAbout
            // 
            this.toolBtnAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnAbout.Image = global::ARMS.Properties.Resources.arms128;
            this.toolBtnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnAbout.Name = "toolBtnAbout";
            this.toolBtnAbout.Size = new System.Drawing.Size(52, 54);
            this.toolBtnAbout.Text = "About Automated Result Management System";
            // 
            // sfd
            // 
            this.sfd.DefaultExt = "xlsx";
            this.sfd.Filter = "Microsoft Excel Worksheet|*.xlsx|Microsoft Excel Macro-Enabled Worksheet|*.xlsm|M" +
    "icrosoft Word Document|*.docx";
            this.sfd.InitialDirectory = "C:\\";
            this.sfd.RestoreDirectory = true;
            // 
            // fbd
            // 
            this.fbd.Description = "Choose a folder to save the transcripts to or create a new folder.";
            this.fbd.SelectedPath = "C:\\";
            this.fbd.HelpRequest += new System.EventHandler(this.fbd_HelpRequest);
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "A robust result management solution for Educational Institutions";
            this.notifyIcon1.BalloonTipTitle = "Automated Result Management System 1.0";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
            this.notifyIcon1.BalloonTipShown += new System.EventHandler(this.notifyIcon1_BalloonTipShown);
            // 
            // ExamOfficer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 555);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(776, 593);
            this.Name = "ExamOfficer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exam Officer Administration Panel";
            this.Load += new System.EventHandler(this.ExamOfficer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageHome.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBoxActivity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).EndInit();
            this.panel4.ResumeLayout(false);
            this.groupBoxMessages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).EndInit();
            this.tabPageStudents.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericLimit)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabPageReports.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSession)).EndInit();
            this.groupBoxLevel.ResumeLayout(false);
            this.groupBoxLevel.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewStudents;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageStudents;
        private System.Windows.Forms.TabPage tabPageReports;
        private System.Windows.Forms.TabPage tabPageHome;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBoxActivity;
        private System.Windows.Forms.DataGridView dataGridActivity;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityText;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBoxMessages;
        private System.Windows.Forms.Button buttonPreviousAnnounce;
        private System.Windows.Forms.Button buttonNextAnnounce;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.PictureBox displayPic;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox toolLevelChooser;
        private System.Windows.Forms.ToolStripButton btnAnnouncer;
        private System.Windows.Forms.ComboBox comboBoxCriteria1;
        private System.Windows.Forms.NumericUpDown numericLimit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox checkedListBoxIncludedata;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonViewinExcel;
        private System.Windows.Forms.Button buttonPrintReport;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonViewTrans;
        private System.Windows.Forms.Button buttonSaveTrans;
        private System.Windows.Forms.Button buttonPrintTrans;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox comboBoxTLevel;
        private System.Windows.Forms.TextBox textBoxTRegNo;
        private System.Windows.Forms.RadioButton radioButtonByRegno;
        private System.Windows.Forms.RadioButton radioButtonByLevel;
        private System.Windows.Forms.Button buttonTranscript;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button buttonGenerateReports;
        private System.Windows.Forms.GroupBox groupBoxLevel;
        private System.Windows.Forms.RadioButton radioLevel600;
        private System.Windows.Forms.RadioButton radioLevel500;
        private System.Windows.Forms.RadioButton radioLevel400;
        private System.Windows.Forms.RadioButton radioLevel300;
        private System.Windows.Forms.RadioButton radioLevel200;
        private System.Windows.Forms.RadioButton radioLevel100;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioBroadSheet;
        private System.Windows.Forms.RadioButton radioFacultySummary;
        private System.Windows.Forms.ToolStripProgressBar myProgress;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioSemester2;
        private System.Windows.Forms.RadioButton radioSemester1;
        private System.Windows.Forms.NumericUpDown numericUpDownSession;
        private System.Windows.Forms.Label labelCurrentSession;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioLevel700;
        private System.Windows.Forms.SaveFileDialog sfd;
        private System.Windows.Forms.ToolStripStatusLabel progressLabel;
        private System.Windows.Forms.Button buttonView;
        private System.Windows.Forms.RichTextBox richTextBoxAnnouncements;
        private System.Windows.Forms.RadioButton radioLevel800;
        private System.Windows.Forms.FolderBrowserDialog fbd;
        private System.Windows.Forms.ToolStripTextBox toolTextAcademicYear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripComboBox toolComboSemester;
        private System.Windows.Forms.ToolStripButton toolButtonLock;
        private System.Windows.Forms.ComboBox comboBoxCriteria2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem leaveAMessageForThisStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactLecturerToolStripMenuItem;
        private System.Windows.Forms.DataGridView studentsData;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentSn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalGP;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGP;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button btnviewList;
        private System.Windows.Forms.ToolStripButton toolBtnAbout;
    }
}
