using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Linq;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ARMS
{
    public partial class loginForm : Form
    {
        public loginForm()
        {
            InitializeComponent();
            //Thread t = new Thread(update);
            //t.Start(Application.ProductVersion);
        }

        private void update(object o)
        {
            string s = o as string;
            string ver = new WebClient().DownloadString(Helper.UpdateUrl);
            bool mandatory = ver[0] != '0';
            ver = ver.Substring(1);
            if (ver != s)
            {
                if (mandatory)
                {
                    MessageBox.Show("Smart Update has discovered your version of Automated Result Management System is outdated, This new update is mandatory. Please ensure you are connected to the internet and click ok to proceed with the update.", "Mandatory Update!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Process.Start("cmd.exe", "-k @echo off\necho Updating program. Please wait....");
                    Process.GetCurrentProcess().Kill();
                }
                else
                {

                    var dr = MessageBox.Show("Smart Update has discovered your version of Automated Result Management System is outdated, would you like to update now? (Please ensure you are connected to the internet before clicking yes)", "New Version Available!", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dr == DialogResult.Yes)
                    {
                        Process.Start("cmd.exe", "-k @echo off\necho Updating program. Please wait....");
                        Process.GetCurrentProcess().Kill();
                    }
                }
            }
        }

        private string usr;

        private void btnLogin_Click(object sender, EventArgs e)
        {
            checkLogin();
        }

        private void checkLogin()
        {
            //Processing p = new Processing();
            //p.Show();
            usr = textUsername.Text;
            string password = Crypto.computeMD5(textPassword.Text).ToLower();
            if (loginMode.Text == "Local")
            {
                localLogin(password);
            }
            else if (loginMode.Text == "Online")
            {
                onlineLogin(password, false);
            }
            else
            {
                onlineLogin(password, true);
            }
            //p.Hide();
            //p.Close();

        }

        private void onlineLogin(string password, bool autodetect)
        {
            var results = Helper.selectResults(new[] {"secret", "gender", "group_concat(role)"}, "users", new[] {"roles.username", "password", "deleted"},
                                                new[] {textUsername.Text, password, "N"}, "join roles on users.username = roles.username");

            if (results == null)
            {
                if (autodetect)
                {

                    var dr =
                        MessageBox.Show(
                            "It seems the online server is not responding. Do you want to try logging in locally?",
                            "No Server Response", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        localLogin(password);
                    }
                }
                else
                {
                    MessageBox.Show(
                        "Error encountered logging in. Is the Server on? If problem persists, please contact the administrator.",
                        "Cannot Login", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                }
            }
            else if (results.Length == 0)
            {
                MessageBox.Show("Invalid username/password. Please try again", "Error Logging In", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else
            {
                User ur = new User {Gender = results[0][1], Secret = results[0][0], Username = usr, Password = password};
                string uroles = results[0][2];
                doLogin(ur, false, uroles);
            }
        }

        private void localLogin(string password)
        {
            User uget = (User)Helper.Deserialize(Application.StartupPath + "\\userdb.xml", typeof(User));
            if (uget.Username == usr && password == uget.Password)
            {
                doLogin(uget, true, String.Join(",", uget.Roles));
            }
            else
            {
                MessageBox.Show("Invalid username/password. Please try again", "Error Logging In", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            }
        }

        private void doLogin(User user, bool local, string uroles)
        {
            Visible = false;
            ShowInTaskbar = false;
            string secret = user.Secret;
            bool gender = user.Gender == "M";
            string[] roles = new string[]{};
            if (local)
            {
                roles = user.Roles;
                secret = user.Secret;
            }
            else
            {
                roles = uroles.Split(',');
                user.Roles = roles;
                user.Secret = secret;
                try
                {
                    Helper.SerializeData(typeof(User), Application.StartupPath + "\\userdb.xml", user);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex);
                }
            }
            try
            {
                if (roles.Length == 1 && roles[0] == "Lecturer")
                {
                    var lecturer =
                        Helper.selectResults(
                            new[] {"title", "lastName", "department", "email", "firstName", "faculty", "staffCode"},
                            "lecturers", new[] {"username"}, new[] {textUsername.Text}, "")[0];
                    var lcd = new LecturerDetails
                        {
                            UserName = textUsername.Text,
                            Title = lecturer[0],
                            LastName = lecturer[1],
                            Department = lecturer[2],
                            Email = lecturer[3],
                            FirstName = lecturer[4],
                            Faculty = lecturer[5],
                        };

                    if (local == false)
                    {
                        if (Crypto.computeMD5(lecturer[6]) == user.Password)
                        {
                            LecturerReg lr = new LecturerReg(lcd.UserName, gender, lcd, true);
                            lr.ShowDialog();
                        }
                    }

                    try
                    {
                        homeLecturer hl = new homeLecturer(lcd, local);
                        hl.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(ex);
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    ChooseLoad cl = new ChooseLoad(roles, usr, secret, gender, local);
                    cl.ShowDialog();
                }
                textPassword.Clear();
                Visible = true;
                ShowInTaskbar = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loginForm_Load(object sender, EventArgs e)
        {
            //introTooltip1.doWelcome();
            //pictureBox1.Image = Properties.Resources.armsanim128;
            //introTooltip1.Show((string)textUsername.Tag, textUsername);
            //introTooltip1.BeginShow(this);
        }
    }
}
