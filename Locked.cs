using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ARMS
{
    /// <summary>
    /// Locks the form whenever it becomes idle for a certain amount of time
    /// </summary>
    public partial class Locked : Form
    {
        private readonly string usr;

        /// <summary>
        /// Initializes the Gorm using the specified username
        /// </summary>
        /// <param name="user">The username of the user currently using the application</param>
        public Locked(string user)
        {
            usr = user;
            InitializeComponent();
        }

        /// <summary>
        /// The unlock event args for the Lock Form. Listen to this event to know when the main form should be shown
        /// </summary>
        public class UnlockEventArgs : EventArgs
        {
            /// <summary>
            /// Auto property. Sets or tells whether the form has been unlocked or not
            /// </summary>
            public bool Unlocked { get; set; }
        }

        /// <summary>
        /// The event handler for the Unlock Event
        /// </summary>
        public event EventHandler<UnlockEventArgs> UnlockEvent;

        private bool canClose;

        private void Locked_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !canClose;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string password = Crypto.computeMD5(textBoxPassword.Text).ToLower();

            XmlSerializer serializer = new XmlSerializer(typeof(User));
            FileStream fs = new FileStream(Application.StartupPath + "\\userdb.xml", FileMode.Open);
            User uget;
            uget = (User)serializer.Deserialize(fs);
            if (uget.Username == usr && password == uget.Password)
            {
                canClose = true;
                UnlockEventArgs uea = new UnlockEventArgs();
                uea.Unlocked = true;
                UnlockEvent(this, uea);
                Close();
            }
            else
            {
                MessageBox.Show("Invalid username/password. Please try again", "Cannot Unlock Session", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
    }
}
