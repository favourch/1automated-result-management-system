using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ARMS
{
    public partial class homeBursar : Form
    {
        private string username;

        public homeBursar(string username)
        {
            /*
            InitializeComponent();
            this.username = username;
            if (Globals.GetUnreadMessageCount(username) > 0)
            {
                toolBtnMsg.Image = Properties.Resources.nmsg;
                var msgs = Globals.GetMessages(username);
                foreach (var m in msgs)
                {
                    if (m.Type == MessageType.Critical && m.Sender != username && m.Unread)
                    {
                        CriticalMessage cm = new CriticalMessage(msgs);
                        cm.ShowDialog();
                    }
                }

            }
             */
        }

        /*

        private void btnAddView_Click(object sender, EventArgs e)
        {
            MySqlConnection mc = new MySqlConnection(Globals.LocalConnection);


            string sql = "SELECT * FROM students WHERE lName=@lName";
            List<student> students = new List<student>();
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            msc.Parameters.AddWithValue("@lName", searchName.Text);
            ;
            MySqlDataReader dr = msc.ExecuteReader();
            while (dr.Read())
            {
                student mstudent = new student();
                mstudent.firstname = dr["fName"].ToString();
                mstudent.lastname = dr["lName"].ToString();
                mstudent.department = dr["dept"].ToString();
                mstudent.email = dr["email"].ToString();
                mstudent.middlename = dr["mName"].ToString();
                mstudent.regno = dr["regNo"].ToString();
                mstudent.Gender = dr["gender"].ToString();
                students.Add(mstudent);
            }
            //  foreach (regnos
            //  vstudents = students;
            dataGridView1.DataSource = students;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            MySqlConnection mc = new MySqlConnection(Globals.LocalConnection);


            string sql = "SELECT * FROM students WHERE regNo=@regNo";
            List<student> students = new List<student>();
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            msc.Parameters.AddWithValue("@regNo", searchText.Text);
            ;
            MySqlDataReader dr = msc.ExecuteReader();
            while (dr.Read())
            {
                student mstudent = new student();
                mstudent.firstname = dr["fName"].ToString();
                mstudent.lastname = dr["lName"].ToString();
                mstudent.department = dr["dept"].ToString();
                mstudent.email = dr["email"].ToString();
                mstudent.middlename = dr["mName"].ToString();
                mstudent.regno = dr["regNo"].ToString();
                mstudent.Gender = dr["gender"].ToString();
                students.Add(mstudent);
            }
            //  foreach (regnos

            dataGridView1.DataSource = students;

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int index = dataGridView1.SelectedRows[0].Index;
                DataGridViewRow dgrow = dataGridView1.Rows[index];
                searchText.Text = dgrow.Cells["regno"].Value.ToString();
                if (checkIfLocked(dgrow.Cells["regno"].Value.ToString()) == true)
                {
                    btnLock.Image = Properties.Resources.Keychain;
                    btnLock.Text = "Unlock Student";
                }
                else if (checkIfLocked(dgrow.Cells["regno"].Value.ToString()) == false)
                {
                    btnLock.Image = Properties.Resources.Lock;
                    btnLock.Text = "Lock Student";
                }
            }
            catch
            {
            }
        }

        private bool checkLock()
        {
            MySqlConnection conn = new MySqlConnection(Globals.LocalConnection);
            MySqlCommand cmd = new MySqlCommand();
            string sql = "SELECT * FROM access WHERE regno=@regno";
            cmd.Connection = conn;
            cmd.CommandText = sql;
            string number = null;
            cmd.Parameters.AddWithValue("@regno", searchText.Text);
            conn.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                number = reader["regno"].ToString();

            }
            if (number == null)
                return false;
            else return true;
        }

        private void btnLock_Click(object sender, EventArgs e)
        {
            if (btnLock.Text == "Lock Student")
            {
                lockStudent();
            }
            else
            {
                unlock();
            }
        }


        private void lockStudent()
        {
            MySqlConnection conn = new MySqlConnection(Globals.LocalConnection);

            if (checkLock() == false)
            {
                MySqlCommand cmd = new MySqlCommand();
                string sql = "INSERT INTO access VALUES (NULL,@regno,@locked)";
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("@regno", searchText.Text);
                cmd.Parameters.AddWithValue("@locked", true);
                conn.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Student locked successfully", "Automated Result Management System", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("There was an error locking the student", "Automated Result Management System", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
            else
            {
                MySqlCommand cmd = new MySqlCommand();
                string sql = "UPDATE access SET locked=@locked WHERE (regno=@regno)";
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("@locked", true);
                cmd.Parameters.AddWithValue("@regno", searchName.Text);
                conn.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Student locked successfully", "Automated Result Management System", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("There was an error locking the student", "Automated Result Management System", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
        }


        private bool checkIfLocked(string rNo)
        {
            MySqlConnection conn = new MySqlConnection(Globals.LocalConnection);
            MySqlCommand cmd = new MySqlCommand();
            string sql = "SELECT locked FROM access WHERE regno=@regno";
            cmd.Connection = conn;
            cmd.CommandText = sql;
            bool number = new bool();
            cmd.Parameters.AddWithValue("@regno", rNo);
            conn.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                number = Convert.ToBoolean(reader["locked"]);

            }
            return number;
            //else return true;
        }


        private void unlock()
        {
            MySqlConnection conn = new MySqlConnection(Globals.LocalConnection);

            if (checkLock() == false)
            {
                MySqlCommand cmd = new MySqlCommand();
                string sql = "INSERT INTO access VALUES (NULL,@regno,@locked)";
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("@regno", searchText.Text);
                cmd.Parameters.AddWithValue("@locked", false);
                conn.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Student unlocked successfully", "Automated Result Management System", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("There was an error unlocking the student", "Automated Result Management System", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                }
            }
            else
            {
                MySqlCommand cmd = new MySqlCommand();
                string sql = "UPDATE access SET locked=@locked WHERE (regno=@regno)";
                cmd.Connection = conn;
                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("@locked", false);
                cmd.Parameters.AddWithValue("@regno", searchName.Text);
                conn.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Student unlocked successfully", "Automated Result Management System", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("There was an error unlocking the student", "Automated Result Management System", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void btnUnlock_Click(object sender, EventArgs e)
        {

        }

        private void btnMakePayment_Click(object sender, EventArgs e)
        {
            if (searchText.Text == "" || searchText.Text == "Search by Reg. No.")
            {
                MessageBox.Show(
                    "No student items. Please select a student first, then choose level and fee type. Then try again. To select a student you can search by registration no. or last name.",
                    "Automated Result Management System", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                searchText.Focus();
                return;
            }
            if (cbofeetype.SelectedText == "")
            {
                MessageBox.Show("Please choose type of fee first, then try again.", "Automated Result Management System",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbofeetype.Focus();
                return;
            }
            MySqlConnection conn = new MySqlConnection(Globals.LocalConnection);
            MySqlCommand cmd = new MySqlCommand();
            string sql = "INSERT INTO payment VALUES (NULL,@regno,@feetype,@paymentyear)";
            cmd.Connection = conn;
            cmd.CommandText = sql;
            conn.Open();
            cmd.Parameters.AddWithValue("@regno", searchText.Text);
            cmd.Parameters.AddWithValue("@feetype", cbofeetype.SelectedText);
            cmd.Parameters.AddWithValue("@paymentyear", DateTime.Now.Date);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Payment made successfully", "Automated Result Management System", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("There was an error making the payment", "Automated Result Management System", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }

        }

        private void btnMessage_Click(object sender, EventArgs e)
        {
            eMessage msg = new eMessage("Bursar");
            msg.ShowDialog();
        }

        private void btnPhistory_Click(object sender, EventArgs e)
        {
            paymentForm pf = new paymentForm();
            pf.Show();
        }

        private void searchText_Click(object sender, EventArgs e)
        {
            ToolStripTextBox t = sender as ToolStripTextBox;
            t.Clear();
        }
         */
    }
}
