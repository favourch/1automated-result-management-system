using System;
using System.Windows.Forms;
using System.IO;

namespace ARMS
{
    public partial class chooseCourse : Form
    {
        public string courseCode;
        public string courseTitle;
        public string units;
        public string semester;
        public chooseCourse()
        {
            InitializeComponent();
            var cus = File.ReadAllLines("courses.txt");
            for (int i = 2; i < cus.Length; i += 7)
            {
                listView1.Items.Add(new ListViewItem(new[]{cus[i + 1], cus[i]}));
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0) {
                MessageBox.Show("Please choose a course from the list and try again.", "No course items", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            courseCode = listView1.SelectedItems[0].SubItems[0].Text;
            courseTitle = listView1.SelectedItems[0].SubItems[1].Text;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
