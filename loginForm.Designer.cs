namespace ARMS
{
    partial class loginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(loginForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.textUsername = new System.Windows.Forms.TextBox();
            this.textPassword = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.loginMode = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.introTooltip1 = new ARMS.introTooltip();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Candara", 16.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(11, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(280, 114);
            this.label1.TabIndex = 8;
            this.label1.Text = "Please login to continue";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11F);
            this.label2.Location = new System.Drawing.Point(13, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Username:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 11F);
            this.label3.Location = new System.Drawing.Point(13, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Password:";
            // 
            // btnLogin
            // 
            this.btnLogin.Font = new System.Drawing.Font("Calibri", 15.75F);
            this.btnLogin.Location = new System.Drawing.Point(343, 149);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(110, 88);
            this.btnLogin.TabIndex = 3;
            this.btnLogin.Tag = "Good, now click here to Login";
            this.btnLogin.Text = "Login";
            this.introTooltip1.SetToolTip(this.btnLogin, "Login");
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 240);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(464, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(230, 17);
            this.toolStripStatusLabel1.Text = "Designed && Developed by Robotic Systems";
            // 
            // textUsername
            // 
            this.textUsername.Font = new System.Drawing.Font("Verdana", 11F);
            this.textUsername.Location = new System.Drawing.Point(120, 149);
            this.textUsername.Name = "textUsername";
            this.textUsername.Size = new System.Drawing.Size(217, 25);
            this.textUsername.TabIndex = 1;
            this.textUsername.Tag = "Enter the username provided to you by department to Login here.";
            this.introTooltip1.SetToolTip(this.textUsername, "Enter Your Username");
            // 
            // textPassword
            // 
            this.textPassword.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
            this.textPassword.Location = new System.Drawing.Point(120, 180);
            this.textPassword.Name = "textPassword";
            this.textPassword.PasswordChar = 'o';
            this.textPassword.Size = new System.Drawing.Size(217, 25);
            this.textPassword.TabIndex = 2;
            this.textPassword.Tag = "Enter your password here. This is by default the StaffCode provided to you by dep" +
    "artment";
            this.introTooltip1.SetToolTip(this.textPassword, "Enter Password");
            this.textPassword.UseSystemPasswordChar = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 11F);
            this.label4.Location = new System.Drawing.Point(13, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Login Mode:";
            // 
            // loginMode
            // 
            this.loginMode.Font = new System.Drawing.Font("Verdana", 11F);
            this.loginMode.FormattingEnabled = true;
            this.loginMode.Items.AddRange(new object[] {
            "Local",
            "Online",
            "Auto Detect"});
            this.loginMode.Location = new System.Drawing.Point(120, 211);
            this.loginMode.Name = "loginMode";
            this.loginMode.Size = new System.Drawing.Size(217, 26);
            this.loginMode.TabIndex = 0;
            this.loginMode.TabStop = false;
            this.loginMode.Text = "Auto Detect";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Calibri", 9F);
            this.label5.Location = new System.Drawing.Point(13, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(275, 42);
            this.label5.TabIndex = 6;
            this.label5.Text = "Having problems logging in? Contact the Administrator at {Admin Number}";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ARMS.Properties.Resources.arms128;
            this.pictureBox1.Location = new System.Drawing.Point(330, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(123, 127);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // introTooltip1
            // 
            this.introTooltip1.AutoPopDelay = 5000;
            this.introTooltip1.AutoToolTipTitle = true;
            this.introTooltip1.InitialDelay = 500;
            this.introTooltip1.IntroText = "Welcome to Automated Result Management System";
            this.introTooltip1.IsBalloon = true;
            this.introTooltip1.OwnerDraw = true;
            this.introTooltip1.ReshowDelay = 1000;
            this.introTooltip1.ShowAlways = true;
            this.introTooltip1.Title = null;
            this.introTooltip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // loginForm
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(464, 262);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.loginMode);
            this.Controls.Add(this.textUsername);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textPassword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(480, 300);
            this.MinimumSize = new System.Drawing.Size(480, 300);
            this.Name = "loginForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Welcome to the Automated Result Management System";
            this.Load += new System.EventHandler(this.loginForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TextBox textUsername;
        private System.Windows.Forms.MaskedTextBox textPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox loginMode;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private introTooltip introTooltip1;
    }
}
