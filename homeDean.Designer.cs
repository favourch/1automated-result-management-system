namespace ARMS
{
    partial class homeDean
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(homeDean));
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelMode = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripComboCourse = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolTextAcademicYear = new System.Windows.Forms.ToolStripTextBox();
            this.btnAnnouncements = new System.Windows.Forms.ToolStripButton();
            this.toolButtonLock = new System.Windows.Forms.ToolStripButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageHODHome = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBoxMessages = new System.Windows.Forms.GroupBox();
            this.richTextBoxAnnouncements = new System.Windows.Forms.RichTextBox();
            this.buttonPreviousAnnounce = new System.Windows.Forms.Button();
            this.buttonNextAnnounce = new System.Windows.Forms.Button();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.displayPic = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dataGridActivity = new System.Windows.Forms.DataGridView();
            this.activityDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activityText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageHODResults = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelApproved = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.studentsData = new System.Windows.Forms.DataGridView();
            this.StudentSn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExamScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBoxResultSummary = new System.Windows.Forms.GroupBox();
            this.tableDetails = new System.Windows.Forms.TableLayoutPanel();
            this.labelNR = new System.Windows.Forms.Label();
            this.labelA = new System.Windows.Forms.Label();
            this.labelAR = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelF = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            this.labelE = new System.Windows.Forms.Label();
            this.labelD = new System.Windows.Forms.Label();
            this.btnReject = new ARMS.RibbonMenuButton();
            this.btnApprove = new ARMS.RibbonMenuButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxDepartments = new System.Windows.Forms.ComboBox();
            this.comboCourse = new System.Windows.Forms.ComboBox();
            this.tabPageStaff = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonSaveStaff = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.buttonDeleteStaff = new System.Windows.Forms.Button();
            this.buttonEditStaff = new System.Windows.Forms.Button();
            this.buttonAddStaff = new System.Windows.Forms.Button();
            this.dataGridViewStaff = new System.Windows.Forms.DataGridView();
            this.staffID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonAssignHOD = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxLecturers = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxHDept = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.toolBtnAbout = new System.Windows.Forms.ToolStripButton();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageHODHome.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBoxMessages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).BeginInit();
            this.tabPageHODResults.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBoxResultSummary.SuspendLayout();
            this.tableDetails.SuspendLayout();
            this.tabPageStaff.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStaff)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.labelMode);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(816, 62);
            this.panel2.TabIndex = 2;
            // 
            // labelMode
            // 
            this.labelMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.labelMode.Font = new System.Drawing.Font("Quartz MS", 18F, System.Drawing.FontStyle.Bold);
            this.labelMode.ForeColor = System.Drawing.Color.Red;
            this.labelMode.Location = new System.Drawing.Point(628, 12);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(176, 39);
            this.labelMode.TabIndex = 5;
            this.labelMode.Text = "ONLINE";
            this.labelMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboCourse,
            this.toolStripLabel1,
            this.toolTextAcademicYear,
            this.btnAnnouncements,
            this.toolButtonLock,
            this.toolBtnAbout});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(816, 62);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripComboCourse
            // 
            this.toolStripComboCourse.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolStripComboCourse.Name = "toolStripComboCourse";
            this.toolStripComboCourse.Size = new System.Drawing.Size(160, 62);
            this.toolStripComboCourse.Text = "Select Course";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(105, 59);
            this.toolStripLabel1.Text = "Academic Year:";
            // 
            // toolTextAcademicYear
            // 
            this.toolTextAcademicYear.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolTextAcademicYear.Name = "toolTextAcademicYear";
            this.toolTextAcademicYear.Padding = new System.Windows.Forms.Padding(3);
            this.toolTextAcademicYear.Size = new System.Drawing.Size(98, 62);
            this.toolTextAcademicYear.Text = "2012/2013";
            // 
            // btnAnnouncements
            // 
            this.btnAnnouncements.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAnnouncements.Image = global::ARMS.Properties.Resources.announcements;
            this.btnAnnouncements.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAnnouncements.Name = "btnAnnouncements";
            this.btnAnnouncements.Size = new System.Drawing.Size(52, 59);
            this.btnAnnouncements.Text = "New Announcement";
            this.btnAnnouncements.Click += new System.EventHandler(this.btnAnnouncements_Click);
            // 
            // toolButtonLock
            // 
            this.toolButtonLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonLock.Image = global::ARMS.Properties.Resources.Lock;
            this.toolButtonLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonLock.Name = "toolButtonLock";
            this.toolButtonLock.Size = new System.Drawing.Size(52, 59);
            this.toolButtonLock.Text = "Lock Session";
            this.toolButtonLock.Click += new System.EventHandler(this.toolButtonLock_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageHODHome);
            this.tabControl1.Controls.Add(this.tabPageHODResults);
            this.tabControl1.Controls.Add(this.tabPageStaff);
            this.tabControl1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.tabControl1.Location = new System.Drawing.Point(3, 63);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(9, 6);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(813, 501);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPageHODHome
            // 
            this.tabPageHODHome.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageHODHome.Controls.Add(this.panel4);
            this.tabPageHODHome.Controls.Add(this.panel6);
            this.tabPageHODHome.Location = new System.Drawing.Point(4, 28);
            this.tabPageHODHome.Name = "tabPageHODHome";
            this.tabPageHODHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHODHome.Size = new System.Drawing.Size(805, 469);
            this.tabPageHODHome.TabIndex = 6;
            this.tabPageHODHome.Text = "Home";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.groupBoxMessages);
            this.panel4.Controls.Add(this.labelWelcome);
            this.panel4.Controls.Add(this.displayPic);
            this.panel4.Location = new System.Drawing.Point(3, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(799, 245);
            this.panel4.TabIndex = 4;
            // 
            // groupBoxMessages
            // 
            this.groupBoxMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMessages.Controls.Add(this.richTextBoxAnnouncements);
            this.groupBoxMessages.Controls.Add(this.buttonPreviousAnnounce);
            this.groupBoxMessages.Controls.Add(this.buttonNextAnnounce);
            this.groupBoxMessages.Location = new System.Drawing.Point(8, 41);
            this.groupBoxMessages.Name = "groupBoxMessages";
            this.groupBoxMessages.Size = new System.Drawing.Size(642, 201);
            this.groupBoxMessages.TabIndex = 3;
            this.groupBoxMessages.TabStop = false;
            this.groupBoxMessages.Text = "Announcements";
            // 
            // richTextBoxAnnouncements
            // 
            this.richTextBoxAnnouncements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxAnnouncements.Location = new System.Drawing.Point(6, 22);
            this.richTextBoxAnnouncements.Name = "richTextBoxAnnouncements";
            this.richTextBoxAnnouncements.Size = new System.Drawing.Size(630, 140);
            this.richTextBoxAnnouncements.TabIndex = 2;
            this.richTextBoxAnnouncements.Text = "No announcements found";
            // 
            // buttonPreviousAnnounce
            // 
            this.buttonPreviousAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPreviousAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonPreviousAnnounce.Location = new System.Drawing.Point(454, 168);
            this.buttonPreviousAnnounce.Name = "buttonPreviousAnnounce";
            this.buttonPreviousAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonPreviousAnnounce.TabIndex = 1;
            this.buttonPreviousAnnounce.Text = "Previous";
            this.buttonPreviousAnnounce.UseVisualStyleBackColor = true;
            // 
            // buttonNextAnnounce
            // 
            this.buttonNextAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonNextAnnounce.Location = new System.Drawing.Point(548, 168);
            this.buttonNextAnnounce.Name = "buttonNextAnnounce";
            this.buttonNextAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonNextAnnounce.TabIndex = 1;
            this.buttonNextAnnounce.Text = "Next";
            this.buttonNextAnnounce.UseVisualStyleBackColor = true;
            // 
            // labelWelcome
            // 
            this.labelWelcome.BackColor = System.Drawing.Color.Transparent;
            this.labelWelcome.Font = new System.Drawing.Font("Trebuchet MS", 18F);
            this.labelWelcome.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelWelcome.Location = new System.Drawing.Point(3, 4);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(488, 33);
            this.labelWelcome.TabIndex = 1;
            this.labelWelcome.Text = "Good day Mr. UNN. ";
            this.labelWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // displayPic
            // 
            this.displayPic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.displayPic.Image = global::ARMS.Properties.Resources.UNN_Logo1;
            this.displayPic.Location = new System.Drawing.Point(656, 51);
            this.displayPic.Name = "displayPic";
            this.displayPic.Size = new System.Drawing.Size(138, 185);
            this.displayPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.displayPic.TabIndex = 0;
            this.displayPic.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.AliceBlue;
            this.panel6.Controls.Add(this.dataGridActivity);
            this.panel6.Location = new System.Drawing.Point(0, 257);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(5);
            this.panel6.Size = new System.Drawing.Size(805, 206);
            this.panel6.TabIndex = 5;
            // 
            // dataGridActivity
            // 
            this.dataGridActivity.AllowUserToAddRows = false;
            this.dataGridActivity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridActivity.BackgroundColor = System.Drawing.Color.White;
            this.dataGridActivity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridActivity.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridActivity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridActivity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.activityDate,
            this.activityText});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridActivity.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridActivity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridActivity.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridActivity.EnableHeadersVisualStyles = false;
            this.dataGridActivity.GridColor = System.Drawing.Color.White;
            this.dataGridActivity.Location = new System.Drawing.Point(5, 5);
            this.dataGridActivity.MultiSelect = false;
            this.dataGridActivity.Name = "dataGridActivity";
            this.dataGridActivity.ReadOnly = true;
            this.dataGridActivity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridActivity.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridActivity.Size = new System.Drawing.Size(795, 196);
            this.dataGridActivity.TabIndex = 3;
            this.dataGridActivity.TabStop = false;
            // 
            // activityDate
            // 
            this.activityDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.activityDate.DividerWidth = 1;
            this.activityDate.FillWeight = 25F;
            this.activityDate.HeaderText = "Date";
            this.activityDate.MinimumWidth = 150;
            this.activityDate.Name = "activityDate";
            this.activityDate.ReadOnly = true;
            this.activityDate.Width = 160;
            // 
            // activityText
            // 
            this.activityText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.activityText.HeaderText = "Details";
            this.activityText.Name = "activityText";
            this.activityText.ReadOnly = true;
            // 
            // tabPageHODResults
            // 
            this.tabPageHODResults.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageHODResults.Controls.Add(this.panel1);
            this.tabPageHODResults.Location = new System.Drawing.Point(4, 28);
            this.tabPageHODResults.Name = "tabPageHODResults";
            this.tabPageHODResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHODResults.Size = new System.Drawing.Size(805, 469);
            this.tabPageHODResults.TabIndex = 7;
            this.tabPageHODResults.Text = "Approve Results";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelApproved);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.studentsData);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.comboBoxDepartments);
            this.panel1.Controls.Add(this.comboCourse);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(799, 463);
            this.panel1.TabIndex = 10;
            // 
            // labelApproved
            // 
            this.labelApproved.AutoEllipsis = true;
            this.labelApproved.Location = new System.Drawing.Point(682, 17);
            this.labelApproved.Name = "labelApproved";
            this.labelApproved.Size = new System.Drawing.Size(47, 21);
            this.labelApproved.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(602, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 18);
            this.label7.TabIndex = 13;
            this.label7.Text = "Approved:";
            // 
            // studentsData
            // 
            this.studentsData.AllowUserToAddRows = false;
            this.studentsData.AllowUserToDeleteRows = false;
            this.studentsData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.studentsData.BackgroundColor = System.Drawing.Color.White;
            this.studentsData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentsData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StudentSn,
            this.StudentName,
            this.RegNo,
            this.CAScore,
            this.ExamScore,
            this.TotalScore,
            this.Grade});
            this.studentsData.Location = new System.Drawing.Point(0, 53);
            this.studentsData.Name = "studentsData";
            this.studentsData.ReadOnly = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsData.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.studentsData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.studentsData.Size = new System.Drawing.Size(791, 287);
            this.studentsData.TabIndex = 11;
            // 
            // StudentSn
            // 
            this.StudentSn.DividerWidth = 1;
            this.StudentSn.HeaderText = "SN.";
            this.StudentSn.MaxInputLength = 900;
            this.StudentSn.Name = "StudentSn";
            this.StudentSn.ReadOnly = true;
            this.StudentSn.ToolTipText = "Serial Number";
            this.StudentSn.Width = 40;
            // 
            // StudentName
            // 
            this.StudentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StudentName.DividerWidth = 1;
            this.StudentName.HeaderText = "Name of Student";
            this.StudentName.Name = "StudentName";
            this.StudentName.ReadOnly = true;
            this.StudentName.ToolTipText = "Student Name";
            // 
            // RegNo
            // 
            this.RegNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RegNo.DividerWidth = 1;
            this.RegNo.HeaderText = "Reg. No.";
            this.RegNo.Name = "RegNo";
            this.RegNo.ReadOnly = true;
            this.RegNo.ToolTipText = "Student Registration Number";
            this.RegNo.Width = 79;
            // 
            // CAScore
            // 
            this.CAScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CAScore.DividerWidth = 1;
            this.CAScore.HeaderText = "CA Mark";
            this.CAScore.MaxInputLength = 2;
            this.CAScore.Name = "CAScore";
            this.CAScore.ReadOnly = true;
            this.CAScore.ToolTipText = "Continous Assessment";
            this.CAScore.Width = 77;
            // 
            // ExamScore
            // 
            this.ExamScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ExamScore.DividerWidth = 1;
            this.ExamScore.HeaderText = "Exam Mark";
            this.ExamScore.MaxInputLength = 2;
            this.ExamScore.Name = "ExamScore";
            this.ExamScore.ReadOnly = true;
            this.ExamScore.ToolTipText = "Examination Mark";
            this.ExamScore.Width = 91;
            // 
            // TotalScore
            // 
            this.TotalScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TotalScore.DividerWidth = 1;
            this.TotalScore.FillWeight = 10F;
            this.TotalScore.HeaderText = "Total";
            this.TotalScore.MaxInputLength = 3;
            this.TotalScore.Name = "TotalScore";
            this.TotalScore.ReadOnly = true;
            this.TotalScore.ToolTipText = "Total Mark";
            this.TotalScore.Width = 64;
            // 
            // Grade
            // 
            this.Grade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Grade.DividerWidth = 1;
            this.Grade.FillWeight = 10F;
            this.Grade.HeaderText = "Grade";
            this.Grade.MaxInputLength = 2;
            this.Grade.Name = "Grade";
            this.Grade.ReadOnly = true;
            this.Grade.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Grade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Grade.ToolTipText = "Result Letter Grade";
            this.Grade.Width = 50;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBoxResultSummary);
            this.panel3.Controls.Add(this.btnReject);
            this.panel3.Controls.Add(this.btnApprove);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 338);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5);
            this.panel3.Size = new System.Drawing.Size(799, 125);
            this.panel3.TabIndex = 3;
            // 
            // groupBoxResultSummary
            // 
            this.groupBoxResultSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxResultSummary.Controls.Add(this.tableDetails);
            this.groupBoxResultSummary.Location = new System.Drawing.Point(8, 8);
            this.groupBoxResultSummary.Name = "groupBoxResultSummary";
            this.groupBoxResultSummary.Size = new System.Drawing.Size(374, 106);
            this.groupBoxResultSummary.TabIndex = 3;
            this.groupBoxResultSummary.TabStop = false;
            this.groupBoxResultSummary.Text = "Result Summary";
            // 
            // tableDetails
            // 
            this.tableDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tableDetails.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableDetails.ColumnCount = 4;
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.93617F));
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.06383F));
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189F));
            this.tableDetails.Controls.Add(this.labelNR, 3, 1);
            this.tableDetails.Controls.Add(this.labelA, 0, 0);
            this.tableDetails.Controls.Add(this.labelAR, 3, 0);
            this.tableDetails.Controls.Add(this.labelB, 0, 1);
            this.tableDetails.Controls.Add(this.labelF, 2, 1);
            this.tableDetails.Controls.Add(this.labelC, 1, 0);
            this.tableDetails.Controls.Add(this.labelE, 2, 0);
            this.tableDetails.Controls.Add(this.labelD, 1, 1);
            this.tableDetails.Location = new System.Drawing.Point(6, 22);
            this.tableDetails.Name = "tableDetails";
            this.tableDetails.RowCount = 2;
            this.tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableDetails.Size = new System.Drawing.Size(362, 78);
            this.tableDetails.TabIndex = 4;
            // 
            // labelNR
            // 
            this.labelNR.AutoSize = true;
            this.labelNR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNR.Location = new System.Drawing.Point(173, 40);
            this.labelNR.Name = "labelNR";
            this.labelNR.Size = new System.Drawing.Size(184, 36);
            this.labelNR.TabIndex = 0;
            this.labelNR.Text = "No Results:";
            this.labelNR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelA.Location = new System.Drawing.Point(5, 2);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(40, 36);
            this.labelA.TabIndex = 0;
            this.labelA.Text = "As:";
            this.labelA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAR
            // 
            this.labelAR.AutoSize = true;
            this.labelAR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAR.Location = new System.Drawing.Point(173, 2);
            this.labelAR.Name = "labelAR";
            this.labelAR.Size = new System.Drawing.Size(184, 36);
            this.labelAR.TabIndex = 0;
            this.labelAR.Text = "Ommitted Results:";
            this.labelAR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelB.Location = new System.Drawing.Point(5, 40);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(40, 36);
            this.labelB.TabIndex = 0;
            this.labelB.Text = "Bs:";
            this.labelB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelF
            // 
            this.labelF.AutoSize = true;
            this.labelF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelF.Location = new System.Drawing.Point(103, 40);
            this.labelF.Name = "labelF";
            this.labelF.Size = new System.Drawing.Size(62, 36);
            this.labelF.TabIndex = 0;
            this.labelF.Text = "Fs:";
            this.labelF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC.Location = new System.Drawing.Point(53, 2);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(42, 36);
            this.labelC.TabIndex = 0;
            this.labelC.Text = "Cs:";
            this.labelC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelE
            // 
            this.labelE.AutoSize = true;
            this.labelE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelE.Location = new System.Drawing.Point(103, 2);
            this.labelE.Name = "labelE";
            this.labelE.Size = new System.Drawing.Size(62, 36);
            this.labelE.TabIndex = 0;
            this.labelE.Text = "Es:";
            this.labelE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelD
            // 
            this.labelD.AutoSize = true;
            this.labelD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelD.Location = new System.Drawing.Point(53, 40);
            this.labelD.Name = "labelD";
            this.labelD.Size = new System.Drawing.Size(42, 36);
            this.labelD.TabIndex = 0;
            this.labelD.Text = "Ds:";
            this.labelD.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnReject
            // 
            this.btnReject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReject.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnReject.BackColor = System.Drawing.Color.Transparent;
            this.btnReject.ColorBase = System.Drawing.Color.Gainsboro;
            this.btnReject.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))));
            this.btnReject.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReject.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnReject.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnReject.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnReject.Enabled = false;
            this.btnReject.FadingSpeed = 35;
            this.btnReject.FlatAppearance.BorderSize = 0;
            this.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReject.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnReject.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnReject.Image = global::ARMS.Properties.Resources.Delete;
            this.btnReject.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnReject.ImageOffset = 5;
            this.btnReject.IsPressed = false;
            this.btnReject.KeepPress = false;
            this.btnReject.Location = new System.Drawing.Point(648, 8);
            this.btnReject.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnReject.MenuPos = new System.Drawing.Point(0, 0);
            this.btnReject.Name = "btnReject";
            this.btnReject.Radius = 6;
            this.btnReject.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnReject.Size = new System.Drawing.Size(143, 106);
            this.btnReject.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnReject.SplitDistance = 0;
            this.btnReject.TabIndex = 1;
            this.btnReject.Text = "Reject Results";
            this.btnReject.Title = "";
            this.btnReject.UseVisualStyleBackColor = false;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnApprove
            // 
            this.btnApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApprove.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnApprove.BackColor = System.Drawing.Color.Transparent;
            this.btnApprove.ColorBase = System.Drawing.Color.Gainsboro;
            this.btnApprove.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))));
            this.btnApprove.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnApprove.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnApprove.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnApprove.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnApprove.Enabled = false;
            this.btnApprove.FadingSpeed = 35;
            this.btnApprove.FlatAppearance.BorderSize = 0;
            this.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApprove.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnApprove.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnApprove.Image = global::ARMS.Properties.Resources.apply;
            this.btnApprove.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnApprove.ImageOffset = 5;
            this.btnApprove.IsPressed = false;
            this.btnApprove.KeepPress = false;
            this.btnApprove.Location = new System.Drawing.Point(497, 8);
            this.btnApprove.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnApprove.MenuPos = new System.Drawing.Point(0, 0);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Radius = 6;
            this.btnApprove.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnApprove.Size = new System.Drawing.Size(143, 106);
            this.btnApprove.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnApprove.SplitDistance = 0;
            this.btnApprove.TabIndex = 2;
            this.btnApprove.Text = "Approve Results";
            this.btnApprove.Title = "";
            this.btnApprove.UseVisualStyleBackColor = false;
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Candara", 9.75F);
            this.label6.Location = new System.Drawing.Point(17, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "Department:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Candara", 9.75F);
            this.label4.Location = new System.Drawing.Point(335, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Course:";
            // 
            // comboBoxDepartments
            // 
            this.comboBoxDepartments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartments.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboBoxDepartments.FormattingEnabled = true;
            this.comboBoxDepartments.Location = new System.Drawing.Point(101, 15);
            this.comboBoxDepartments.Name = "comboBoxDepartments";
            this.comboBoxDepartments.Size = new System.Drawing.Size(206, 24);
            this.comboBoxDepartments.TabIndex = 4;
            this.comboBoxDepartments.SelectedIndexChanged += new System.EventHandler(this.comboBoxDepartments_SelectedIndexChanged);
            // 
            // comboCourse
            // 
            this.comboCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCourse.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboCourse.FormattingEnabled = true;
            this.comboCourse.Location = new System.Drawing.Point(389, 15);
            this.comboCourse.Name = "comboCourse";
            this.comboCourse.Size = new System.Drawing.Size(174, 24);
            this.comboCourse.TabIndex = 4;
            this.comboCourse.SelectedIndexChanged += new System.EventHandler(this.comboCourse_SelectedIndexChanged);
            // 
            // tabPageStaff
            // 
            this.tabPageStaff.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageStaff.Controls.Add(this.groupBox1);
            this.tabPageStaff.Controls.Add(this.groupBox3);
            this.tabPageStaff.Location = new System.Drawing.Point(4, 28);
            this.tabPageStaff.Name = "tabPageStaff";
            this.tabPageStaff.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStaff.Size = new System.Drawing.Size(805, 469);
            this.tabPageStaff.TabIndex = 5;
            this.tabPageStaff.Text = "Manage Staff";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel5);
            this.groupBox1.Controls.Add(this.dataGridViewStaff);
            this.groupBox1.Location = new System.Drawing.Point(10, 132);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(792, 326);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "View All Staff";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.buttonSaveStaff);
            this.panel5.Controls.Add(this.buttonDeleteStaff);
            this.panel5.Controls.Add(this.buttonEditStaff);
            this.panel5.Controls.Add(this.buttonAddStaff);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(3, 272);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(786, 51);
            this.panel5.TabIndex = 4;
            // 
            // buttonSaveStaff
            // 
            this.buttonSaveStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSaveStaff.BackColor = System.Drawing.Color.Transparent;
            this.buttonSaveStaff.ForeColor = System.Drawing.Color.Black;
            this.buttonSaveStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSaveStaff.ImageKey = "apply.png";
            this.buttonSaveStaff.ImageList = this.imageList1;
            this.buttonSaveStaff.Location = new System.Drawing.Point(630, 6);
            this.buttonSaveStaff.Name = "buttonSaveStaff";
            this.buttonSaveStaff.Padding = new System.Windows.Forms.Padding(6);
            this.buttonSaveStaff.Size = new System.Drawing.Size(148, 43);
            this.buttonSaveStaff.TabIndex = 7;
            this.buttonSaveStaff.Text = "&Save Changes";
            this.buttonSaveStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSaveStaff.UseVisualStyleBackColor = false;
            this.buttonSaveStaff.Click += new System.EventHandler(this.buttonSaveStaff_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "print_48.png");
            this.imageList1.Images.SetKeyName(1, "cap_graduate.png");
            this.imageList1.Images.SetKeyName(2, "pencil.png");
            this.imageList1.Images.SetKeyName(3, "edit_add.png");
            this.imageList1.Images.SetKeyName(4, "edit_remove.png");
            this.imageList1.Images.SetKeyName(5, "apply.png");
            this.imageList1.Images.SetKeyName(6, "Occupé.png");
            // 
            // buttonDeleteStaff
            // 
            this.buttonDeleteStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDeleteStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonDeleteStaff.BackColor = System.Drawing.Color.Transparent;
            this.buttonDeleteStaff.ForeColor = System.Drawing.Color.Black;
            this.buttonDeleteStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDeleteStaff.ImageKey = "Occupé.png";
            this.buttonDeleteStaff.ImageList = this.imageList1;
            this.buttonDeleteStaff.Location = new System.Drawing.Point(201, 5);
            this.buttonDeleteStaff.Name = "buttonDeleteStaff";
            this.buttonDeleteStaff.Padding = new System.Windows.Forms.Padding(6);
            this.buttonDeleteStaff.Size = new System.Drawing.Size(104, 43);
            this.buttonDeleteStaff.TabIndex = 2;
            this.buttonDeleteStaff.Text = "&Delete";
            this.buttonDeleteStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonDeleteStaff.UseVisualStyleBackColor = false;
            this.buttonDeleteStaff.Click += new System.EventHandler(this.buttonDeleteStaff_Click);
            // 
            // buttonEditStaff
            // 
            this.buttonEditStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonEditStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonEditStaff.BackColor = System.Drawing.Color.Transparent;
            this.buttonEditStaff.ForeColor = System.Drawing.Color.Black;
            this.buttonEditStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEditStaff.ImageKey = "pencil.png";
            this.buttonEditStaff.ImageList = this.imageList1;
            this.buttonEditStaff.Location = new System.Drawing.Point(105, 6);
            this.buttonEditStaff.Name = "buttonEditStaff";
            this.buttonEditStaff.Padding = new System.Windows.Forms.Padding(6);
            this.buttonEditStaff.Size = new System.Drawing.Size(90, 43);
            this.buttonEditStaff.TabIndex = 2;
            this.buttonEditStaff.Text = "&Edit";
            this.buttonEditStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEditStaff.UseVisualStyleBackColor = false;
            this.buttonEditStaff.Click += new System.EventHandler(this.buttonEditStaff_Click);
            // 
            // buttonAddStaff
            // 
            this.buttonAddStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAddStaff.BackColor = System.Drawing.Color.Transparent;
            this.buttonAddStaff.ForeColor = System.Drawing.Color.Black;
            this.buttonAddStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddStaff.ImageKey = "edit_add.png";
            this.buttonAddStaff.ImageList = this.imageList1;
            this.buttonAddStaff.Location = new System.Drawing.Point(10, 5);
            this.buttonAddStaff.Name = "buttonAddStaff";
            this.buttonAddStaff.Padding = new System.Windows.Forms.Padding(6);
            this.buttonAddStaff.Size = new System.Drawing.Size(89, 43);
            this.buttonAddStaff.TabIndex = 2;
            this.buttonAddStaff.Text = "&Add";
            this.buttonAddStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddStaff.UseVisualStyleBackColor = false;
            this.buttonAddStaff.Click += new System.EventHandler(this.buttonAddStaff_Click);
            // 
            // dataGridViewStaff
            // 
            this.dataGridViewStaff.AllowUserToAddRows = false;
            this.dataGridViewStaff.AllowUserToDeleteRows = false;
            this.dataGridViewStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewStaff.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewStaff.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStaff.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.staffID,
            this.staffName,
            this.staffCode});
            this.dataGridViewStaff.Location = new System.Drawing.Point(6, 22);
            this.dataGridViewStaff.Name = "dataGridViewStaff";
            this.dataGridViewStaff.Size = new System.Drawing.Size(783, 250);
            this.dataGridViewStaff.TabIndex = 3;
            this.dataGridViewStaff.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewStaff_CellBeginEdit);
            this.dataGridViewStaff.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStaff_CellEndEdit);
            // 
            // staffID
            // 
            this.staffID.HeaderText = "ID";
            this.staffID.Name = "staffID";
            this.staffID.Width = 44;
            // 
            // staffName
            // 
            this.staffName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.staffName.HeaderText = "Name";
            this.staffName.Name = "staffName";
            // 
            // staffCode
            // 
            this.staffCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.staffCode.FillWeight = 33F;
            this.staffCode.HeaderText = "Staff Code";
            this.staffCode.Name = "staffCode";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.buttonAssignHOD);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.comboBoxLecturers);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.comboBoxHDept);
            this.groupBox3.Location = new System.Drawing.Point(10, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(789, 98);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Head of Department Assignment";
            // 
            // buttonAssignHOD
            // 
            this.buttonAssignHOD.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAssignHOD.BackColor = System.Drawing.Color.Transparent;
            this.buttonAssignHOD.ForeColor = System.Drawing.Color.Black;
            this.buttonAssignHOD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAssignHOD.ImageKey = "cap_graduate.png";
            this.buttonAssignHOD.ImageList = this.imageList1;
            this.buttonAssignHOD.Location = new System.Drawing.Point(621, 45);
            this.buttonAssignHOD.Name = "buttonAssignHOD";
            this.buttonAssignHOD.Padding = new System.Windows.Forms.Padding(6, 6, 3, 6);
            this.buttonAssignHOD.Size = new System.Drawing.Size(114, 38);
            this.buttonAssignHOD.TabIndex = 3;
            this.buttonAssignHOD.Text = "&Assign";
            this.buttonAssignHOD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAssignHOD.UseVisualStyleBackColor = false;
            this.buttonAssignHOD.Click += new System.EventHandler(this.buttonAssignHOD_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(322, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Choose Designated Staff:";
            // 
            // comboBoxLecturers
            // 
            this.comboBoxLecturers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLecturers.FormattingEnabled = true;
            this.comboBoxLecturers.Location = new System.Drawing.Point(325, 52);
            this.comboBoxLecturers.Name = "comboBoxLecturers";
            this.comboBoxLecturers.Size = new System.Drawing.Size(264, 26);
            this.comboBoxLecturers.TabIndex = 0;
            this.comboBoxLecturers.SelectedIndexChanged += new System.EventHandler(this.comboBoxLecturers_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Department:";
            // 
            // comboBoxHDept
            // 
            this.comboBoxHDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHDept.FormattingEnabled = true;
            this.comboBoxHDept.Location = new System.Drawing.Point(9, 52);
            this.comboBoxHDept.Name = "comboBoxHDept";
            this.comboBoxHDept.Size = new System.Drawing.Size(264, 26);
            this.comboBoxHDept.TabIndex = 0;
            this.comboBoxHDept.SelectedIndexChanged += new System.EventHandler(this.comboBoxHDept_SelectedIndexChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "A robust result management solution for Educational Institutions";
            this.notifyIcon1.BalloonTipTitle = "Automated Result Management System 1.0";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
            this.notifyIcon1.BalloonTipShown += new System.EventHandler(this.notifyIcon1_BalloonTipShown);
            // 
            // toolBtnAbout
            // 
            this.toolBtnAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnAbout.Image = global::ARMS.Properties.Resources.arms128;
            this.toolBtnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnAbout.Name = "toolBtnAbout";
            this.toolBtnAbout.Size = new System.Drawing.Size(52, 59);
            this.toolBtnAbout.Text = "About Automated Result Management System";
            // 
            // homeDean
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(816, 566);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(670, 473);
            this.Name = "homeDean";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dean of Faculty Administration Panel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.homeDean_Load);
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageHODHome.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBoxMessages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).EndInit();
            this.tabPageHODResults.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBoxResultSummary.ResumeLayout(false);
            this.tableDetails.ResumeLayout(false);
            this.tableDetails.PerformLayout();
            this.tabPageStaff.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStaff)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageStaff;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxLecturers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxHDept;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewStaff;
        private System.Windows.Forms.TabPage tabPageHODHome;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBoxMessages;
        private System.Windows.Forms.RichTextBox richTextBoxAnnouncements;
        private System.Windows.Forms.Button buttonPreviousAnnounce;
        private System.Windows.Forms.Button buttonNextAnnounce;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.PictureBox displayPic;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridView dataGridActivity;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityText;
        private System.Windows.Forms.TabPage tabPageHODResults;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelApproved;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView studentsData;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentSn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExamScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grade;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBoxResultSummary;
        private System.Windows.Forms.TableLayoutPanel tableDetails;
        private System.Windows.Forms.Label labelNR;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelAR;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelF;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Label labelE;
        private System.Windows.Forms.Label labelD;
        private RibbonMenuButton btnReject;
        private RibbonMenuButton btnApprove;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboCourse;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboCourse;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox toolTextAcademicYear;
        private System.Windows.Forms.ToolStripButton btnAnnouncements;
        private System.Windows.Forms.ToolStripButton toolButtonLock;
        private System.Windows.Forms.ComboBox comboBoxDepartments;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button buttonSaveStaff;
        private System.Windows.Forms.Button buttonDeleteStaff;
        private System.Windows.Forms.Button buttonEditStaff;
        private System.Windows.Forms.Button buttonAddStaff;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffID;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffName;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffCode;
        private System.Windows.Forms.Button buttonAssignHOD;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripButton toolBtnAbout;
    }
}

