using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace ARMS
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
            richTextBox1.Rtf =
                @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033\deflangfe1033{\fonttbl{\f0\fswiss\fprq2\fcharset0 Calibri;}{\f1\froman\fprq2\fcharset2 Symbol;}}
{\*\generator Msftedit 5.41.21.2510;}\viewkind4\uc1\pard\sa160\sl252\slmult1\b\f0\fs24 DEPARTMENT OF COMPUTER SCIENCE SOFTWARE TEAM\par
\pard\fi-360\li720\sa160\sl252\slmult1\b0\f1\'b7\tab\b\i\f0 Mr. S. C. Echezona\par
\b0\i0\f1\'b7\tab\b\i\f0 Engr. Collins Udeanor\par
\b0\i0\f1\'b7\tab\b\i\f0 Dr. M. C. Okoronkwo\par
\pard\sa160\sl252\slmult1\i0 DEPARTMENT OF GEOLOGY\i\par
\pard\fi-360\li720\sa160\sl252\slmult1\b0\i0\f1\'b7\tab\b\i\f0 Dr. Charles Ugbor\b0\i0\par
}
 ";
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(linkLabel1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            update(Application.ProductVersion);
        }

        public static void updateCheck()
        {
            WebClient wc = new WebClient();
            try
            {
                string[] info = wc.DownloadString(Helper.UpdateUrl).Split('~');
                string version = info[0];
                string[] oversiona = Application.ProductVersion.Split('.');
                string oversion = oversiona[0] + "." + oversiona[1];
                bool isNewer = Convert.ToDecimal(version) > Convert.ToDecimal(oversion);
                if (isNewer)
                {
                    if (MessageBox.Show(string.Format("A new update is available! Update Automated Result Management System Now to Enjoy the latest features.\n{0}", info[1]), "New Update Available", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        wc.DownloadFileCompleted += wc_DownloadFileCompleted;
                        wc.DownloadFileAsync(Helper.UpdateUrl, "temp_ARMS.exe");
                        MessageBox.Show("Downloading the new update. It will take some time to update the applicaiton. Please wait while the update complete.", "Update in Progress...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        p = new Processing();
                        p.ShowDialog();
                    }
                }
            }
            catch
            {
                if (MessageBox.Show("There was an error checking the newest version available. Do you want to try again?", "Update Failed", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                {
                    updateCheck();
                }
            }
        }

        private static Processing p;

        public static void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            p.Close();
            if (e.Cancelled || e.Error != null)
            {
                if (MessageBox.Show("An error occurred during the update. Do you want to try again?", "Update Failed", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Retry)
                {
                    updateCheck();
                }
            }
            else
            {
                MessageBox.Show("Automated Result Management System has been updated successfully. Press OK to restart and launch new version.", "Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                File.WriteAllText("update.bat", string.Format(@"pushd {0}
taskkill /f /im ARMS.exe
del ARMS.exe
ren temp_ARMS.exe ARMS.exe
start ARMS.exe", Application.StartupPath));
                batchRun("update.bat", false);
            }
        }

        public static void batchRun(string file, bool admin)
        {
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(file);
            psi.UseShellExecute = admin;
            psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            psi.CreateNoWindow = !admin;
            if (admin) psi.Verb = "runas";
            p.StartInfo = psi;
            p.Start();
            p.WaitForExit();
        }

        private static void update(object o)
        {
            string s = o as string;
            string ver = new WebClient().DownloadString(Helper.UpdateUrl);
            bool mandatory = ver[0] != '0';
            ver = ver.Substring(1);
            if (ver != s)
            {
                if (mandatory)
                {
                    MessageBox.Show("Smart Update has discovered your version of Automated Result Management System is outdated, This new update is mandatory. Please ensure you are connected to the internet and click ok to proceed with the update.", "Mandatory Update!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Process.Start("cmd.exe", "-k @echo off\necho Updating program. Please wait....");
                    Process.GetCurrentProcess().Kill();
                }
                else
                {
                    var dr = MessageBox.Show("Smart Update has discovered your version of Automated Result Management System is outdated, would you like to update now? (Please ensure you are connected to the internet before clicking yes)", "New Version Available!", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dr == DialogResult.Yes)
                    {
                        Process.Start("cmd.exe", "-k @echo off\necho Updating program. Please wait....");
                        Process.GetCurrentProcess().Kill();
                    }
                }
            }
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, System.EventArgs e)
        {
            TourMaster tm = new TourMaster();
            tm.Show();
        }


    }
}
