using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ARMS
{
    public partial class CriticalMessage : Form
    {
        private List<Messages> Messages;

        public CriticalMessage(List<Messages> messages)
        {
            InitializeComponent();
            Messages = messages;
            Messages x = messages[0];
            button1.Text = x.Subject + ":\n" + x.Message;
            if (messages.Count > 1)
            {
                for (int i = 1; i < messages.Count; i++)
                {
                    Messages m = messages[i];
                    Button b = button1;
                    b.Name += i;
                    b.Tag = i;
                    b.Text = m.Subject + ":\n" + m.Message;
                    flowLayoutPanel1.Controls.Add(b);
                }
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            var m = Messages[Convert.ToInt32(b.Tag)];
            StubbornWindow sw = new StubbornWindow(m);
            sw.Show();
            flowLayoutPanel1.Controls.Remove(b);
        }

        private void CriticalMessage_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(flowLayoutPanel1.Controls.Count > 0)
            {
                MessageBox.Show(
                    "You still have unread critical messages, please open each message, then try closing again.",
                    "Unread Critical Messages", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Cancel = true;
            }

        }
    }
}
