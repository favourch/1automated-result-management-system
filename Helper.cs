using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Serialization;
using Microsoft.Win32;
using MySql.Data.MySqlClient;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO;
using System.IO.Packaging;
using DocumentFormat.OpenXml;
using System.Runtime.InteropServices;

namespace ARMS
{
    public static class Utils
    {
        /// <summary>
        /// Converts the specified string To Title Case also called CamelCase
        /// </summary>
        /// <param name="s">The string to convert</param>
        /// <returns>The converted string</returns>
        public static string ToTitleCase(this string s)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(s);
        }

        /// <summary>
        /// Strip RichTextFormat from the string
        /// </summary>
        /// <param name="rtfString">The string to strip RTF from</param>
        /// <returns>The string without RTF</returns>
        public static string StripRTF(this string rtfString)
        {
            string result = rtfString;
            if (!rtfString.IsRtf()) return rtfString;
            try
            {
                // Put body into a RichTextBox so we can strip RTF
                using (System.Windows.Forms.RichTextBox rtfTemp = new System.Windows.Forms.RichTextBox())
                {
                    rtfTemp.Rtf = rtfString;
                    result = rtfTemp.Text;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
            }
            return result;
        }

        /// <summary>
        /// Checks if a message model object is inside a list of messages
        /// </summary>
        /// <param name="msg">The message object data</param>
        /// <param name="msgs">The list of messages to check</param>
        /// <returns>True if the message list contains a message with the same id as that being checked</returns>
        public static bool IsIn(this Messages msg, List<Messages> msgs)
        {
            return msgs.Any(n => n.Id == msg.Id);
        }

        /// <summary>
        /// Checks if an activity model object is inside a list of activities
        /// </summary>
        /// <param name="act">The activity data</param>
        /// <param name="acts">The activity list</param>
        /// <returns>True if the activity list contains an activity id the same as the one being checked</returns>
        public static bool IsIn(this Activity act, List<Activity> acts)
        {
            return acts.Any(n => n.Id == act.Id);
        }

        /// <summary>
        /// Checks s for RichTextFormat
        /// </summary>
        /// <param name="s">The string to check</param>
        /// <returns>True if s is in RichTextFormat</returns>
        public static bool IsRtf(this string s)
        {
            return (s != null) &&
                   (s.Trim().StartsWith("{\\rtf"));
        }

        /// <summary>
        /// Returns a string excerpt of the specified string using the specified length
        /// </summary>
        /// <param name="s">The string to extract the excerpt from</param>
        /// <param name="length">The length of text to extract</param>
        /// <returns>The excerpt with ellipsis</returns>
        public static string Excerpt(this string s, int length)
        {
            return s.Length > length ? s.Remove(length) + "..." : s;
        }
    }

    public class FileUnblocker
    {
        [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DeleteFile(string name);

        public static bool Unblock(string fileName)
        {
            return DeleteFile(fileName + ":Zone.Identifier");
        }
    }

    public static class Helper
    {
        /// <summary>
        /// The Programs Registry Key
        /// </summary>
        public static RegistryKey rk = Registry.CurrentUser.CreateSubKey("Software\\Robotic Systems\\Automated Result Management System");

        /// <summary>
        /// Internal Connection String
        /// </summary>
        public static string LocalConnection  = "server=10.0.0.11;port=3306;uid=admin;pwd=Innovation123;database=trwiz;Convert Zero Datetime=True";

        /// <summary>
        /// Internal Archive Connection String
        /// </summary>
        public static string netArchive = "server=10.0.0.11;port=3307;uid=root;pwd=Innovation1;database=ecarchive;Convert Zero Datetime=True";

        /// <summary>
        /// Local Connection String
        /// </summary>
        public static string netConnection= "server=127.0.0.1;port=3307;uid=root;pwd=usbw;database=trwiz;Convert Zero Datetime=True";

        /// <summary>
        /// Local Archive Connection String
        /// </summary>
        public static string BackupConnection = "server=10.0.0.11;port=3306;uid=admin;pwd=Innovation123;";

        /// <summary>
        /// Local Archive Connection String
        /// </summary>
        public static string LocalArchive = "server=10.0.0.11;port=3306;uid=admin;pwd=Innovation123;database=ecarchive;Convert Zero Datetime=True";

        /// <summary>
        /// Name of the archive DB
        /// </summary>
        public static string archiveDb;

        /// <summary>
        /// Name of the online db
        /// </summary>
        public static string netDb;

        /// <summary>
        /// Url to check for updates
        /// </summary>
        public static Uri UpdateUrl = new Uri("http://10.0.0.11:8080/ARMS.txt");

        /// <summary>
        /// MySql connection for the program
        /// </summary>
        public static string MyConnection
        {
            get { return Helper.netConnection; }
        }

        public static int Move(string table, string Key, string Value)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            MySqlConnection mca = new MySqlConnection(Helper.LocalArchive);
            string copytable = string.Format("INSERT INTO {0} SELECT * FROM {3}.{0} WHERE {1}='{2}'", table, Key, Value, netDb);
            string sql = string.Format("DELETE FROM {0} WHERE {1} = '{2}'", table, Key, Value);
            MySqlCommand msc = new MySqlCommand(copytable, mca);
            mca.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
                msc = new MySqlCommand(sql, mc);
                mc.Open();
                msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
                mca.Close();
            }
            return retVal;
        }

        public static int Move(string table, string[] args, string[] values, bool and)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            MySqlConnection mca = new MySqlConnection(Helper.LocalArchive);
            string copytable = string.Format("INSERT INTO {0} SELECT * FROM {3}.{0} WHERE {1}='{2}' {4}", table, args[0], values[0], netDb, and ? "AND" : "OR");
            string clause = "";
            for (int i = 1; i < values.Length; i++)
            {
                clause += string.Format("{2} {0}='{1}'", args[i], values[i], and ? "AND" : "OR");
            }
            string sql = string.Format("DELETE FROM {0} WHERE {1} = '{2}'", table, args[0], values[0]);
            MySqlCommand msc = new MySqlCommand(copytable + clause, mca);
            mca.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
                msc = new MySqlCommand(sql + clause, mc);
                mc.Open();
                msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
                mca.Close();
            }
            return retVal;

        }

        public static string XLGetCellValue(string fileName, string sheetName, string addressName)
        {
            string value = null;
            try
            {
                Package spreadsheetPackage = Package.Open(fileName, FileMode.Open, FileAccess.Read);
                using (SpreadsheetDocument document = SpreadsheetDocument.Open(spreadsheetPackage))
                {
                    WorkbookPart wbPart = document.WorkbookPart;

                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                      Where(s => s.Name == sheetName).FirstOrDefault();

                    if (theSheet == null)
                    {
                        return null;
                    }
                    
                    WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));
                    Cell theCell = wsPart.Worksheet.Descendants<Cell>().
                      Where(c => c.CellReference == addressName).FirstOrDefault();
                    if (theCell != null)
                    {
                        value = ProcessValue(theCell, wbPart);
                    }
                }
            }
            catch (IOException e)
            {
                AppHelper.showMessage(string.Format("The file {0} you are trying to access is in use by another application. Please close any applications that might be using the file and try again. If this problem persists, plese consider logging off and logging in again. Thanks", fileName), "Unauthorized Acesss" , 2);
            }
            catch (Exception e)
            {
                Logger.WriteLog(e.Source, e.Message + Environment.NewLine + DateTime.Now + Environment.NewLine + e.StackTrace);
            }
            return value;
        }

        private static string ProcessValue(Cell theCell, WorkbookPart wbPart)
        {
            if (theCell.CellValue == null || theCell.CellValue.InnerText == null) return null;
            string value = theCell.CellValue.InnerText;
            if (theCell.DataType != null)
            {
                switch (theCell.DataType.Value)
                {
                    case CellValues.SharedString:
                        var stringTable = wbPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                        if (stringTable != null)
                        {
                            value = stringTable.SharedStringTable.ElementAt(int.Parse(value)).InnerText;
                        }
                        break;

                    case CellValues.Boolean:
                        switch (value)
                        {
                            case "0":
                                value = "FALSE";
                                break;
                            default:
                                value = "TRUE";
                                break;
                        }
                        break;
                }
            }
            return value;
        }

        /// <summary>
        /// Inserts a collection of Student Data Objects to Excel Document
        /// </summary>
        /// <param name="docName">The name of the document</param>
        /// <param name="students">The StudentData list to insert</param>
        /// <param name="sheetName">The sheetname we will write to</param>
        /// <param name="columnStart">The column to start from</param>
        /// <param name="rowIndex">The row to start from</param>
        /// <param name="insertLength">The extent to insert data for the students</param>
        /// <returns>The cell reference for the last insertion</returns>
        public static string InsertStudenData(string docName, List<StudentData> students, string sheetName, string columnStart, uint rowIndex, int insertLength, int jumpLength, bool serialize)
        {
            //Unblock the file just in case
            FileUnblocker.Unblock(docName);
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
            {
                WorksheetPart worksheetPart =
                      GetWorksheetPartByName(spreadSheet, sheetName);

                if (worksheetPart != null)
                {
                    int inserted = 0;
                    int jumpCount = 0;
                    for (int i = 0; i < students.Count; i++)
                    {
                        char columnName = columnStart[0];
                        StudentData sd = students[i];
                        var items = sd.GetItemsShort();
                        //Insert only first four data which are the only necessary ones before they are filled in excel
                        int extra = (serialize ? 1 : 0);
                        for (int j = 0; j < insertLength + extra; j++)
                        {
                            try
                            {
                                string toInsert;
                                Cell cell;
                                if (j == 0 && serialize)
                                {
                                    toInsert = (inserted + 1).ToString();
                                    try
                                    {
                                        cell = GetCell(worksheetPart.Worksheet, columnName.ToString(), rowIndex);
                                    }
                                    catch
                                    {
                                        cell = InsertCellInWorksheet(columnName.ToString(), rowIndex, worksheetPart);
                                    }
                                    cell.CellValue = new CellValue(toInsert);
                                    cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                    columnName++;
                                }
                                toInsert = items[j];
                                try
                                {
                                    cell = GetCell(worksheetPart.Worksheet, columnName.ToString(), rowIndex);
                                }
                                catch
                                {
                                    cell = InsertCellInWorksheet(columnName.ToString(), rowIndex, worksheetPart);
                                }
                                cell.CellValue = new CellValue(toInsert);
                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                            }
                            catch (Exception e) { }
                            columnName++;
                        }
                        inserted++;
                        if (((jumpCount + 1) * jumpLength) == inserted)
                        {
                            rowIndex += (uint)(jumpLength - 1);
                            //inserted = 0;
                            jumpCount++;
                        }
                        rowIndex++;
                    }
                    worksheetPart.Worksheet.Save();
                }
            }
            return (columnStart[0] + insertLength).ToString() + rowIndex;
        }

        /// <summary>
        /// Inserts a collection of Student Data Objects to Excel Document
        /// </summary>
        /// <param name="docName">The name of the document</param>
        /// <param name="students">The StudentData list to insert</param>
        /// <param name="sheetName">The sheetname we will write to</param>
        /// <param name="columnStart">The column to start from</param>
        /// <param name="rowIndex">The row to start from</param>
        /// <returns>The cell reference for the last insertion</returns>
        public static string InsertBroadSheetData(string docName, Dictionary<string, List<StudentResult>> students, Dictionary<string, Course> courses,  string sheetName, int columnStart, uint rowIndex)
        {
            //Unblock the file just in case
            FileUnblocker.Unblock(docName);
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
            {
                WorksheetPart worksheetPart =
                      GetWorksheetPartByName(spreadSheet, sheetName);

                if (worksheetPart != null)
                {
                    int column = columnStart;
                    List<string> courseCodes = new List<string>();
                    //Let's insert the course details
                    foreach (var c in courses)
                    {
                        string toInsert = string.Format("{0}({1})", c.Key, c.Value.Units);
                        InsertCellString(worksheetPart, columnMap[column], 4, toInsert);
                        column++;
                        courseCodes.Add(c.Key);
                    }

                    //Let's insert results data
                    int inserted = 0;
                    foreach (var student in students)
                    {
                        //Get student name using first result
                        //Can be optimized by including name inside regno(key) instead, but hardly useful
                        string toInsert = student.Value[0].Name;
                        InsertCellNumber(worksheetPart, "A", rowIndex, (inserted + 1).ToString(CultureInfo.InvariantCulture));
                        InsertCellString(worksheetPart, "B", rowIndex, toInsert);
                        InsertCellString(worksheetPart, "C", rowIndex, student.Key);
                        int totalUnits = 0;
                        int actualUnits = 0;
                        int totalGp = 0;
                        for (int j = 0; j < student.Value.Count; j++)
                        {
                            var cstudent = student.Value[j];
                            try
                            {
                                //Find the correct index for the course
                                int ins = courseCodes.IndexOf(cstudent.CourseCode) + columnStart;
                                ushort units = courses[cstudent.CourseCode].Units;
                                actualUnits += units;
                                toInsert = cstudent.TotalScore;
                                if (cstudent.GpScore != -1)
                                {
                                    totalGp += cstudent.GpScore*units;
                                    totalUnits += units;
                                    InsertCellNumber(worksheetPart, columnMap[ins], rowIndex, toInsert);
                                }
                                else 
                                {
                                    InsertCellString(worksheetPart, columnMap[ins], rowIndex, toInsert);
                                }                                
                            }
                            catch (Exception e) { }
                        }

                        //Let's insert GP details ActualUnits, TotalUnits, TGP, and GPA in AR, AS, AT, AU

                        try
                        {
                            //Calculate the GPA
                            double gpa = totalGp/totalUnits;

                            InsertCellNumber(worksheetPart, "AO", rowIndex, actualUnits.ToString());
                            InsertCellNumber(worksheetPart, "AP", rowIndex, totalUnits.ToString());
                            InsertCellNumber(worksheetPart, "AQ", rowIndex, totalGp.ToString());
                            InsertCellNumber(worksheetPart, "AR", rowIndex, gpa.ToString());
                        }
                        catch (Exception e) { Logger.WriteLog(e); }

                        //So, we're done inserting result for this student, next please
                        rowIndex++;
                        inserted++;
                    }
                    worksheetPart.Worksheet.Save();
                    spreadSheet.Close();
                    return columnMap[column] + rowIndex;
                }
            }
            return null;
        }

        public static void GenerateExcel(string[] headers, List<string[]> data, string fileName)
        {
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(fileName, true))
            {
                WorksheetPart worksheetPart =
                    GetWorksheetPartByName(spreadSheet, "Sheet1");

                if (worksheetPart != null)
                {
                    int column = 0;
                    //Let's insert the headers,
                    foreach (var c in headers)
                    {
                        InsertCellString(worksheetPart, columnMap[column], 1, c);
                        column++;
                    }
                    uint rowIndex = 2;

                    //Let's insert the data
                    foreach (var c in data)
                    {
                        column = 0;
                        //Let's insert the headers,
                        foreach (var x in c)
                        {
                            InsertCellString(worksheetPart, columnMap[column], rowIndex, x);
                            column++;
                        }
                        rowIndex++;
                    }
                }
            }
        }

        private static void InsertCellNumber(WorksheetPart worksheetPart, string columnName, uint rowIndex, string toInsert)
        {
            Cell cell;
            try
            {
                cell = GetCell(worksheetPart.Worksheet, columnName, rowIndex) ??
                       InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            }
            catch
            {
                cell = InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            }
            cell.CellValue = new CellValue(toInsert);
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
        }

        private static void InsertCellString(WorksheetPart worksheetPart, string columnName, uint rowIndex, string toInsert)
        {
            Cell cell;
            try
            {
                cell = GetCell(worksheetPart.Worksheet, columnName, rowIndex) ??
                       InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            }
            catch
            {
                cell = InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            }
            cell.CellValue = new CellValue(toInsert);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        private static string[] columnMap = new[]
            {
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
                "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN",
                "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE",
                "BF", "BG", "BH", "BI", "BJ"
            };

        private static WorksheetPart GetWorksheetPartByName(SpreadsheetDocument document, string sheetName)
        {
            IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().
                Elements<Sheet>().Where(s => s.Name == sheetName);

            if (sheets.Count() == 0)
            {
                // The specified worksheet does not exist.
                return null;
            }

            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(relationshipId);
            return worksheetPart;
        }

        private static Cell GetCell(Worksheet worksheet, string columnName, uint rowIndex)
        {
            Row row = GetRow(worksheet, rowIndex);
            if (row == null)
            {
                return null;
            }

            IEnumerable<Cell> cells = row.Elements<Cell>().Where(c => string.Compare(c.CellReference.Value, columnName + rowIndex, true) == 0);
            if (!cells.Any()) return null;
            return cells.First();
        }


        // Given a worksheet and a row index, return the row.
        private static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
              Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }

        // Given a document name and text, 
        // inserts a new work sheet and writes the text to cell "A1" of the new worksheet.

        public static void InsertText(string docName, string text)
        {
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
            {
                // Get the SharedStringTablePart. If it does not exist, create a new one.
                SharedStringTablePart shareStringPart;
                if (spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Count() > 0)
                {
                    shareStringPart = spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
                }
                else
                {
                    shareStringPart = spreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
                }

                // Insert the text into the SharedStringTablePart.
                int index = InsertSharedStringItem(text, ref shareStringPart);

                // Insert a new worksheet.
                WorksheetPart worksheetPart = InsertNewWorksheet(spreadSheet.WorkbookPart);

                // Insert cell A1 into the new worksheet.
                Cell cell = InsertCellInWorksheet("A", 1, worksheetPart);

                // Set the value of cell A1.
                cell.CellValue = new CellValue(index.ToString());
                cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);

                // Save the new worksheet.
                worksheetPart.Worksheet.Save();
            }
        }

        // Given text and a SharedStringTablePart, creates a SharedStringItem with the specified text 
        // and inserts it into the SharedStringTablePart. If the item already exists, returns its index.
        private static int InsertSharedStringItem(string text, ref SharedStringTablePart shareStringPart)
        {
            // If the part does not contain a SharedStringTable, create one.
            if (shareStringPart.SharedStringTable == null)
            {
                shareStringPart.SharedStringTable = new SharedStringTable();
            }

            int i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (SharedStringItem item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerText == text)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            shareStringPart.SharedStringTable.Save();

            return i;
        }

        // Given a WorkbookPart, inserts a new worksheet.
        private static WorksheetPart InsertNewWorksheet(WorkbookPart workbookPart)
        {
            // Add a new worksheet part to the workbook.
            WorksheetPart newWorksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());
            newWorksheetPart.Worksheet.Save();

            Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = workbookPart.GetIdOfPart(newWorksheetPart);

            // Get a unique ID for the new sheet.
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Count() > 0)
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            string sheetName = "Sheet" + sheetId;

            // Append the new worksheet and associate it with the workbook.
            Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = sheetName };
            sheets.Append(sheet);
            workbookPart.Workbook.Save();

            return newWorksheetPart;
        }

        // Given a column name, a row index, and a WorksheetPart, inserts a cell into the worksheet. 
        // If the cell already exists, returns it. 
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.            
            var row = sheetData.Elements<Row>().FirstOrDefault(r => r.RowIndex == rowIndex);
            if (row == null)
            {
                row = new Row() { RowIndex = rowIndex};
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            var rcell = row.Elements<Cell>().FirstOrDefault(c => c.CellReference.Value == cellReference);
            if (rcell == null)
            {
                rcell = new Cell() { CellReference = cellReference };
            }
            row.Append(rcell);
            return rcell;
        }

        public static SpreadsheetDocument GetSheetDoc(string fileName)
        {
            SpreadsheetDocument doc = null;
            try
            {
                Package spreadsheetPackage = Package.Open(fileName, FileMode.Open, FileAccess.Read);
                doc = SpreadsheetDocument.Open(spreadsheetPackage);
            }
            catch (IOException e)
            {
                AppHelper.showMessage(string.Format("The file {0} you are trying to access is in use by another application. Please close any applications that might be using the file and try again. If this problem persists, plese consider logging off and logging in again. Thanks", fileName), "Unauthorized Acesss", 2);
            }
            catch (Exception e)
            {
                Logger.WriteLog(e);
            }
            return doc;
        }

        public static string cellByReference(Dictionary<string, Cell> cells, SpreadsheetDocument document, string addressName)
        {
            Cell theCell = null;
            cells.TryGetValue(addressName, out theCell);
            WorkbookPart wbPart = document.WorkbookPart;
            string value = ProcessValue(theCell, wbPart);
            return theCell == null ? null : value;
        }

        public static Dictionary<string, Cell> getRowCells(string fileName, string sheetName)
        {
            Dictionary<string, Cell> cells = null;
            try
            {
                Package spreadsheetPackage = Package.Open(fileName, FileMode.Open, FileAccess.Read);
                using (SpreadsheetDocument document = SpreadsheetDocument.Open(spreadsheetPackage))
                {
                    WorkbookPart wbPart = document.WorkbookPart;

                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                      Where(s => s.Name == sheetName).FirstOrDefault();

                    if (theSheet == null)
                    {
                        return null;
                    }

                    WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));
                    //Scan everything since we don't know which part contains the results
                    //Todo: Use student count for the course as countts, i.e. decsendants.any(count)
                    cells = wsPart.Worksheet.Descendants<Cell>().ToDictionary(c => c.CellReference.Value);
                }
            }
            catch (IOException e)
            {
                AppHelper.showMessage(string.Format("The file {0} you are trying to access is in use by another application. Please close any applications that might be using the file and try again. If this problem persists, plese consider logging off and logging in again. Thanks", fileName), "Unauthorized Acesss", 2);
            }
            catch (Exception e)
            {
                Logger.WriteLog(e.Source, e.Message + Environment.NewLine + DateTime.Now + Environment.NewLine + e.StackTrace);
            }
            return cells;
        }

        public static SerializableDictionary<string, LecturerCourse> GetLecturerCourses(string lecturerName, bool detailed)
        {
            SerializableDictionary<string, LecturerCourse> lcourses = new SerializableDictionary<string, LecturerCourse>();
            string[][] result;
            if (!detailed)
            {
                result =
                    selectResults(
                        new[]
                            {"CourseCode", "submitted", "dapproved", "fapproved", "department"},
                        "lecturer_courses",
                        new[] {"username", }, new[] {lecturerName}, "");
            }
            else
            {
                result =
                    selectResults(
                        new string[]
                            {},
                        "lecturer_courses",
                        new[] {"username", }, new[] {lecturerName},
                        "");
            }

            foreach (var s in result)
            {
                LecturerCourse mycourse = new LecturerCourse
                    {
                        CourseCode = s[0],
                        Submitted = s[1] == "Y",
                        HodApproved = s[2] == "" ? (bool?) null : s[2] == "Y",
                        DeanApproved = s[3] == "" ? (bool?) null : s[2] == "Y",
                        Department = s[4]
                    };
                    lcourses.Add(s[0]+":"+s[4], mycourse);
            }
            return result == null ? null : lcourses;
        }

        public static List<string> GetLecturerCourses(string lecturerName)
        {
            List<string> lcourses = new List<string>();
            var result = Helper.selectResults(new string[] { "CourseCode" }, "lecturer_courses",
                                               new string[] { "username" }, new string[] { lecturerName }, "");
            foreach (var s in result)
            {
                lcourses.Add(s[0]);
            }
            return result == null? null : lcourses;
        }

        /// <summary>
        /// Gets the message count of the specified user
        /// </summary>
        /// <param name="user">The user to get their message count</param>
        /// <returns>Returns 0 if no message is found or the number of messages for the specified user</returns>
        public static int GetUnreadMessageCount(string user)
        {
            try
            {
                //select count(messages) where user = 
                var results = selectResults(new[] {"count(id)"}, "messages", new[] {"recipient"}, new[] {user}, "");
                if (results != null)
                {
                    return Convert.ToInt32(results[0][0]);
                }
                return 0;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        /// <summary>
        /// Gets the messages associated with the specified user
        /// </summary>
        /// <param name="user">The user to retrieve their messaegs</param>
        /// <returns>A list of the message class of all the messages received by the user</returns>
        public static List<Messages> GetReceivedMessages(string user)
        {
            var results = selectResults(new[] { "recipient", "sender", "message", "subject", "date", "unread", "id", "type", "mtable", "mfield", "expected" }, "messages", new[] { "recipient"}, new[] { user }, "ORDER BY date DESC");
            if (results != null)
            {
                List<Messages> msgs = new List<Messages>();
                foreach (var r in results)
                {
                    Messages msg = new Messages();
                    msg.Date = Convert.ToDateTime(r[4]);
                    msg.Message = r[2];
                    msg.Recipient = r[0];
                    msg.Sender = r[1];
                    msg.Subject = r[3];
                    msg.Unread = r[5] == "Y";
                    msg.Id = r[6];
                    msg.Type = (MessageType)Convert.ToInt32(r[7]);
                    msg.Table = r[8];
                    msg.Field = r[9];
                    msg.Expected = r[10];
                    msgs.Add(msg);
                }
                return msgs;
            }
            return null;
        }

        /// <summary>
        /// Gets the messages associated with the specified user
        /// </summary>
        /// <param name="user">The user to retrieve their messaegs</param>
        /// <returns>A list of the message class of all the messages received by the user</returns>
        public static List<Messages> GetMessages(string user)
        {
            var results = selectResults(new[] { "recipient", "sender", "message", "subject", "date", "unread", "id", "type", "mtable", "mfield", "expected" }, "messages", new[] { "recipient", "sender" }, new[] { user, user }, "ORDER BY date DESC", "OR");
            if (results != null)
            {
                List<Messages> msgs = new List<Messages>();
                foreach (var r in results)
                {
                    Messages msg = new Messages();
                    msg.Date = Convert.ToDateTime(r[4]);
                    msg.Message = r[2];
                    msg.Recipient = r[0];
                    msg.Sender = r[1];
                    msg.Subject = r[3];
                    msg.Unread = r[5] == "Y";
                    msg.Id = r[6];
                    msg.Type = (MessageType)Convert.ToInt32(r[7]);
                    msg.Table = r[8];
                    msg.Field = r[9];
                    msg.Expected = r[10];
                    msgs.Add(msg);
                }
                return msgs;
            }
            return null;
        }

        /// <summary>
        /// Sends a normal message to a specified user
        /// </summary>
        /// <param name="recipient">The username of the recipient</param>
        /// <param name="sender">The username of the sender</param>
        /// <param name="message">The message to send</param>
        /// <param name="subject">The subject of the message</param>
        /// <returns></returns>
        public static int sendMessage(string recipient, string sender, string message, string subject)
        {
            return dbInsert("messages", new[] { "id", "recipient", "sender", "message", "subject", "date", "unread" }, new[] { "NULL", recipient, sender, message, subject, DateTime.Now.ToString(), "0" });
        }

        /// <summary>
        /// Sends a critical message to the specified user
        /// </summary>
        /// <param name="recipient">The username of the recipient receiving the message</param>
        /// <param name="sender">The username of the current sender</param>
        /// <param name="message">The message to send</param>
        /// <param name="subject">The subject of the message</param>
        /// <param name="table">The name of the table we are checking for</param>
        /// <param name="field">The field to watch</param>
        /// <param name="expected">The expected value for the specified field</param>
        /// <returns></returns>
        public static int sendMessage(string recipient, string sender, string message, string subject, string table, string field, string expected, string args, string argvalues)
        {
            int i = dbInsert("messages", new[] { "id", "recipient", "sender", "message", "subject", "date", "unread", "type", "mtable", "mfield", "expected" }, new[] { "NULL", recipient, sender, message, subject, DateTime.Now.ToString(), "0", "1", table, field, expected });
            return insertCritical(i.ToString(), args, argvalues);
        }

        private static int insertCritical(string id, string args, string argvalues)
        {
            return dbInsert("critical", new[] {"id", "args", "argvalues"}, new[] {id, args, argvalues});
        }

        /// <summary>
        /// Inserts fields and values to a specified table
        /// </summary>
        /// <param name="table">The table to insert values to</param>
        /// <param name="fields">The fields of the table to insert to</param>
        /// <param name="values">The values to insert</param>
        /// <param name="useIgnore">Determine if the query type should be INSERT IGNORE</param>
        /// <param name="escape">Determines if the values should be escaped</param>
        /// <returns>Returns -1 if the an error occurs during execution or the number of rows affected by the operation</returns>
        public static int dbInsert(string table, object fields, object values, bool useIgnore, bool escape)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string sql = genInsertQuery(table, fields, values, escape, useIgnore);
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Inserts fields and values to a specified table
        /// </summary>
        /// <param name="table">The table to insert values to</param>
        /// <param name="fields">The fields of the table to insert to</param>
        /// <param name="values">The values to insert</param>
        /// <param name="useIgnore">Specifies if we should use Insert Ignore</param>
        /// <returns>Returns -1 if the an error occurs during execution or the number of rows affected by the operation</returns>
        public static int dbInsert(string table, string[] fields, string[] values, bool useIgnore)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string sql = genInsertQuery(table, fields, values, false, useIgnore);
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Inserts fields and values to a specified table
        /// </summary>
        /// <param name="table">The table to insert values to</param>
        /// <param name="fields">The fields of the table to insert to</param>
        /// <param name="values">The values to insert</param>
        /// <returns>Returns -1 if the an error occurs during execution or the number of rows affected by the operation</returns>
        public static int dbInsert(string table, object fields, object values)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string sql = genInsertQuery(table, fields, values, false, false);
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Inserts fields and values to a specified table
        /// </summary>
        /// <param name="table">The table to insert values to</param>
        /// <param name="fields">The fields of the table to insert to</param>
        /// <param name="values">The values to insert</param>
        /// <returns>Returns -1 if the an error occurs during execution or the number of rows affected by the operation</returns>
        public static int dbInsert(string table, object fields, object values, bool escape)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string sql = genInsertQuery(table, fields, values, escape, false);
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Performs a direct mysql insert/update/delete command
        /// </summary>
        /// <param name="cmd">The command string</param>
        /// <param name="Connection">The connection string to use</param>
        /// <returns>Returns affected rows or -1 on failure.</returns>
        public static int ExecuteNonQuery(string cmd, string Connection)
        {
            MySqlConnection mc = new MySqlConnection(Connection);
            MySqlCommand msc = new MySqlCommand(cmd, mc);
            mc.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Performs a direct mysql insert/update/delete command
        /// </summary>
        /// <param name="cmd">The command string</param>
        /// <returns>Returns affected rows or -1 on failure.</returns>
        public static int ExecuteNonQuery(string cmd)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            MySqlCommand msc = new MySqlCommand(cmd, mc);
            mc.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Performs a direct mysql select command
        /// </summary>
        /// <param name="cmd">The command string</param>
        /// <returns>Returns a string multi-array of the results/returns>
        public static string[][] ExecuteSelect(string cmd)
        {
            MySqlConnection mc = new MySqlConnection(LocalConnection);
            MySqlCommand msc = new MySqlCommand(cmd, mc);
            try
            { mc.Open(); }
            catch
            {
                return null;
            }
            var dr = msc.ExecuteReader();
            List<string[]> result = new List<string[]>();
            try
            {
                while (dr.Read())
                {
                    List<string> lresult = new List<string>();
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        lresult.Add(dr[i].ToString());
                    }
                    result.Add(lresult.ToArray());
                }
                return result.ToArray();
            }
            catch (Exception e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                dr.Close();
                mc.Close();
            }
            return null;
        }

        public static string genInsertQuery(string table, object fields, object values, bool escape, bool useIgnore)
        {
            string[] fieldVals = fields as string[];
            string[] valData = values as string[];
            if (fieldVals.Length != valData.Length) { return "Number of fields not equal to values."; }
            string query = string.Format("INSERT {0} INTO {1} (", useIgnore ? "IGNORE" : String.Empty, table);
            string part2 = "";
            for (int i = 0; i < fieldVals.Length; i++)
            {
                if (i == (fieldVals.Length - 1)) { query += fieldVals[i]; part2 += string.Format("'{0}'", escape ? System.Text.RegularExpressions.Regex.Escape(valData[i]) : valData[i]); }
                else { query += fieldVals[i] + ", "; part2 += string.Format("'{0}', ", escape ? System.Text.RegularExpressions.Regex.Escape(valData[i]) : valData[i]); }
            }
            query += ") VALUES(";
            return query + part2 + ")";
        }

        /// <summary>
        /// Runs an sql update query on the specified table with the specified parameters
        /// </summary>
        /// <param name="table">The table to perform the query on</param>
        /// <param name="toAlter">The field value(s) to alter</param>
        /// <param name="alValues">the new values of the specified field(s)</param>
        /// <param name="args">paramters for where clause</param>
        /// <param name="values">values for where clause</param>
        /// <param name="append">append any other desired value for the query, eg. ORDER BY</param>
        /// <returns><Returns -1 if the an error occurs during execution or the number of rows affected by the operation/returns>
        internal static int Update(string table, string[] toAlter, string[] alValues, string[] args, string[] values, string append, bool escape)
        {
            int result = -1;
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string clause = string.Format("WHERE {0}= '{1}'", args[0], values[0]);
            string update = string.Format("SET {0} = '{1}'", toAlter[0], alValues[0]);
            for (int i = 1; i < toAlter.Length; i++)
            {
                update += string.Format(", {0} = '{1}'", toAlter[i], alValues[i]);
            }
            for (int i = 1; i < args.Length; i++)
            {
                clause += string.Format(" AND {0}= '{1}'", args[i], values[i]);
            }
            string sql = string.Format("UPDATE {0} {1} {2}", table, update, clause);
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            try
            {
                result = msc.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Logger.WriteLog(e);
            }
            finally { mc.Close(); }
            return result;
        }

        /// <summary>
        /// Runs an sql update query on the specified table with the specified parameters
        /// </summary>
        /// <param name="table">The table to perform the query on</param>
        /// <param name="toAlter">The field value(s) to alter</param>
        /// <param name="alValues">the new values of the specified field(s)</param>
        /// <param name="args">paramters for where clause</param>
        /// <param name="values">values for where clause</param>
        /// <param name="append">append any other desired value for the query, eg. ORDER BY</param>
        /// <returns><Returns -1 if the an error occurs during execution or the number of rows affected by the operation/returns>
        internal static int Update(string table, string[] toAlter, string[] alValues, string[] args, string[] values, string append)
        {
            int result = -1;
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string clause = string.Format("WHERE {0}= '{1}'", args[0], values[0]);
            string update = string.Format("SET {0} = '{1}'", toAlter[0], alValues[0]);
            for(int i = 1; i < toAlter.Length; i++)
            {
                update += string.Format(", {0} = '{1}'", toAlter[i], alValues[i]);
            }
            for(int i = 1; i < args.Length; i++)
            {
                clause += string.Format(" AND {0}= '{1}'", args[i], values[i]);
            }
            string sql = string.Format("UPDATE {0} {1} {2}", table, update, clause);
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            try
            {
                result = msc.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Logger.WriteLog(e);
            }
            finally { mc.Close(); }
            return result;
        }

        internal static string[][] selectResults(string[] fields, string table, string[] args, string[] values, string append, string clauseArg)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string clause = string.Format("WHERE {0}= '{1}'", args[0], values[0]);
            for (int i = 1; i < args.Length; i++)
            {
                clause += string.Format(" {2} {0}= '{1}' ", args[i], values[i], clauseArg);
            }
            string fieldvalues = fields[0];
            for (int i = 1; i < fields.Length; i++)
            {
                fieldvalues += ", " + fields[i];
                fields[i] = stripAs(fields[i]);
            }

            string sql = append.ToUpperInvariant().Contains("JOIN")
                 ? string.Format("SELECT {0} FROM {1} {2} ", fieldvalues, table, append) + clause
                 : string.Format("SELECT {0} FROM {1} {2} ", fieldvalues, table, clause) + append;
            
            MySqlCommand msc = new MySqlCommand(sql, mc);
            try
            { mc.Open(); }
            catch
            {
                return null;
            }
            var dr = msc.ExecuteReader();
            List<string[]> result = new List<string[]>();
            try
            {
                while (dr.Read())
                {
                    List<string> lresult = new List<string>();
                    for (int i = 0; i < fields.Length; i++)
                    {
                        lresult.Add(dr[fields[i]].ToString());
                    }
                    result.Add(lresult.ToArray());
                }
                return result.ToArray();
            }
            catch (Exception e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                dr.Close();
                mc.Close();
            }

            return null;
        }

        /// <summary>
        /// Strips the as sql keyword from a string and removes leading/trailing spaces
        /// </summary>
        /// <param name="s">The string to process</param>
        /// <returns>The original string is returned if it does not contain the keyword, otherwise the stripped version is returned</returns>
        private static string stripAs(string s)
        {
            int i = s.IndexOf("as ");
            return i >= 0 ? s.Substring(i + 3) : s.Trim();
        }

        /// <summary>
        /// Performs a query using the specified parameters.
        /// </summary>
        /// <param name="fields">An array containing the fields to query. You can also select fields/columns using the as keyword</param>
        /// <param name="table">The table to select from</param>
        /// <param name="args">An array containing the fields/columns you want to use as criteria</param>
        /// <param name="values">An array containing the values you want to use to match the argument crtieria array specified</param>
        /// <param name="append">Additional sql query to perform such as JOIN, GROUP etc.</param>
        /// <returns>An array of rows (array) containing the values retrieved from the database</returns>
        internal static string[][] selectResults(string[] fields, string table, string[] args, string[] values, string append)
        {
            if (fields.Length == 1 && fields[0].Contains(","))
            {
                fields = fields[0].Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries);
            }
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string clause = args.Length == 0 ? "" : string.Format("WHERE {0}= '{1}'", args[0], values[0]);
            for(int i = 1; i < args.Length; i++)
            {
                clause += string.Format(" AND {0}= '{1}' ", args[i], values[i]);
            }
            string fieldvalues = "";
            if (fields.Length == 0) fieldvalues = "*";
            else
            {
                fieldvalues = fields[0];
                fields[0] = stripAs(fields[0]);

                for (int i = 1; i < fields.Length; i++)
                {
                    fieldvalues += ", " + fields[i];
                    fields[i] = stripAs(fields[i]);
                }
            }
            string sql = append.ToUpperInvariant().Contains("JOIN")
                             ? string.Format("SELECT {0} FROM {1} {2} ", fieldvalues, table, append) + clause
                             : string.Format("SELECT {0} FROM {1} {2} ", fieldvalues, table, clause) + append;
            MySqlCommand msc = new MySqlCommand(sql, mc);
            try
            { mc.Open(); }
            catch (Exception ex){
                Logger.WriteLog(ex);
                return null;
            }
            var dr = msc.ExecuteReader();
            List<string[]> result = new List<string[]>();
            try
            {
                while (dr.Read())
                {
                    List<string> lresult = new List<string>();
                    if (fields.Length == 0)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            lresult.Add(dr[i].ToString());
                        }
                    }
                    else
                    {
                        for (int i = 0; i < fields.Length; i++)
                        {
                            lresult.Add(dr[fields[i]].ToString());
                        }
                    }
                    result.Add(lresult.ToArray());
                }
                return result.ToArray();
            }
            catch(Exception e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                dr.Close();
                mc.Close();
            }

            return null;
        }

        /// <summary>
        /// Performs a query using the specified parameters.
        /// </summary>
        /// <param name="fields">An array containing the fields to query. You can also select fields/columns using the as keyword</param>
        /// <param name="table">The table to select from</param>
        /// <param name="clause">A criteria to use for the selection</param>
        /// <returns>An array of rows (array) containing the values retrieved from the database</returns>
        internal static string[][] selectResults(string[] fields, string table, string clause)
        {
            if (fields.Length == 1 && fields[0].Contains(","))
            {
                fields = fields[0].Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            }
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string fieldvalues = fields[0];
            fields[0] = stripAs(fields[0]);

            for (int i = 1; i < fields.Length; i++)
            {
                fieldvalues += ", " + fields[i];
                fields[i] = stripAs(fields[i]);
            }
            string sql = string.Format("SELECT {0} FROM {1} {2}", fieldvalues, table, clause);
            MySqlCommand msc = new MySqlCommand(sql, mc);
            try
            { mc.Open(); }
            catch
            {
                return null;
            }
            var dr = msc.ExecuteReader();
            List<string[]> result = new List<string[]>();
            try
            {
                while (dr.Read())
                {
                    List<string> lresult = new List<string>();
                    for (int i = 0; i < fields.Length; i++)
                    {
                        lresult.Add(dr[fields[i]].ToString());
                    }
                    result.Add(lresult.ToArray());
                }
                return result.ToArray();
            }
            catch (Exception e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                dr.Close();
                mc.Close();
            }

            return null;
        }

        public static string getProperty(string property)
        {
            try
            {
                return ExecuteSelect(string.Format("SELECT value from schoolDetails where name = '{0}'", property))[0][0];
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the academic year. Assumptions: New Academic Year starts from August
        /// </summary>
        public static string AcademicYear
        {
            get { return getProperty("academicYear"); }
        }

        /// <summary>
        /// Performs a query using the specified parameters.
        /// </summary>
        /// <param name="fields">An array containing the fields to query. You can also select fields/columns using the as keyword</param>
        /// <param name="table">The table to select from</param>
        /// <param name="args">An array containing the fields/columns you want to use as criteria</param>
        /// <param name="values">An array containing the values you want to use to match the argument crtieria array specified</param>
        /// <param name="append">Additional sql query to perform such as JOIN, GROUP etc.</param>
        /// <param name="and">Boolean to indicate use of AND/OR. Use only if all the values have to be separated using OR</param>
        /// <returns>An array of rows (array) containing the values retrieved from the database</returns>
        internal static string[][] selectResults(string[] fields, string table, string[] args, string[] values, string append, bool and)
        {
            if (fields.Length == 1 && fields[0].Contains(","))
            {
                fields = fields[0].Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            }
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string clause = args.Length == 0 ? "" : string.Format("WHERE {0}= '{1}'", args[0], values[0]);
            for (int i = 1; i < args.Length; i++)
            {
                clause += string.Format(" {2} {0}= '{1}' ", args[i], values[i], and ? "AND" : "OR");
            }
            string fieldvalues = fields[0];
            fields[0] = stripAs(fields[0]);

            for (int i = 1; i < fields.Length; i++)
            {
                fieldvalues += ", " + fields[i];
                fields[i] = stripAs(fields[i]);
            }
            string sql = string.Format("SELECT {0} FROM {1} {2}", fieldvalues, table, clause) + append;
            MySqlCommand msc = new MySqlCommand(sql, mc);
            try
            { mc.Open(); }
            catch
            {
                return null;
            }
            var dr = msc.ExecuteReader();
            List<string[]> result = new List<string[]>();
            try
            {
                while (dr.Read())
                {
                    List<string> lresult = new List<string>();
                    for (int i = 0; i < fields.Length; i++)
                    {
                        lresult.Add(dr[fields[i]].ToString());
                    }
                    result.Add(lresult.ToArray());
                }
                return result.ToArray();
            }
            catch (Exception e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                dr.Close();
                mc.Close();
            }

            return null;
        }

        /// <summary>
        /// Computes rating based on average score
        /// </summary>
        /// <param name="score">The average score for a items number of students</param>
        /// <returns>A string showing Excellent, Very Good, etc</returns>
        public static string GetRating(int score)
        {
            if (score >= 90 && score <= 100)
                return "Excellent!";
            else if (score >= 70 && score < 90)
                return "Very Good";
            else if (score >= 50 && score < 70)
                return "Good";
            else if (score >= 30 && score < 50)
                return "Poor";
            else if (score >= 20 && score < 30)
                return "Very Poor";
            else if (score < 20)
                return "Ugly!";
            else
                return "Unknown";
        }
        
        public static string GpGrade(string gp)
        {
            switch (gp)
            {
                case "0":
                    return "F";
                case "1":
                    return "E";
                case "2":
                    return "D";
                case "3":
                    return "C";
                case "4":
                    return "B";
                case "5":
                    return "A";
                default:
                    return gp;
            }
        }

        internal static string genDeleteQuery(string table, object[] fields, object[] values)
        {
            string[] fieldVals = fields as string[];
            string[] valData = values as string[];
            if (fieldVals.Length != valData.Length) { return "Number of fields not equal to values."; }

            string clause = string.Format("WHERE {0}= '{1}'", fieldVals[0], valData[0]);
            for (int i = 1; i < fieldVals.Length; i++)
            {
                clause += string.Format(" AND {0}= '{1}' ", fieldVals[i], valData[i]);
            }

            string sql = string.Format("DELETE FROM {0} {1}", table, clause);
            return sql;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="primaryKey"></param>
        /// <param name="keyvalue"></param>
        /// <returns></returns>
        internal static int DbDelete(string table, string primaryKey, string keyvalue)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string sql = string.Format("DELETE FROM {0} WHERE {1} = '{2}'", table, primaryKey, keyvalue);
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
            }
            return retVal;
        }

        internal static int DbDelete(string table, object[] fields, object[] values)
        {
            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string sql = genDeleteQuery(table, fields, values);
            MySqlCommand msc = new MySqlCommand(sql, mc);
            mc.Open();
            int retVal = -1;
            try
            {
                retVal = msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
            }
            return retVal;
        }

        internal static string GenResultsData(List<StudentData> students, string courseCode, string academicYear)
        {
            return students.Aggregate("", (current, sd) => current + string.Format(",('{0}','{1}','{2}','{3}','{4}', '{5}')", sd.RegNo, courseCode, sd.CaMark, sd.ExamMark, sd.GpScore, academicYear));
        }

        internal static string GenDeleteData(List<StudentData> students, string courseCode, string academicYear)
        {
            return students.Aggregate(" ", (current, sd) => current + string.Format("(regNo = '{0}' AND CourseCode='{1}' AND academicYear ='{2}') OR ", sd.RegNo, courseCode, academicYear));
        }

        /// <summary>
        /// Returns a date-sorted list of activities suitable to use in a listview with just date and activity details
        /// </summary>
        /// <param name="criteriaName">The field name or criteria to match</param>
        /// <param name="criteria">The criteria value</param>
        /// <returns></returns>
        internal static List<Activity> GetActivities(string criteriaName, string criteria)
        {
            var result = selectResults(new[] {"activityDate", "text", "id"}, "activity", new[] {criteriaName},
                                       new[] {criteria}, " ORDER by activityDate");
            List<Activity> activities = new List<Activity>();
            if (result != null)
            {
                foreach (var activity in result)
                {
                    activities.Add(new Activity {ActivityDate = DateTime.Parse(activity[0]), Text = activity[1], Id=activity[2]});
                }
            }
            return activities;
        }

        /// <summary>
        /// Logs an activity model. The date and time is automatically inserted on the database side if not included
        /// </summary>
        /// <param name="ac">The activity model data</param>
        /// <returns></returns>
        internal static int logActivity(Activity ac)
        {
            Logger.WriteLog(ac.Username, ac.Username + ": " + ac.Text);
            var result = dbInsert("activity", new[] {"username", "text", "department", "faculty"},
                              new[] {ac.Username, ac.Text, ac.Department, ac.Faculty});
            return result;
        }
        
        /// <summary>
        /// Uploads the results for the specified students
        /// </summary>
        /// <param name="students">The StudentData mode list containing the students to upload their results</param>
        /// <param name="courseCode">The CourseCode of the course the results is being uploaded</param>
        /// <param name="academicYear">The academic year of the specified result</param>
        /// <param name="resubmit">Is this a resubmission? True otherwise false</param>
        /// <param name="department">The department that owns this course</param>
        /// <returns></returns>
        internal static int UploadResults(List<StudentData> students, string courseCode, string academicYear, bool resubmit, string department)
        {
            int result = -1;

            MySqlConnection mc = new MySqlConnection(Helper.LocalConnection);
            string sql = "INSERT INTO student_exams (regNo, CourseCode, ca_score, exam_score, gp_score, academicYear) VALUES " +
                         GenResultsData(students, courseCode, academicYear).Remove(0, 1);//Remove(0,1) because we don't need the first coma
            string toset = resubmit ? "submitted='Y',dapproved=NULL, fapproved=NULL" : "submitted='Y'";
            string submitquery = string.Format("UPDATE lecturer_courses SET {2} WHERE CourseCode='{0}' AND academicYear='{1}' AND department='{3}'",
                                               courseCode, academicYear, toset, department);

            if (resubmit)
            {
                var res = ExecuteNonQuery("INSERT INTO student_exams_archive (regNo, CourseCode, ca_score, exam_score, gp_score, academicYear) VALUES " +
                    GenResultsData(students, courseCode, academicYear).Remove(0, 1));
                if (res == -1)
                {
                    return -1;
                }
                string remove = GenDeleteData(students, courseCode, academicYear).Remove(0, 1);
                remove = remove.Remove(remove.Length - 3);
                res =
                    ExecuteNonQuery("DELETE FROM student_exams WHERE " +
                                    remove);
                if (res == -1)
                {
                    return -1;
                } 
            }

            MySqlCommand msc = new MySqlCommand(sql, mc);
            try
            {
                mc.Open();
                result = msc.ExecuteNonQuery();
                msc = new MySqlCommand(submitquery, mc);
                msc.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Logger.WriteLog(e);
            }
            finally
            {
                mc.Close();
            }
            return result;
        }

        internal static int InsertAnnouncement(string department, string faculty, string group, string announcement, string username)
        {
            return dbInsert("announcements",
                                    new[] {"department", "faculty", "target", "text", "username"},
                                    new[] {department, faculty, group, announcement, username}, true, true);
        }

        /// <summary>
        /// Returns announcements using specified parameters. Default Limit: 30
        /// </summary>
        /// <param name="rule">The departmen/faculty to search</param>
        /// <param name="criteria">The name of the department/faculty</param>
        /// <param name="role">The role of the user to retrieve announcements for</param>
        /// <returns>A simple string array of the announcements or null if none found</returns>
        internal static string[] GetAnnouncements(string rule, string criteria, string role)
        {
            return GetAnnouncements(rule, criteria, role, 30);
        }

        /// <summary>
        /// Returns announcements using specified parameters.
        /// </summary>
        /// <param name="rule">The departmen/faculty to search</param>
        /// <param name="criteria">The name of the department/faculty</param>
        /// <param name="role">The role of the user to retrieve announcements for</param>
        /// <param name="limit">The limit of the announcement to retrieve</param>
        /// <returns>A simple string array of the announcements or null if none found</returns>
        internal static string[] GetAnnouncements(string rule, string criteria, string role, int limit)
        {
            var results = selectResults(new[] { "text", "username", "announcementDate" }, "announcements",
                                        new[] { rule, "target" },
                                        new[] { criteria, "All" }, string.Format("OR target='{0}' ORDER by announcementDate LIMIT {1}", role, limit));
            if (results == null) return null;
            string[] announcements = new string[results.Length];
            for (int i = 0; i < results.Length; i++)
            {
                announcements[i] = results[i][0].Remove(results[i][0].Length - 9) + "\\par\\i By " + results[i][1] + ": " +
                                   results[i][2] + "\\i0\\par}\r\n";
            }
            return announcements;
        }

        private static int getColumnIndex(string columname)
        {
            for (int i = 0; i < columnMap.Length; i++)
            {
                if (columnMap[i] == columname)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Inserts transcript data to a generated transcript template
        /// </summary>
        /// <param name="tempName">The full path to the spreadsheet</param>
        /// <param name="sheetName">The name of the sheet to insert the data on</param>
        /// <param name="student">An array containing the students details. Todo:Change this to StudentData model</param>
        /// <param name="units">The total units the student registered</param>
        /// <param name="bforward">The total cummulative of the students registered units starting from first year first semester</param>
        /// <param name="columnStart">The column to start the insertion on</param>
        /// <param name="rowIndex">The row to start the insertion on</param>
        /// <returns></returns>
        internal static string[] InsertTranscriptData(string tempName, string sheetName, string[][] student, string units, string bforward, string columnStart, uint rowIndex)
        {
            // Open the document for editing.
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(tempName, true))
            {
                WorksheetPart worksheetPart = GetWorksheetPartByName(spreadSheet, sheetName);
                if (worksheetPart != null)
                {
                    //Let's insert the broadsheet forward
                    InsertCellNumber(worksheetPart, "H", 13, units);
                    InsertCellNumber(worksheetPart, "J", 13, bforward);
                    int column = 0;
                    //We have shared columns so let's use a custom sequence for iteration
                    string[] customMap = {"A", "C", "H", "I", "J", "K"};
                    int totalUnits = 0;
                    int totalGp = 0;
                    foreach (var result in student)
                    {
                        column = getColumnIndex(columnStart);
                        for (int i = 0; i < result.Length; i++)
                        {
                            switch (column)
                            {
                                case 2:
                                case 4:
                                    InsertCellNumber(worksheetPart, customMap[column], rowIndex, result[i]);
                                    totalUnits += column == 2 ? Convert.ToInt32(result[i]) : 0;
                                    totalGp += column == 4 ? Convert.ToInt32(result[i]) : 0;
                                    break;
                                case 3:
                                    InsertCellString(worksheetPart, customMap[column], rowIndex, GpGrade(result[i]));
                                    break;
                                default:
                                    InsertCellString(worksheetPart, customMap[column], rowIndex, result[i]);
                                    break;
                            }
                            column++;
                        }
                        rowIndex++;
                    }

                    //Let's insert the total Units and GP data
                    InsertCellNumber(worksheetPart, "H", 28, totalUnits + "");
                    InsertCellNumber(worksheetPart, "J", 28, totalGp + "");

                    //Let's insert the cumulative total
                    if (units == "")
                    {
                        units = "0";
                    }
                    if (bforward == "")
                    {
                        bforward = "0";
                    }
                    int allUnits = Convert.ToInt32(units);
                    int allGp = Convert.ToInt32(bforward);
                    int finalUnits = totalUnits + allUnits;
                    int finalGp = totalGp + allGp;

                    InsertCellNumber(worksheetPart, "H", 29, finalUnits + "");
                    InsertCellNumber(worksheetPart, "J", 29, finalGp + "");

                    //Let's calculate final GP and insert
                    double cgp = (double)finalGp / finalUnits;
                    double gp = (double)totalGp / totalUnits;

                    string sgp = (gp + "");
                    sgp = sgp.Length > 4 ? sgp.Remove(4) : sgp;
                    string scgp = (cgp + "");
                    scgp = scgp.Length > 4? scgp.Remove(4) : scgp;

                    InsertCellString(worksheetPart, "K", 28, sgp + " GPA");
                    InsertCellString(worksheetPart, "K", 29, scgp + " CGPA");

                    worksheetPart.Worksheet.Save();
                    spreadSheet.Close();
                    return new[] {sgp, scgp};
                }
            }
            return null;
        }


        /// <summary>
        /// Returns announcements using specified parameters. Limit: 30
        /// </summary>
        /// <param name="rule">The departmen/faculty to search</param>
        /// <param name="criteria">The name of the department/faculty</param>
        /// <param name="role"></param>
        /// <returns>A simple string array of the announcements or null if none found</returns>
        public static int GetAnnouncementCount(string rule, string criteria, string role)
        {
            try
            {
                var results = selectResults(new[] {"COUNT(*)"}, "announcements",
                                            new[] {rule, "target"},
                                            new[] {criteria, "All"}, string.Format("OR target='{0}'", role));
                if (results == null) return -1;
                return Convert.ToInt32(results[0][0]);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        
        /// <summary>
        /// Returns the count of announcements with the specified criteria
        /// </summary>
        /// <param name="rule">The field name or criteria to match</param>
        /// <param name="criteria">The criteria value</param>
        /// <returns></returns>
        public static int GetActivitiesCount(string rule, string criteria)
        {
            try
            {
                var result = selectResults(new[] {"COUNT(*)"}, "activity", new[] {rule},
                                           new[] {criteria}, "");
                if (result == null)
                {
                    return -1;
                }
                return Convert.ToInt32(result[0][0]);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        /// <summary>
        /// Takes a modeled data and serializes it to the specified path
        /// </summary>
        /// <param name="type">The type of data to serialize as</param>
        /// <param name="path">The path to store the serialization</param>
        /// <param name="data">The data to serialize</param>
        public static void SerializeData(Type type, string path, object data)
        {
            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer;
                serializer = new XmlSerializer(type);
                serializer.Serialize(writer, data);
            }
        }

        /// <summary>
        /// Deserializes a serialized file to the appropriate object type
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal static object Deserialize(string path, Type type)
        {
            using (var fs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer serializer;
                serializer = new XmlSerializer(type);
                object deserialize = serializer.Deserialize(fs);
                return deserialize;
            }
        }
    }

    public enum MessageType
    {
        Normal = 0,
        Critical = 1
    }
}
