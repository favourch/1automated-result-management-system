using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ARMS
{
    public partial class Announcer : Form
    {
        private string username;
        private string faculty;

        public Announcer(string username, string faculty, string dept)
        {
            InitializeComponent();
            this.username = username;
            if (faculty == "")
            {
                //Load Faculties
                var results = Helper.selectResults(new[] {"facultyName"}, "faculties", "");
                foreach (var fac in results)
                {
                    comboBoxFaculty.Items.Add(fac[0]);
                }
                this.faculty = comboBoxFaculty.Text;
            }
            else
            {
                this.faculty = faculty;
            }
            loadDepartments(dept);
        }

        private Dictionary<string, string> departments = new Dictionary<string, string>();

        private void loadDepartments(string deptName)
        {
            var depts = Helper.selectResults(new[]{"deptName", "faculty"}, "departments", string.Format("WHERE faculty = '{0}'", faculty));
            foreach(var dept in depts)
            {
                if (deptName == "")
                {
                    comboBoxDepartment.Items.Add(dept[0]);
                }
                departments.Add(dept[0], dept[1]);
                if (!comboBoxFaculty.Items.Contains(dept[1])) comboBoxFaculty.Items.Add(dept[1]);
            }
            comboBoxFaculty.Text = faculty;
            comboBoxDepartment.Text = deptName;
            comboBoxGroup.Text = "All";
        }

        private void buttonAnnounce_Click(object sender, EventArgs e)
        {
            if (
                Helper.InsertAnnouncement(comboBoxDepartment.Text, comboBoxFaculty.Text, comboBoxGroup.Text,
                                           richTextBoxExtended1.RichTextBox.Rtf, username) == 1)
            {
                MessageBox.Show("Your announcement has been sent successfully", "Announcement Successful",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                richTextBoxExtended1.RichTextBox.Clear();
            }
            else
            {
                var dr = MessageBox.Show("An error occured while trying to make the announcement. Please try again.",
                                "Announcement Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (dr == DialogResult.Retry)
                {
                    buttonAnnounce.PerformClick();
                }
            }
        }

        private void comboBoxFaculty_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDepartment.Items.Clear();
            foreach (var dept in departments)
            {
                if (dept.Value == comboBoxFaculty.Text)
                {
                    comboBoxDepartment.Items.Add(dept.Key);
                }
            }
        }
    }
}
